#!/usr/bin/env python3

import logging
import os
import sys

import dotenv
import requests

import parsepost

logger = logging.getLogger(__name__)


def post_to_medium(filename):
    front_matter, body = parsepost.parse_post(filename)

    if len(front_matter['title']) > 100:
        raise Exception(f"""Title "{front_matter['title']}" is too long.""")
    for tag in front_matter['tags']:
        if len(tag) > 25:
            raise Exception(f"""Tag "{tag}" is too long.""")
    if len(front_matter['tags']) > 5:
        logger.warning("""Too many tags for filename "%(filename)s".""", {
            'filename': filename,
        })

    response = requests.post(
        f"https://api.medium.com/v1/publications/{os.environ['MEDIUM_PUBLICATION_ID']}/posts",
        json={
            'title': front_matter['title'],
            'contentFormat': 'markdown',
            'content': body,
            'tags': front_matter['tags'],
             # TODO: Do not hard-code.
            'canonicalUrl': f"https://mitar.tnode.com/post/{front_matter['slug']}/",
            'publishStatus': 'public',
            'publishedAt': front_matter['publishDate'].isoformat(timespec='seconds'),
            'license': 'cc-40-by-sa',
            'notifyFollowers': True,
        },
        headers={
            'authorization': f"Bearer {os.environ['MEDIUM_ACCESS_TOKEN']}",
        }
    )
    response.raise_for_status()
    json_response = response.json()

    logger.info("""Published Medium post for filename "%(filename)s: %(response)s""", {
        'filename': filename,
        'response': json_response,
    })


def main():
    logging.basicConfig(level=logging.INFO)
    dotenv.load_dotenv()

    for filename in sys.argv[1:]:
        try:
            post_to_medium(filename)
        except:
            logger.exception("""Exception for filename "%(filename)s".""", {
                'filename': filename,
            })


if __name__ == '__main__':
    main()
