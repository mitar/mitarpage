#!/usr/bin/env python3

import logging
import sys
import datetime

import parsepost

logger = logging.getLogger(__name__)


def update_timestamp(filename):
    front_matter, body = parsepost.parse_post(filename)

    front_matter['publishDate'] = datetime.datetime.now(datetime.timezone.utc)

    parsepost.write_post(front_matter, body, filename)


def main():
    logging.basicConfig(level=logging.INFO)

    for filename in sys.argv[1:]:
        try:
            update_timestamp(filename)
        except:
            logger.exception("""Exception for filename "%(filename)s".""", {
                'filename': filename,
            })


if __name__ == '__main__':
    main()
