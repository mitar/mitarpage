#!/usr/bin/env python3

import logging
import sys

import parsepost

logger = logging.getLogger(__name__)


def normalize_post(filename):
    front_matter, body = parsepost.parse_post(filename)

    parsepost.write_post(front_matter, body, filename)


def main():
    logging.basicConfig(level=logging.INFO)

    for filename in sys.argv[1:]:
        try:
            normalize_post(filename)
        except:
            logger.exception("""Exception for filename "%(filename)s".""", {
                'filename': filename,
            })


if __name__ == '__main__':
    main()
