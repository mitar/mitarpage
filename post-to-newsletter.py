#!/usr/bin/env python3

import logging
import os
import smtplib
import ssl
import sys
from email import message

import dotenv
import html_text
import mistune

import parsepost

logger = logging.getLogger(__name__)

READ_MORE_TEMPLATE="""Read the full blog post and comment at:

{link}
"""
READ_TEMPLATE="""Read the blog post and comment at:

{link}
"""
EMAIL_TEMPLATE_ONE="""Hi!

I just published a new blog post:

*{title}*

{summary}

{read_more}

Mitar
"""

markdown = mistune.Markdown(escape=False, hard_wrap=False)


def format_one(filename):
    front_matter, body = parsepost.parse_post(filename)

    parts = body.split('<!--more-->')
    if len(parts) == 2:
        if parts[1].strip() == "":
            del parts[1]

    if len(parts) == 1:
        read_more = READ_TEMPLATE.format(link=f"https://mitar.tnode.com/post/{front_matter['slug']}/",)
    elif len(parts) == 2:
        read_more = READ_MORE_TEMPLATE.format(link=f"https://mitar.tnode.com/post/{front_matter['slug']}/",)
    else:
        raise Exception(f"""Invalid number of parts in filename "{filename}".""")

    summary = html_text.extract_text(markdown(parts[0].strip())).strip()

    return front_matter['title'], EMAIL_TEMPLATE_ONE.format(title=front_matter['title'], summary=summary, read_more=read_more)


def post_to_newsletter(filename):
    subject, body = format_one(filename)

    print(body)

    answer = input("Post this message? ").lower()
    if not answer.startswith("y"):
        logger.info("Aborted.")
        return

    msg = message.EmailMessage()
    msg.set_content(body)

    msg['Subject'] = subject
    msg['From'] = os.environ['NEWSLETTER_FROM_EMAIL']
    msg['To'] = os.environ['NEWSLETTER_TO_EMAIL']

    # Support old SSL configurations.
    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.set_ciphers('DEFAULT:@SECLEVEL=1')
    smtp_client = smtplib.SMTP_SSL(os.environ['NEWSLETTER_SMTP_SERVER'], context=ssl_context)
    smtp_client.login(os.environ['NEWSLETTER_SMTP_USERNAME'], os.environ['NEWSLETTER_SMTP_PASSWORD'])
    smtp_client.send_message(msg)
    smtp_client.quit()

    logger.info("Posted to newsletter.")


def main():
    logging.basicConfig(level=logging.INFO)
    dotenv.load_dotenv()

    try:
        post_to_newsletter(sys.argv[1])
    except:
        logger.exception("Error posting to newsletter.")


if __name__ == '__main__':
    main()
