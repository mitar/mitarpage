FROM node:16-buster

RUN \
  apt-get update -q -q && \
  apt-get install --yes libnss3 libatk1.0-0 libatk-bridge2.0-0 libx11-xcb1 \
  libcups2 libdrm2 libxkbcommon0 libxcomposite1 libxdamage1 libxrandr2 \
  libgtk-3-0 libgbm1 libasound2 && \
  PERCY_POSTINSTALL_BROWSER=true npm install -g "@percy/cli@1.26.2" && \
  apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
