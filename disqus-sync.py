#!/usr/bin/env python3

import logging
import os
import re
import sys

import dotenv
import requests

import parsepost

logger = logging.getLogger(__name__)

# Hugo currently makes a http meta tag redirect for GitLab pages, so we parse it out.
REDIRECT_REGEX = re.compile('<meta[^>]+?url=([^"]+?)"', re.IGNORECASE)


def post_metadata(filename):
    front_matter, body = parsepost.parse_post(filename)

    title = front_matter['title']
    url = f"https://mitar.tnode.com/post/{front_matter['slug']}/"
    disqus_id = front_matter.get('disqus_id', front_matter['slug'])

    return {
        'title': title + " | Mitar's Point",
        'url': url,
        'disqus_id': disqus_id,
    }


def get_disqus_threads():
    threads = []
    cursor = None
    while True:
        response = requests.get(
            'https://disqus.com/api/3.0/threads/list.json',
            params={
                'forum': os.environ['DISQUS_FORUM'],
                'api_key': os.environ['DISQUS_API_KEY'],
                'api_secret': os.environ['DISQUS_API_SECRET'],
                'access_token': os.environ['DISQUS_ACCESS_TOKEN'],
                'limit': 100,
                'cursor': cursor,
            },
        )
        response.raise_for_status()
        json_response = response.json()
        for thread in json_response['response']:
            threads.append({
                'id': thread['id'],
                'url': thread['link'],
                'title': thread['title'],
                'disqus_ids': thread['identifiers'],
                'posts': thread['posts'],
            })

        if json_response['cursor']['hasNext']:
            cursor = json_response['cursor']['next']
        else:
            break

    return threads


def update_disqus_thread(thread_id, **params):
    params.update({
        'thread': thread_id,
        'api_key': os.environ['DISQUS_API_KEY'],
        'api_secret': os.environ['DISQUS_API_SECRET'],
        'access_token': os.environ['DISQUS_ACCESS_TOKEN'],
    })
    response = requests.post(
        'https://disqus.com/api/3.0/threads/update.json',
        params=params,
    )
    response.raise_for_status()


def delete_disqus_thread(thread_id):
    response = requests.post(
        'https://disqus.com/api/3.0/threads/remove.json',
        params={
            'thread': thread_id,
            'api_key': os.environ['DISQUS_API_KEY'],
            'api_secret': os.environ['DISQUS_API_SECRET'],
            'access_token': os.environ['DISQUS_ACCESS_TOKEN'],
        },
    )
    response.raise_for_status()


def get_new_url(url):
    response = requests.get(url)
    response.raise_for_status()
    new_url = response.url
    match = REDIRECT_REGEX.search(response.text)
    if match:
        new_url = match[1]
    return new_url


def main():
    logging.basicConfig(level=logging.INFO)
    dotenv.load_dotenv()

    known_posts = []
    for filename in sys.argv[1:]:
        known_posts.append(post_metadata(filename))

    disqus_threads = get_disqus_threads()

    known_posts_dict = {post['disqus_id']: post for post in known_posts}
    threads_with_disqus_id = {}
    threads_without_disqus_id = []
    for thread in disqus_threads:
        if not thread['disqus_ids']:
            threads_without_disqus_id.append(thread)
        else:
            for disqus_id in thread['disqus_ids']:
                assert disqus_id not in threads_with_disqus_id, disqus_id
                threads_with_disqus_id[disqus_id] = thread

    for disqus_id, post in known_posts_dict.items():
        if disqus_id not in threads_with_disqus_id:
            logger.error("Not in Disqus: %(post)s", {'post': post})
        else:
            thread = threads_with_disqus_id[disqus_id]
            if post['title'] != thread['title']:
                logger.error('Title does not match: "%(post_title)s" vs. "%(thread_title)s"', {'post_title': post['title'], 'thread_title': thread['title']})
                update_disqus_thread(thread['id'], title=post['title'])
            if post['url'] != thread['url']:
                logger.error('URL does not match: "%(post_url)s" vs. "%(thread_url)s"', {'post_url': post['url'], 'thread_url': thread['url']})
                update_disqus_thread(thread['id'], url=post['url'])

    for disqus_id, thread in threads_with_disqus_id.items():
        if disqus_id not in known_posts_dict:
            if not thread['posts']:
                logger.error("Not in posts, empty so deleting: %(thread)s", {'thread': thread})
                delete_disqus_thread(thread['id'])
            else:
                logger.error("Not in posts, not empty so keeping: %(thread)s", {'thread': thread})

    for thread in threads_without_disqus_id:
        if not thread['posts']:
            logger.error("Thread without ID, empty so deleting: %(thread)s", {'thread': thread})
            delete_disqus_thread(thread['id'])
        else:
            # Use this as an input to the URL mapper at: https://mitar.disqus.com/admin/discussions/migrate/
            # After this, one has to open all new URLs to populate threads with new "disqus_id".
            print(f'''"{thread['url']}","{get_new_url(thread['url'])}"''')


if __name__ == '__main__':
    main()
