## Introduction

This is repo for the [mitar.tnode.com](https://mitar.tnode.com) blog.
It uses [hugo](https://gohugo.io/) a static website generator as the framework.
Check their website for instructions on how to install it.

## Project structure

The following is the projects structure and use.
* `archetypes` Is a folder where templates for new posts is stored. Running `hugo new post/something.md` will look for the `archetypes/post.md`
and populate the `something.md` file from that template.
* `assets` Is a folder that resources are stored in. When using the `resources.Get` function in the template it is used as root directory it works in.
* `content` Is a folder where the markdown files are stored (posts and other). Some files have to be there for the hugo to generate the html file
even if the file itself is empty.
* `layouts` In this folder the html templates are stored. The `_default` is used for the base template and base of different page types. We only use taxonomy which is a page that describes a specific tag. `partials` are just that.
The folder `post` stores the template for the page with type `post`. `section`'s are one of pages like the archive or research. While you would think that index and 404 belong here those are spacial pages as they are present on all hugo sites while in our case `archive` and `about` are not.
* `public` The output of `hugo`  is put in the public folder which is what gets deployed.
* `resources` Is a spacial folder that hugo uses for generated files like minified or css files generated from scss. It should be ignored.
* `static` Folder is mounted at root of the page so if you have a file `static/images/favico.jpg` it would be outputted to `{BASE_URL}/images/favico.jpg`.

## Developing

Install `hugo`. On Ubuntu:

```bash
$ sudo snap install hugo --channel=extended/stable
```

Running the `hugo server --buildFuture --buildDrafts` will load the hugo's development server which watches for changes and pushes them with hot reload functionality
to the browser.

## Building

To build the website running the command `hugo` will generate the page.

## Using Docker

You can also build using Docker:

```bash
$ docker run --rm -v "$(pwd):/data" -w /data registry.gitlab.com/pages/hugo/hugo_extended:0.57.2 hugo
```
