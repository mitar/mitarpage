---
title: nodewatcher
icon: link
publishDate: 2013-03-28 23:39:10-07:00
draft: false
tags:
- project
- wlanslovenija
- nodewatcher
- nodedb
- python
- mongodb
- django
disqus_id: "46576200126"
slug: nodewatcher
aliases:
- /post/46576200126/nodewatcher
- /post/46576200126/
---

[_nodewatcher_](http://dev.wlan-si.net/wiki/Nodewatcher) is a platform for organic (bottom-up) growing of open networks,
a network planning, deployment, monitoring, and maintenance platform.

<!--more-->
