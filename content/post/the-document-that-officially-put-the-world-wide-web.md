---
title: The document that officially put the World Wide Web into the public domain
icon: link
publishDate: 2013-09-18 07:30:10-07:00
draft: false
tags:
- www
- publicdomain
disqus_id: "61588585128"
slug: the-document-that-officially-put-the-world-wide-web
aliases:
- /post/61588585128/the-document-that-officially-put-the-world-wide
- /post/61588585128/
---

[The document](https://cds.cern.ch/record/1164399?ln=en) that officially put the World Wide Web into the public domain on 30 April 1993.

<!--more-->
