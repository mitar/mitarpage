---
title: Google search across languages
publishDate: 2021-01-08 07:54:22.877804+00:00
draft: false
tags:
- google
- search
- languagebubble
- languages
- web
- idea
slug: google-search-across-languages
---

In 2013, I wrote [how crowdsourcing could help break out of the language bubble](https://mitar.tnode.com/post/crowdsourcing-the-language-bubble-breakout/)
we all find ourselves in when we search online. How our search results only come from the language we write
our search keywords in. But since then, machine translation between languages has greatly improved.
So why are we still in our language bubbles?

There are some rays of hope. Google is reportedly able to return you results from other languages
when there are no good hits in your primary language. I have to say, though, that I have never experienced this.
Maybe because I usually search in English. I would guess that when you search in smaller languages,
you sometimes get English results added.

But I want the opposite. I want to see ideas and thoughts and solutions that might be available
in other languages, the languages I do not speak, when I search in English.
I want diversity.
I think all the pieces to build this are available.
Why is this not already available? Am I overlooking any obstacle to this?

<!--more-->
