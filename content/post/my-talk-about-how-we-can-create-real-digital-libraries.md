---
title: My talk about how we can create real digital libraries
icon: video
publishDate: 2014-10-27 17:30:10-07:00
draft: false
tags:
- media
- peerlibrary
- api
- publishers
- diversity
- ecosystem
- libraries
disqus_id: "101130439746"
slug: my-talk-about-how-we-can-create-real-digital-libraries
aliases:
- /post/101130439746/my-talk-about-how-we-can-create-real-digital
- /post/101130439746/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/PN_ikuobiKM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

My talk about how we can create real digital libraries. Those which not just archive publications, but provide social
interactions around books as well. In short: involve the community to build tools for various social interactions around
digital publications and for that to be possible publishers should offer APIs.

<!--more-->
