---
title: Sybil attacks and shell corporations
publishDate: 2016-07-25 18:47:10-07:00
draft: false
tags:
- corporations
- sybil
disqus_id: "147974368166"
slug: sybil-attacks-and-shell-corporations
aliases:
- /post/147974368166/sybil-attacks-and-shell-corporations
- /post/147974368166/
---

In computer security an important type of an attack in a decentralized system like Internet is a [Sybil
attack](https://en.wikipedia.org/wiki/Sybil_attack). The core of the attack is that many protocols we have developed
depend on the assumption that each entity participating in a protocol participates only once and that it cannot create
an arbitrarily number of additional "puppet" entities which it can control. Because on the Internet it is easy to
present yourself with multiple identities, [decentralized systems with open membership are often susceptible to this
type of an attack](/post/one-person-one-vote-or-one-dollar-one-vote).

Why are we designing such protocols? Maybe it is because so many protocols are based on existing processes we find
between humans? Or maybe there is some fundamental issue of open, decentralized systems and identities.

But even more interesting is to observe that we have similar protocols in our existing society, with the same
assumption. Moreover, we also allow people to create additional identities as needed. We call them corporations. Many
our protocols in our society were designed when only humans were persons. Governments make sure that humans have unique
identities and we have passports to allow governments to trust other governments about this validation. Creating
corporations is on the other hand much less controlled. You have many countries with less strict laws which allow one to
create [shell ("puppet") corporations](https://en.wikipedia.org/wiki/Shell_corporation). Traversing multiple
jurisdictions through interactions between such corporations can hide many traces of linked identities. In a way, we
allow arbitrary number of corporations to be created, without really requiring passports for them to be able to work
with other corporations across borders. [A passport which would link corporations to their unique
identities](https://opencorporates.com/). Furthermore, the issue is even more complicated because there can be multiple
people behind corporations, and also other corporations.

So a real question is not why we are designing such protocols on the Internet, but why we are having ways to compromise
such protocols outside the Internet. When we know that they can be misused and used to launch attacks. We already see
such attacks in practice through pervasive tax evasions and other financial maneuvers.

<!--more-->
