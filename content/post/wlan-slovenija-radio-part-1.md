---
title: wlan slovenija (part 1)
icon: audio
publishDate: 2013-03-23 02:24:10-07:00
draft: false
tags:
- media
- wlanslovenija
- project
- wireless
- meshnetworks
- communitynetworks
disqus_id: "46060565293"
slug: wlan-slovenija-radio-part-1
aliases:
- /post/46060565293/wlan-slovenija-open-wireless-network-radio
- /post/46060565293/
---

<audio class="add-margin" controls src="http://kruljo.radiostudent.si/mp3/KPU/111214-KPU-mitar1-P.mp3" type="audio/mpeg" style="outline: none; width: 570px; margin-bottom: -10px;"></audio>

*wlan slovenija* open wireless network radio interview at local [Radio Student](http://www.radiostudent.si/) (in
Slovene). Part 1.

<!--more-->
