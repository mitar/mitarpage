---
title: Local cooperation benefits global competitiveness
icon: quote
publishDate: 2014-03-20 10:07:10-07:00
draft: false
tags:
- evolution
- cooperation
- competition
disqus_id: "80175469158"
slug: local-cooperation-benefits-global-competitiveness
aliases:
- /post/80175469158/the-basic-biological-answer-is-that-local
- /post/80175469158/
---

> The basic biological answer is that local cooperation benefits global competitiveness.
> But that depends on the definition of local, either actual or perceived!
>
> As the Chinese say, in times of small trouble go to the city. In times of big trouble
> go to the countryside. The rest is commentary!

Answer by [Robert Dudley](http://ib.berkeley.edu/people/faculty/dudleyr) to my question which is more evolutionary
"natural" for humans to do, cooperate or compete.

<!--more-->
