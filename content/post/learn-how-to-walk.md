---
title: Learn how to walk
publishDate: 2016-06-25 10:30:10-07:00
draft: false
tags:
- walking
- growing
- learning
disqus_id: "146460986458"
slug: learn-how-to-walk
aliases:
- /post/146460986458/learn-how-to-walk
- /post/146460986458/
---

Learn how to walk. Literally. When I observe people how they walk I notice that everyone walks differently. This is
great, it allows one to recognize friends from far away. But it is also bad: some types of walking and posture is worse
for you than other. Especially because you are probably walking the same way your whole life.

We are never really taught how to walk. We learn on our own. It is one of first things we learn. And everyone around us
is so happy that they forget to help us improve our walk. And we, based on our first steps we did by chance, extrapolate
to walking and get used to it. For long our body tolerates any way we are using it, but through years we might suddenly
discover that it cannot anymore. But then it is too late.

While others help us with other things we learn (for example, talking, you will get feedback if you talk
incomprehensibly, or too loudly), for walking we have to do it ourselves. So stop just repeating steps you did as a
child and start walking your grown-up walk.

<!--more-->
