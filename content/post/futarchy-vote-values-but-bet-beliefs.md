---
title: 'Futarchy: Vote Values, But Bet Beliefs'
icon: link
publishDate: 2015-09-07 13:28:10-07:00
draft: false
tags:
- decisionmaking
- predictionmarkets
- democracy
- voting
disqus_id: "128583271366"
slug: futarchy-vote-values-but-bet-beliefs
aliases:
- /post/128583271366/futarchy-vote-values-but-bet-beliefs
- /post/128583271366/
---

An [interesting idea](https://mason.gmu.edu/~rhanson/futarchy.html) how to aggregate knowledge for decision making.

<!--more-->
