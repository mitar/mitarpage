---
title: A great talk by Bret Victor about the future of programming
icon: video
publishDate: 2013-08-04 01:57:10-07:00
draft: false
tags:
- bretvictor
- programming
- future
disqus_id: "57320773292"
slug: a-great-talk-by-bret-victor-about-the-future
aliases:
- /post/57320773292/a-great-talk-by-bret-victor-about-the-future-or
- /post/57320773292/
---

<iframe width="570" height="320" class="add-margin" src="https://player.vimeo.com/video/71278954" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

A great talk by Bret Victor about the future (or past?) of programming.

<!--more-->
