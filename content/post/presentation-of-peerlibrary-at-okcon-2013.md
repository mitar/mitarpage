---
title: Presentation of PeerLibrary at OKCon 2013
icon: photo
publishDate: 2013-09-24 03:48:10-07:00
draft: false
tags:
- peerlibrary
- presentation
- okcon
- project
disqus_id: "62143883932"
slug: presentation-of-peerlibrary-at-okcon-2013
aliases:
- /post/62143883932/presentation-of-peerlibrary-at-okcon-2013
- /post/62143883932/
---

[![](/post/peerlibrary-okcon-2013.jpg)](/post/peerlibrary-okcon-2013.jpg)

Presentation of [PeerLibrary](http://peerlibrary.org/) at [OKCon 2013](http://okcon.org/).

via [peerlibrary](https://blog.peerlibrary.org/post/62143491363/presentation-of-peerlibrary-at-okcon-2013)

<!--more-->
