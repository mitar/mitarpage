---
title: Effective democracy
publishDate: 2013-08-03 02:13:10-07:00
draft: false
tags:
- democracy
- effective
- civicengagement
- efficiency
disqus_id: "57234755112"
slug: effective-democracy
aliases:
- /post/57234755112/effective-democracy
- /post/57234755112/
---

What exactly is effective democracy? What exactly is effective participation in democracy? Some people complain that
other people do not participate in democracy enough. Not enough people vote. Not enough people sign petitions. Not
enough people go to riots. Why?

<!--more-->

I believe the answer is simple: people do not see such their civic participation as effective. Effective means two
things:

1. it is not (too) time consuming
2. it is has a (visible) impact

It is really easy. People do not have time to engage in democracy. That is why you appoint your representatives or
delegates, so that you can work your double-shifts. While your representatives or delegates work for you. On the other
hand, you want to feel that their actions do have some impact. That it matters how you vote. Does current democracy
delivers that? No.

We should start working on an effective democracy. Democracy, where it is easy to see how your actions influence
changes. What is the chain of influences between you, your vote or some other civic action and those changes, which
representatives and which their actions. And to have this presented in an easy, intuitive and not time consuming manner.
Actions should be easy to do, not time consuming, and with visible results. We should create 21st century versions of
votes, petitions and riots. We could build technological tools to make actions less time consuming and to display nice
visualizations of changes you influenced, but this is not really enough. We need changes in legislation which should
make possible that those actions have real impact. So a solution might be:

1. technological tools to make 21 century civic engagement time efficient and results of your civic actions evident
2. rethink civic actions for 21 century
3. legislation which makes such 21st century civic engagement impactful, for real changes

But who wants changes?
