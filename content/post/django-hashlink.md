---
title: django-hashlink
icon: link
publishDate: 2013-03-28 22:19:10-07:00
draft: false
tags:
- project
- django
- hashlink
- python
- mongodb
disqus_id: "46572035081"
slug: django-hashlink
aliases:
- /post/46572035081/django-hashlink
- /post/46572035081/
---

[Django app](https://github.com/mitar/django-hashlink) to store and retrieve URL hash (fragment) based webpage state in
[MongoDB](http://www.mongodb.org/) through [Django](https://www.djangoproject.com/).

<!--more-->

It is an experimental idea to store state of GUI of modern dynamic web applications on the server, identified by a
string which is then used as hash (fragment) in URL. In this way URL really represents the state of the application so
if it is shared and opened independently, visitor gets exactly the same application GUI state (opened windows, location
on the page, etc.). On every GUI state change web application sends new state and retrieves string ID for it, updating
the URL.

Additionally, this helps application developers to better understand how users are using the application as it allows
precise tracking of their interaction with the application. django-hashlink is designed so that it does not store any
user identifiable information along its data.
