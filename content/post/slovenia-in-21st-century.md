---
title: Slovenia in 21st century
publishDate: 2013-06-19 02:20:10-07:00
draft: false
tags:
- slovenia
- datacenter
- future
- imagination
- vision
- law
- 21century
- legislation
disqus_id: "53348340616"
slug: slovenia-in-21st-century
aliases:
- /post/53348340616/slovenia-in-21st-century
- /post/53348340616/
---

When I look at [Slovenia](https://en.wikipedia.org/wiki/Slovenia) I see a country without a vision. Without imagination
what would make the country something special. What would put it on the map. I see many great individuals with great
ideas doing great things. But as a country? A small country lost somewhere in the middle of the Europe, without its own
path, just following and responding to internal and external pressures. Not really in control of its own future.

Maybe it is because our politicians are corrupt, but maybe it is also because we don't really want to change things. We
don't aim for some bright future, but would just like that it is "good enough". We just want to survive. We don't see
the country as something which is ours and over which we have some control and which we can improve and improve until it
shines.

So how do you want to see Slovenia in 21st century? For what it should be known? What should be those ways which would
bring prosperity to it? We should do new things for that, as a country, we cannot just replicate others.

<!--more-->

My main idea is that laws are those which define the country. If the country has all laws copied from other countries,
what advantage it can have over them? It can be bigger, it can have more natural resources, it can have colonies or in
general some kind of global power, it can have better education and academia it worked on for centuries, it can be more
connected, more cooperative, more streamlined. But what about young small country without natural resources and with
traditional internal conflicts like Slovenia?

It can have laws which make it special in some way. Which can make for its citizens easier to innovate, build, and
provide some products and services to others. Some countries provide lower taxes for companies, less protection for
workers, and in general less government services and regulation. But what if we want to keep social and caring nature of
Slovenia? If we want to keep universal healthcare, free education for everybody, and workers' rights? We cannot really
lower taxes then.

Some countries provide laws kind to financial sector, tax havens, banking friendly laws. But market here is saturated.
One more tax haven? No thank you.

But what we could do is recognize that we are in 21st century. We could embrace Internet and all its sharing and P2P
culture, its crowdsourced and crowdfunded creativity, and its data and networks centric lifestyle. We could innovate in
laws concerning intellectual rights, data protection, privacy and anonymity. We could innovate in ways laws govern how
things can be funded and how things can be build in a collaborative fashion. The country could stand behind people
publishing creativity, information, and knowledge, in the same way countries traditionally stand behind their banks.

For example, Slovenia could pass the law which would give foreigners sovereignty over their data which is stored in
datacenters in Slovenia. Like embassies in a given country have sovereignty. Why not also data? So than Google could
have datacenters in Slovenia, but they would still be susceptible only to USA legislation (because it is founded there)
and Slovenia would not have any right to seize or access that data. This would allow big Internet companies to have
their servers in the center of Europe, close to their European customers, but could still be assured that nobody would
interfere with their operations.

On the other hand, Slovenia could change laws to provide asylum to data. Foreigners could then store data in datacenters
in Slovenia and request its protection from their own government. Or the protection could simply be automatic.

Slovenia with its central geoposition in Europe could become a country known by its safe datacenters, protecting both
commercial and humanitarian interests. It would be clean, ecological and something to be proud of. Something to build
upon other innovative law changes and resulting innovative products and services.

Oh, yes, with our central location in Europe we could become also the transport hub of Europe. I even believe that this
is our current strategy and we are building highways for that. But is this really the best we can do? Have high prices
for road tax vignettes for all trucks and tourists going to Croatia (so that they really remember their short but
expensive moment in Slovenia)? Have polluted air? To be known with our only two lanes highways as a constipation of
Europe? Isn't it better to become the data hub of Europe?
