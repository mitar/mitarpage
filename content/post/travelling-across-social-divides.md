---
title: Travelling across social divides
publishDate: 2013-10-15 07:30:10-07:00
draft: false
tags:
- bart
- divide
- money
- ride
disqus_id: "64115840634"
slug: travelling-across-social-divides
aliases:
- /post/64115840634/travelling-across-social-divides
- /post/64115840634/
---

Few days ago I was taking a [BART](http://bart.gov/) and saw a guy going into the BART without the ticket and the
officer there complaining and not allowing him to go in. He was not lying or trying to make excuses. He said that he
does not have money but needs a ride.

<!--more-->

What I was thinking was that we have so many divides between classes. More money you have, more you can do. Less you
have, less you can do. How can one then move towards more money, if you are limited? Limited in your mobility, in your
Internet access, your education access ... Maybe just because I have all those resources at my disposal, my idea will
succeed but some others' idea will not. Because I can travel, I can present the idea, I can network with other people.
But then, maybe somebody else has a better idea, better concept, but he or she cannot even move from one part of the San
Francisco to another. Maybe even cannot go to work.

You have people who have money and they can invest it. They can give this person with an idea money to be able to travel
and able to go to Internet and be able to make his or her idea come true. But then most of the money the idea gets back
goes to the person funding, makes him or her even richer. So it all depends on which side of the divide you start.
Crossing it, that is hard. Getting more and more apart, this is easy.
