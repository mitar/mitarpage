---
title: Presentation of PeerLibrary at Creative Commons Global Summit 2013
icon: photo
publishDate: 2013-08-29 09:31:10-07:00
draft: false
tags:
- peerlibrary
- creativecommons
- summit
- presentation
- ccsum
- project
disqus_id: "59688659513"
slug: presentation-of-peerlibrary-at-creative-commons
aliases:
- /post/59688659513/presentation-of-peerlibrary-and-open-access-tools
- /post/59688659513/
---

[![](/post/peerlibrary-ccsum-2013.jpg)](/post/peerlibrary-ccsum-2013.jpg)

Presentation of [PeerLibrary](http://peerlibrary.org/) and open access tools panel at [Creative Commons Global Summit
2013](http://wiki.creativecommons.org/Global_Summit_2013) in Buenos Aires.
[Slides](http://www.slideshare.net/mmitar/peerlibrary-ccsummit).

via [peerlibrary](https://blog.peerlibrary.org/post/59688282260/presentation-of-peerlibrary-and-open-access-tools)

<!--more-->
