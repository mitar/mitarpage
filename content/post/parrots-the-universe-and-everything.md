---
title: Parrots the Universe and Everything
icon: video
publishDate: 2013-07-08 06:30:10-07:00
draft: false
tags:
- douglasadams
- ecology
- future
- nature
disqus_id: "54912657212"
slug: parrots-the-universe-and-everything
aliases:
- /post/54912657212/parrots-the-universe-and-everything-is-a-beautiful
- /post/54912657212/
---

<iframe width="570" height="427" class="add-margin" src="https://www.youtube-nocookie.com/embed/_ZG8HBuDjgc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Parrots the Universe and Everything* is a beautiful set of stories by Douglas Adams which show an interesting
perspective of relation between human animal and nature.

<!--more-->
