---
title: Crowdsourcing the language bubble breakout
publishDate: 2013-06-23 21:50:10-07:00
draft: false
tags:
- crowdsearching
- filterbubble
- languagebubble
- crowdsourcing
- search
- web
- idea
disqus_id: "53734497045"
slug: crowdsourcing-the-language-bubble-breakout
aliases:
- /post/53734497045/crowdsourcing-the-language-bubble-breakout
- /post/53734497045/
---

When searching in English you are reaching only English content on the web. But we could crowdsource search in other
languages to native speakers. Where is crowdsearching when you need it?

<!--more-->

Much have already been said about a [filter bubble](https://en.wikipedia.org/wiki/Filter_bubble). Search engines like
Google and other information feeds like Facebook personalize results to you so that you find easier information which is
relevant to you. This is both good and bad. Good is because often it works very well and you faster find information you
are searching for. Bad is, arguably, because it is locking you in the bubble of information you already know or is close
to what you already know. Your opinions and worldviews are thus reinforced as more and more information you are exposed
to aligns with existing opinions and worldviews.

Personally, I don't worry so much about this. I know how to [increase search results per page on
Google](https://support.google.com/websearch/answer/35892?hl=en#resultsperpage) so that I get 100 results displayed and
can very fast skim over all of them without the need to click through pages. I get information from many different
sources and especially by following links more than just one level deep. So even if you get initially personalized link,
further links are not personalized anymore, or at least not personalized in the same way. Have you ever opened [one
Wikipedia page to end up having dozens of tabs open](https://xkcd.com/214/)? Lastly, many service providers are well
aware of this and there are many ways they can help – like adding some random or not personalized results to your
results set, or even known opposites for you.

At the end it all sums to your personal approach of dealing with conflicting information (and ensuing [cognitive
dissonance](https://en.wikipedia.org/wiki/Cognitive_dissonance)). Do you embrace it, try to understand it and include it
into your understanding of the world, or do you just ignore it, or even ignore the signs of it? It was very similar in
the past. If you felt that there might be something more to the information at hand, you could just ignore this feeling
or you could go to libraries and try to find books which would tell you more, confirming or opposing the information you
already have. But it was still your decision how deep you wanted to search.

I am much more worried about the language bubble. We search for information only with keywords in languages we know and
we get results mostly in those languages as well. Even if we search in English, we are still reaching only a bit more
than [50% of the content on the Internet](https://en.wikipedia.org/wiki/Languages_used_on_the_Internet). So half of the
content is out of the reach. Half of human ideas, concepts, solutions, stories, knowledge.

This is not something new. For centuries knowledge was passing from country to country, from civilization to
civilization by translations done by scholars. But that took ages. Literally.

What I am missing now is a web portal where people could ask other people to help them find information in other
languages. Do you want to find what are current best open government practices in South America? What open source
projects are people in China developing? What open source tools are farmers in Africa using to collaborate, if any? It
is not really enough just to translate those keywords. Even when searching in English it is often not really enough just
to type some keywords to search on, but you have to see what results you are getting, skim them, iterate few times,
repeat, and then you maybe find an useful link. When searching in other languages it takes too much time even with
automatic tools helping you translate results. If you know that one page is relevant, than automatic translation can
give you some help to understand the page. But automatic translation is still too imprecise to navigate around fast
while searching.

Furthermore, by publicly asking native speakers for help when searching, multiple similar search queries could be
aggregated together. So most searched for queries could be done first. Even more, if there is no useful content already
available, native speakers could be even willing to prepare such content. For example, in Slovenia we have since the
beginning of 2013 [net neutrality](https://en.wikipedia.org/wiki/Net_neutrality) required by law. There is no official
translation (yet), so when we noticed that people are searching for the law and its translation to help them advocate
for a similar law in their countries, we [made an unofficial
translation](https://wlan-si.net/en/blog/2013/06/16/net-neutrality-in-slovenia/).

Such crowdsearching would help you find relevant similar information you might not know you are searching for. When you
search in a language you understand you often find additional resources of interest. But this works mostly by having
some background knowledge and understanding by which you can determine fast that some link could contain useful
information. Native speakers often have a better understanding of such useful cultural backgrounds and common knowledge
which can help improve quality of results – even those you have not asked for.
