---
title: Interesting presentation of DDOSing
icon: video
publishDate: 2014-02-03 17:47:10-07:00
draft: false
tags:
- ddos
- activism
- onlineactivism
- sitin
- ethics
disqus_id: "75541267511"
slug: interesting-presentation-of-ddosing-as-an-online
aliases:
- /post/75541267511/interesting-presentation-of-ddosing-as-an-online
- /post/75541267511/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/b9X_2IkKo5Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Interesting presentation of [DDOS](https://en.wikipedia.org/wiki/Denial-of-service_attack)ing as an online collective
action. Is it ethical? Activism? What are important aspects of user interface which allows participation?

<!--more-->
