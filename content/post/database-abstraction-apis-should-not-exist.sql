CREATE TABLE blog_entry (id INTEGER, title TEXT, body TEXT, pub_date TEXT, is_draft INTEGER, internal_comment TEXT, PRIMARY KEY (id));
CREATE TABLE blog_entry_owner_users (entry_id INTEGER, user_id INTEGER, PRIMARY KEY (entry_id, user_id));
CREATE TABLE blog_entry_owner_groups (entry_id INTEGER, group_id INTEGER, PRIMARY KEY (entry_id, group_id));
CREATE TABLE groups_to_users (user_id INTEGER, group_id INTEGER, PRIMARY KEY (user_id, group_id));

INSERT INTO blog_entry VALUES (1, "Title", "Body", "2016-02-01", true, "Internal comment");
INSERT INTO blog_entry_owner_users VALUES (1, 42);

-- IDs in the following queries are not correct.

SELECT DISTINCT id, title, body, pub_date, is_draft, internal_comment
  FROM blog_entry
    LEFT JOIN blog_entry_owner_users ON blog_entry.id = blog_entry_owner_users.entry_id
    LEFT JOIN blog_entry_owner_groups ON blog_entry.id = blog_entry_owner_groups.entry_id
    LEFT JOIN groups_to_users ON blog_entry_owner_groups.group_id = groups_to_users.group_id
  WHERE is_draft = true
    AND (
      blog_entry_owner_users.user_id = 42
      OR groups_to_users.user_id = 42
    );

SELECT DISTINCT id, title, body, pub_date, is_draft,
    CASE blog_entry_owner_users.user_id = 42
        OR groups_to_users.user_id = 42
      WHEN true THEN internal_comment
    END AS internal_comment
  FROM blog_entry
    LEFT JOIN blog_entry_owner_users ON blog_entry.id = blog_entry_owner_users.entry_id
    LEFT JOIN blog_entry_owner_groups ON blog_entry.id = blog_entry_owner_groups.entry_id
    LEFT JOIN groups_to_users ON blog_entry_owner_groups.group_id = groups_to_users.group_id
  WHERE id = 42
    AND (
      is_draft = false
      OR (
        is_draft = true
        AND (
          blog_entry_owner_users.user_id = 42
          OR groups_to_users.user_id = 42
        )
      )
    );

CREATE TABLE blog_entry (id INTEGER, version INTEGER, title TEXT, body TEXT, pub_date TEXT, is_draft INTEGER, internal_comment TEXT, PRIMARY KEY (id, version));

INSERT INTO blog_entry VALUES (1, 0, "Title", "Body", "2016-02-01", true, "Internal comment");

INSERT INTO blog_entry
  SELECT b1.id, b1.version + 1, "Hello world",
      b1.body, b1.pub_date, b1.is_draft, b1.internal_comment
    FROM blog_entry b1
    WHERE b1.id = 42
      AND b1.version = (
        SELECT MAX(version) FROM blog_entry b2 WHERE b2.id = b1.id
      );
