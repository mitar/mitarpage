---
title: Exposer
icon: link
publishDate: 2013-03-29 00:25:10-07:00
draft: false
tags:
- project
- exposer
- facebook
- twitter
- demonstrations
- sexism
- protests
disqus_id: "46577942338"
slug: exposer
aliases:
- /post/46577942338/exposer
- /post/46577942338/
---

[Exposer](https://github.com/mitar/exposer) collects all social posts from Facebook and Twitter containing keywords and
displays them aggregated and in real-time. Useful to follow everything around those keywords. [See live
instance](http://exposer.tnode.com/) of all posts about demonstrations in Slovenia. [And another collecting tweet
testimonials of sexism](http://aufschrei.tnode.com/).

<!--more-->
