---
title: 'Crowdfunding or funding the crowds: a new model for the distribution of wealth?'
icon: link
publishDate: 2013-06-24 06:30:10-07:00
draft: false
tags:
- crowdfunding
- critique
- work
- economy
- funding
disqus_id: "53755643217"
slug: crowdfunding-or-funding-the-crowds-a-new-model
aliases:
- /post/53755643217/crowdfunding-or-funding-the-crowds-a-new-model
- /post/53755643217/
---

An interesting [critique of crowdfunding](http://www.aprja.net/crowdfunding-or-funding-the-crowds-a-new-model-for-the-distribution-of-wealth/).

> If one actually paid a decent wage to all of the people involved in helping with the campaign, one would be loosing
> much of the money raised. The production costs of organizing a €10000 campaign will have already used up some of the
> funds for the project that is to be executed when funded. The actual costs of organizing, raising money and carrying out
> the campaign are therefore not funded. It is the people who actually do most of the work (the campaigner) who are not
> paid enough (if anything at all) and then only if the project is successful, i.e. if the amount of funding requested has
> been raised.

<!--more-->
