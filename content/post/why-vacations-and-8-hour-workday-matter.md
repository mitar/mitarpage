---
title: Why vacations and 8 hour workday matter
publishDate: 2015-06-15 03:23:10-07:00
draft: false
tags:
- vacations
- freetime
disqus_id: "121663860261"
slug: why-vacations-and-8-hour-workday-matter
aliases:
- /post/121663860261/why-vacations-and-8-hour-workday-matter
- /post/121663860261/
---

Vacations and the 8 hour workday is a great achievement of a human society. It might not be seen as something
revolutionary, but it is. It is a gentlemen's agreement. Despite some of us being able to work more, at least sometimes,
during some periods of our life, we collectively agree that we will not. This is not because we are lazy. But because we
want to protect those who cannot afford to work more. 8 hours is an arbitrary decision ([we could work
less](/post/in-praise-of-idleness-by-bertrand-russell)), but it is an important one.

<!--more-->

I hear the argument that why would anyone work less if you can work more? Why limit yourself? The main answer to this is
that you are not always able to work more. Maybe currently you do not have a family. Maybe you do not have any other
interests. Maybe you do not want to volunteer for some cause. Maybe you do not read or continue learning. But maybe that
is just at this moment. Things change. So when we as a society put a limit on how many hours to work and we all respect
that limit, we allow all of us more time for other things. Maybe you do not see the benefit of that yet, but there will
be time when you will be glad for it. And because limit is for everyone, we can still compete the same, if we like to
compete. We can still determine who can do more in those 8 hours and who less. The only difference is that we also have
16 hours for other things. Like raising our children. Enjoying company of loved ones. Sleeping. Reading. Creating things
which are not connected to our vocation. I would even claim that having those 16 hours help us do better at those 8
hours of work.

But we are forgetting that this achievement is something we as a society created and like any other social contract can
work only if we believe in it. But today people are proud that they can work more. When they take work home. When they
try to minimize time they spend eating every day. And yes, there are times when some of us can do that. When we can work
more than somebody else. But this is a very short-sighted approach. Of course you can do more. We know that. But you
should not. And you do not know that.
