---
title: django-tastypie-mongoengine
icon: link
publishDate: 2013-03-28 23:22:10-07:00
draft: false
tags:
- project
- django
- mongodb
- tastypie
- mongoengine
- python
disqus_id: "46575468450"
slug: django-tastypie-mongoengine
aliases:
- /post/46575468450/django-tastypie-mongoengine
- /post/46575468450/
---

[This Django application](https://pypi.python.org/pypi/django-tastypie-mongoengine) provides
[MongoEngine](http://mongoengine.org/) support for [django-tastypie](https://github.com/toastdriven/django-tastypie).

<!--more-->
