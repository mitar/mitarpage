---
title: A panel on how to build community mesh networks
icon: video
publishDate: 2015-11-19 02:23:10-07:00
draft: false
tags:
- media
- wlanslovenija
- osb2014
- opensourcebridge
- communitynetworks
- wireless
- meshnetworks
disqus_id: "133522389871"
slug: a-panel-on-how-to-build-community-mesh-networks
aliases:
- /post/133522389871/a-panel-on-how-to-build-community-mesh-networks-at
- /post/133522389871/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/WK4M41DHclc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A panel on how to build community mesh networks at [Open Source Bridge 2014](http://opensourcebridge.org/y2014/) where
we discussed both technical and community aspects of such networks.

<!--more-->
