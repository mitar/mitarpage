---
title: UC Berkeley students lose their health care coverage if they participate in
  riots
publishDate: 2013-08-27 13:11:10-07:00
draft: false
tags:
- demonstrations
- riots
- healthcare
- berkeley
disqus_id: "59513999778"
slug: uc-berkeley-students-lose-their-health-care
aliases:
- /post/59513999778/uc-berkeley-students-lose-their-health-care
- /post/59513999778/
---

From [UC Berkeley health care plan](http://www.uhs.berkeley.edu/students/insurance/pdf/Berkeley_SHIP_Benefit_Booklet_2013-2014.pdf):

> This Plan does not cover nor provide benefits for:
>
> Expense incurred as a result of injury due to participation in a riot. "Participation in a riot" means taking part in
> a riot in any way; including inciting the riot or conspiring to incite it. It does not include actions taken in
> self-defense; so long as they are not taken against persons who are trying to restore law and order.

How can participation in legal or legitimate demonstrations (which is an expression of free speech) which turn violent
suppress your health insurance? One more nail in [the apathy
coffin](http://www.filmsforaction.org/news/8_reasons_young_americans_dont_fight_back_how_the_us_crushed_youth_resistance/).

<!--more-->
