---
title: PeerLibrary 0.1 released
publishDate: 2014-02-01 15:28:10-07:00
draft: false
tags:
- project
- peerlibrary
- beta
- preview
- release
disqus_id: "75302929104"
slug: peerlibrary-01-released
aliases:
- /post/75302929104/peerlibrary-01-released
- /post/75302929104/
---

We released a 0.1 version of PeerLibrary. [Check it out live](http://peerlibrary.org/). It is a really very beta
version, with many [planned features](https://github.com/peerlibrary/peerlibrary/issues/milestones) missing, but so that
you can get a better taste of what is coming. Please, [give us
feedback](https://github.com/peerlibrary/peerlibrary/issues/new).

via [peerlibrary](https://blog.peerlibrary.org/post/75302672962/version-01-released)

<!--more-->
