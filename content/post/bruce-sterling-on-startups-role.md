---
title: Bruce Sterling on startups' role in helping the global rich get richer
icon: video
publishDate: 2016-06-27 11:30:10-07:00
draft: false
tags:
- investors
- startupculture
- venturecapitalists
disqus_id: "146592906236"
slug: bruce-sterling-on-startups-role
aliases:
- /post/146592906236/bruce-sterling-on-startups-role-in-helping-the
- /post/146592906236/
---

<iframe src="https://video.nextconf.eu/v.ihtml?source=share&amp;photo%5fid=8066103" border="0" scrolling="no"
allowfullscreen="1" mozallowfullscreen="1" webkitallowfullscreen="1" width="570" height="320" frameborder="0" class="add-margin"></iframe>

> Bruce Sterling's speech from NEXT Berlin is a blast of cold air on the themes of startup life, disruption, and global
> collapse. Bruce excoriates the startup world for its complicity with the conspiracy of the global investor class to
> vastly increase the wealth of a tiny minority, and describes the role that "design fiction" has in changing this.

via [Boing Boing](http://boingboing.net/2013/04/29/bruce-sterling-on-startups-r.html)

<!--more-->
