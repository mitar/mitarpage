---
title: Beauty of the nature, in its many forms of beauty
icon: video
publishDate: 2013-07-13 13:30:10-07:00
draft: false
tags:
- richardfeynman
- science
- beauty
- nature
disqus_id: "55364349571"
slug: beauty-of-the-nature-in-its-many-forms-of-beauty
aliases:
- /post/55364349571/beauty-of-the-nature-in-its-many-forms-of-beauty
- /post/55364349571/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/cRmbwczTC6E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Beauty of the nature, in its many forms of beauty. Not just an aesthetic one. There is always something beautiful in
everything and everybody.

<!--more-->
