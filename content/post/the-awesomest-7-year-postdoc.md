---
title: The Awesomest 7-Year Postdoc
icon: link
publishDate: 2013-07-23 17:20:10-07:00
draft: false
tags:
- academia
- fun
- life
disqus_id: "56285003546"
slug: the-awesomest-7-year-postdoc
aliases:
- /post/56285003546/the-awesomest-7-year-postdoc-or-how-i-learned-to
- /post/56285003546/
---

[The Awesomest 7-Year Postdoc or: How I Learned to Stop Worrying and Love the Tenure-Track Faculty
Life](https://blogs.scientificamerican.com/guest-blog/the-awesomest-7-year-postdoc-or-how-i-learned-to-stop-worrying-and-love-the-tenure-track-faculty-life/).
How to have fun in academia. Or in life in general. I believe all those ideas and suggestions apply to the life in
general as well. To the meaningful and fun life. Be the best "whole" person you can.

<!--more-->
