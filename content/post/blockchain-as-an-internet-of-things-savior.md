---
title: Blockchain as an Internet of things savior?
publishDate: 2016-07-19 15:50:10-07:00
draft: false
tags:
- blockchain
- iot
- opennetworks
- netneutrality
disqus_id: "147665528556"
slug: blockchain-as-an-internet-of-things-savior
aliases:
- /post/147665528556/blockchain-as-an-internet-of-things-savior
- /post/147665528556/
---

[Internet of things](https://en.wikipedia.org/wiki/Internet_of_things) (IoT) has some big issues. Interoperability is
one. And potential [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in) and [data
silos](https://en.wikipedia.org/wiki/Information_silo) are another.

But I do not buy that [blockchain is the solution to those
problems](https://medium.com/outlier-ventures-io/why-i-am-dedicating-my-career-to-blockchains-8364bd64a140).

<!--more-->

I have heard so many times the idea of combining blockchain and IoT, but I do not get it. IoT will be producing
potentially much more data that anything we had before, and a lot of that data will be privacy sensitive. How can a
public and decentralized technology with scaling issues help here?

It is true that data on a blockchain is public and interoperability seems easier if everyone agrees on one blockchain.
But those properties are not the fundamental properties of a blockchain. Any common technology, if parties agree to use
it, improves interoperability. But nobody assures that data stored on a blockchain would be in open standard. And what
when people will encrypt data stored on a blockchain to address the privacy concerns with IoT data? How does this
prevent data silos?

There are many other ways to address issues of interoperability and data access. One I like is that [gateways for IoT
should not be black boxes but something end-users can
control](http://www.shareconference.net/sh/predavanja/internet-things-rob-van-kranenbrug-0). You can control your home
Internet router, and you should be able to control your home IoT router. If you can do that, you can prevent data silos
and you can assure interoperability with those you want. Furthermore, we could enforce it through regulation (require
open standards for IoT), instead of trying to get IoT companies to adopt a new technology which is not even yet ready
for this use case. Why would those companies do that?

The issues here are to me easier solved through regulation than through technology. And if we want a technological
solution, then open IoT gateways are a better approach. What we need is a net neutrality movement for IoT and not a
blockchain.
