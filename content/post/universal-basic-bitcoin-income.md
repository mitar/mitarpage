---
title: Universal basic Bitcoin income
publishDate: 2014-03-26 07:30:10-07:00
draft: false
tags:
- bitcoin
- socialexperiment
- universalbasicincome
- experiment
disqus_id: "80778859588"
slug: universal-basic-bitcoin-income
aliases:
- /post/80778859588/universal-basic-bitcoin-income
- /post/80778859588/
---

I have a proposal for a double social experiment, merging two disruptive ideas. One is that [Bitcoin
technology](https://bitcoin.org/) is removing government from the equation of controlling and taxing money flows, which
is arguably better because it allows people to freely decide how they want their money to be spend and not be forced by
others. The other is [universal basic income](https://en.wikipedia.org/wiki/Basic_income), a system of social security
where everybody regularly receives an unconditional sum of money. Let's create a way for all Bitcoin users to
voluntarily add Bitcoins to a common wallet from which all users would then unconditionally get a regular and equal
payment.

<!--more-->

Argument is that if we would not force people to contribute to social welfare through taxes they would still
philanthropically donate enough. That we just have to allow everyone to decide on their own how much to contribute and
to which socially important goal or service. Would we have enough Bitcoins donated to the common wallet for payments to
be of any significant value? Above minimal salary? On the other side, would getting a regular and equal payment really
improve the economical situation of everyone, giving them more freedom to do with their lives whatever they would want
to? Or would those payments just offset a basic cost of everything for exactly the same amount, not really changing
anything? Rents on housing would just increase for exactly that amount, making those payments automatically flow back
into the hands of those who own housing?

(An open question is how to make it so that only real physical persons receive a regular payment, because one could
otherwise create multiple fake Bitcoin users to receive payments multiple times.)
