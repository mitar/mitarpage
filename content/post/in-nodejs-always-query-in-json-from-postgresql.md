---
title: In node.js, always query in JSON from PostgreSQL
publishDate: 2019-01-10 01:51:10-07:00
draft: false
tags:
- javascript
- benchmark
- nodejs
- postgresql
disqus_id: "181893159351"
slug: in-nodejs-always-query-in-json-from-postgresql
aliases:
- /post/181893159351/in-nodejs-always-query-in-json-from-postgresql
- /post/181893159351/
---

Recently I was exploring the use of PostgreSQL as a replacement for MongoDB.
PostgreSQL has in recent versions great support for JSON.
You can store JSON values and you can even make indices on JSON fields.
When combined with node.js and its [driver](https://node-postgres.com/)
things look almost magical.
You read from PostgreSQL and you get automatically a JavaScript object,
JSON fields automatically embedded. But can we also use JSON for transporting
results of queries themselves, especially joins?
In MongoDB the idea is to embed such related documents. In PostgreSQL we could also
embed them instead of joining them, but would that be faster?
I made a benchmark to get answers.

<!--more-->

[Source code](https://github.com/mitar/node-pg-json-benchmark) and
[results](https://github.com/mitar/node-pg-json-benchmark/blob/master/results.csv) are available,
for JS driver and for native driver. First
I [populated the database](https://github.com/mitar/node-pg-json-benchmark/blob/master/populate.js) with two
tables, _posts_ and _comments_, where each _comment_ has a related _post_. I generated 10000 _posts_, each
with 100 _comments_.
I tested [these queries](https://github.com/mitar/node-pg-json-benchmark/blob/master/benchmark.js).

<table class="add-margin">
  <thead>
    <tr>
      <th rowspan="2"></th>
      <th colspan="3" style="text-align: center;">js</th>
      <th colspan="3" style="text-align: center;">native</th>
    </tr>
    <tr>
      <th style="text-align: center;">raw</th>
      <th style="text-align: center;">json</th>
      <th style="text-align: center;">jsonb</th>
      <th style="text-align: center;">raw</th>
      <th style="text-align: center;">json</th>
      <th style="text-align: center;">jsonb</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>posts</strong></td>
      <td style="text-align: right;">48.6</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">24.1</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">28.2</td>
      <td style="text-align: right;">45.5</td>
      <td style="text-align: right;">33.3</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">40.2</td>
    </tr>
    <tr>
      <td><strong>comments</strong></td>
      <td style="text-align: right;">4208.5</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2957.2</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">3752.9</td>
      <td style="text-align: right;">4457.8</td>
      <td style="text-align: right;">4636.8</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">5214.7</td>
    </tr>
    <tr>
      <td><strong>JOIN</strong></td>
      <td style="text-align: right;">7387.2</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">4661.5</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">5747.4</td>
      <td style="text-align: right;">8509.2</td>
      <td style="text-align: right;">7137.4</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">8165.3</td>
    </tr>
    <tr>
      <td><strong>JOIN + array_agg</strong></td>
      <td style="text-align: right;">2828.6</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2967.4</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">3919.1</td>
      <td style="text-align: right;">2956.3</td>
      <td style="text-align: right;">4015.4</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">5091.2</td>
    </tr>
    <tr>
      <td><strong>JOIN + to_json(array_agg)</strong></td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2847.6</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2822.4</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);"></td>
      <td style="text-align: right;">4305.4</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">4278.5</td>
      <td style="border: 1px solid lightgray;"></td>
    </tr>
    <tr>
      <td><strong>JOIN + to_jsonb(array_agg)</strong></td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">3640</td>
      <td style="text-align: right;"></td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">4246.4</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">5015.8</td>
      <td style="text-align: right;"></td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">5824.2</td>
    </tr>
    <tr>
      <td><strong>SUBQUERY + array_agg</strong></td>
      <td style="text-align: right;">2696.4</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2726.3</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">3530.5</td>
      <td style="text-align: right;">3030.1</td>
      <td style="text-align: right;">4125.5</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">4513.3</td>
    </tr>
    <tr>
      <td><strong>SUBQUERY + to_json(array_agg)</strong></td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2690.4</td>
      <td style="text-align: right; background-color: rgb(217, 234, 211);">2729.9</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);"></td>
      <td style="text-align: right;">3751.2</td>
      <td style="text-align: right;">3765.6</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);"></td>
    </tr>
    <tr>
      <td><strong>SUBQUERY + to_jsonb(array_agg)</strong></td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">3525.8</td>
      <td style="text-align: right;"></td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">4776.6</td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">4828.2</td>
      <td style="text-align: right;"></td>
      <td style="text-align: right; background-color: rgb(244, 204, 204);">6047.6</td>
    </tr>
  </tbody>
</table>

<span style="background-color: rgb(217, 234, 211)">Green color</span> marks combinations I would recommend.
<span style="background-color: rgb(244, 204, 204)">Red color</span> shows JSONB should be avoided.

My insights from the benchmark are:

* JavaScript driver is surprisingly faster than a native driver. Even just for a simple _SELECT * FROM comments_. So I will
  focus just on JavaScript driver and would not recommend using native driver at all.
* We can immediately see that converting query results to JSON in the database and then sending them over is
  generally much faster then sending over raw PostgreSQL fields. I believe this is because JSON parsing is so much faster
  in JavaScript than parsing of PostgreSQL field types. We can see that using native driver there is no such boost.
* Doing a traditional JOIN without aggregation is really slow. I attribute this to the fact that traditional join repeats all
  column values in the main table. So for 100 _comments_, related _post_ 's row is repeated 100 times.
  Doing aggregation of _comments_ into an array seems to really improve things. MongoDB's idea of embedding is really
  powerful and you can see benefits even here.
* While using JSONB for storing JSON in PostgreSQL and even having indices on JSON fields is very cool, but using it
  to transport results to the client is not a good idea. Again, probably because JavaScript's JSON parsing cannot be used.
* Subquery is faster than JOIN. A surprise because the word is that subqueries can be at best as fast as joins, but
  sometimes they will be worse. Here, there are better.
* It seems that the best approach is to use subqueries to get related documents, aggregate them into an array, and
  convert the array to JSON. Or, you can be lazy and just simply always convert the whole result (with aggregated arrays) to JSON.
* Important to note is that while it looks that using a subquery and just aggregation is the fastest, the results
  you get on the client are not really parsed into a JavaScript object, because the
  [driver does not parse embedded records](https://github.com/brianc/node-postgres/issues/1801).
  Probably parsing them properly would add quite a bit.

Conclusion. If you are using PostgreSQL fields which can be reasonably converted to JSON and you are using node.js,
it seems you should simply always convert results to JSON before sending them over to the client. If you want to fetch
related documents, do not do JOIN but do a subquery and aggregate results into an array. If you are always converting
results to JSON, then this is it. If not, then at least convert that aggregated array to JSON.
