---
title: cmsplugin-markup-tracwiki
icon: link
publishDate: 2013-03-28 22:02:10-07:00
draft: false
tags:
- project
- djangocms
- django
- python
- markup
- trac
- wiki
disqus_id: "46570997364"
slug: cmsplugin-markup-tracwiki
aliases:
- /post/46570997364/cmsplugin-markup-tracwiki
- /post/46570997364/
---

[A plugin](https://pypi.python.org/pypi/cmsplugin-markup-tracwiki) for
[cmsplugin-markup](https://pypi.python.org/pypi/cmsplugin-markup) which adds [Trac](http://trac.edgewall.org/) wiki
engine support to [Django CMS](https://www.django-cms.org/). This means you can have content in Trac wiki syntax with
all the power of it – Trac plugins and macros.

<!--more-->
