---
title: Limitations of Bitcoin revolution
publishDate: 2015-07-22 13:02:10-07:00
draft: false
tags:
- bitcoin
- innovation
- blockchain
- cryptocurrencies
disqus_id: "124772154021"
slug: limitations-of-bitcoin-revolution
aliases:
- /post/124772154021/limitations-of-bitcoin-revolution
- /post/124772154021/
---

I really like how Bitcoin brought up so much new energy into the cryptocurrencies world. It combined great ideas of P2P
networks, proof of work, append-only databases, and others together into a working and popular system and spawn new
ideas and new innovation. It started a new cycle of decentralization of technologies.

But as a currency itself, it lacks innovation. It is still an one-dimensional numeric-like value used to valuate
everything humans do in the same traditional way. Bitcoin is often compared to the Internet, how it brings a new era.
But if we are comparing it with the Internet, then it is the Internet at the stage where people would take physical
books, scan them as images, and make them available online. No interactivity, no multimedia, no searching or social
capabilities, no additional value besides digitizing the content in the most simple way. Bitcoin does the same. It just
digitizes existing concept of money. It does this in an interesting and innovative way, but it is still just a digital
image of money.

<!--more-->

It is important to notice that traditional money was already a form of the Internet. A global information exchange
network exchanging numerical values of transactions. In fact, it was already digital (numeric), with various transport
layers (gold, coins, banknotes, and recently Internet itself). But it is not so fancy and is full of rules and
conditions if you want to participate in this global network. So a reboot was necessary.

Now we got a different implementation, decentralized one, using modern computer science techniques, but the content is
still the same. Despite being modern, transactions still record only a numerical value/valuation of the transaction
itself. It is still a great reduction of information of what exactly the transaction was about. Such reduction was maybe
reasonable for traditional money, where technology allowed passing around only one number, but today we should [move
on](/post/a-new-global-economic-system).

Bitcoin innovation is continuing, with all new projects building on ideas, trying new ideas, expanding and diversifying
the ecosystem. [Ethereum](https://ethereum.org/) is for example building a general purpose computing platform on top of
these ideas. All this is beautiful. But we should also innovate what money itself is, and [do we even need it in such
form](/post/a-new-global-economic-system)? Bitcoin revolution should not be just a
technological innovation. It should start asking questions about the nature of money itself.
