---
title: Presentation of PeerLibrary at iAnnotate 2014 conference
icon: video
publishDate: 2014-04-17 10:45:10-07:00
draft: false
tags:
- media
- peerlibrary
- project
- demo
- presentation
- openaccess
disqus_id: "83010499852"
slug: presentation-of-peerlibrary-at-iannotate-2014
aliases:
- /post/83010499852/presentation-of-peerlibrary-at-iannotate-2014
- /post/83010499852/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/wYLfNzJu2aQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Presentation of PeerLibrary at [iAnnotate 2014](http://iannotate.org/) conference in San Francisco. Including the demo
of current version in development, 0.2.

via [peerlibrary](https://blog.peerlibrary.org/post/83010269563/presentation-of-peerlibrary-at-iannotate-2014)

<!--more-->
