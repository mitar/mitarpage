---
title: Do Publicly Owned, Planned Economies Work?
icon: link
publishDate: 2016-06-22 10:30:10-07:00
draft: false
tags:
- ussr
- sovietunion
- capitalism
- economy
- gdp
- diversity
disqus_id: "146314072245"
slug: do-publicly-owned-planned-economies-work
aliases:
- /post/146314072245/do-publicly-owned-planned-economies-work
- /post/146314072245/
---

A [post](https://gowans.blog/2012/12/21/do-publicly-owned-planned-economies-work/) by [Stephen Gowans](https://gowans.blog/).

What we need to progress as a human civilization is more diversity in how we are organizing ourselves, in how we run our
economies, and in which values we pursue. We need a global understanding that such diversity is important and we should
establish ways for countries to try and experiment with different paths. Even if we do not believe that their path is
correct we should be proud that they are walking it and we should help them if needed. We will all learn as a result.

If we really believe that our system is better, do we really need to actively try to undermine the other system? We
could even help it, if they need anything, and once it will collapse (if, but our system is better and the only with a
future!), with all circumstances being positive for it, including us helping, we will know for sure that it is really a
worse system. We can even then help those brave explorers who hit the floor. But, until then, let us have some diversity
and encouragement for each other when trying something new.

The fact that [Spartacus](https://en.wikipedia.org/wiki/Spartacus) lost a war does not mean that slavery is a great
system. How many more years of exploration of various systems were needed before we realized that?

<!--more-->
