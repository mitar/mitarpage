---
title: Why programming is like writing poems?
publishDate: 2015-11-04 01:38:10-07:00
draft: false
tags:
- poetry
- programming
- learning
disqus_id: "132528896301"
slug: why-programming-is-like-writing-poems
aliases:
- /post/132528896301/why-programming-is-like-writing-poems
- /post/132528896301/
---

When friends ask me how they could improve their programming skills I often reply that they should imagine that they are
writing poems. How do poets improve their skill and quality of their poems? They read many more poems than they write.
Reading what masters produced in the past can give you insights into ideas, patterns, and structure. When was the last
time you went and read how something is implemented?

<!--more-->

People often say that they do not like reading the code made by others because it is hard to understand. But this is
similar to reading poetry as well. Sometimes it is hard to understand. But poetry done by masters gives you some
understanding at any level of reading. Read great code made by masters. More you will read, easier it will be also to
read code made by everyone. Do you have an issue that the project you want to use has lacking documentation? No problem,
with your skill of code reading you can understand how to use the project by just reading the code. Code is
documentation as well and the source of truth.

Personally, I first noticed what a difference it makes when reading [Django](https://github.com/django/django/)'s code.
Despite its large size and complexity it was always clear what a particular small piece of the code does and how it fits
a larger structure. Reading (and thus contributing) was really easy and I learned many new things. It is also very
useful to use a smart IDE like [PyCharm](https://www.jetbrains.com/pycharm/) which allows you to easily click through
the code to definitions of functions. It is like reading literature with footnotes which gives you relevant context and
references to other parts to easier understand what is happening in a given segment.

Moreover, there are some other similarities between programs and poems, or programs and literature works in general. One
great and smart line does not make a poem. It has to fit the whole poem, its structure. It has to be clear how you get
to it and how you get to the next line. Do not try to make smart one-liners. Make great poems.

Larger software is also similar to larger works. Split into chapters, sections, paragraphs or stanzas. There is some
overarching story, idea.

If you look at functions, good functions have a start, then body, and the end. They do not have multiple bodies, doing
different things.

Also, after you publish one book, you might have to write the next one, which should be compatible with the previous
one. You should not change the history of your characters in the next book.

People might adopt your work to different media, your story should work there as well.
