---
title: My presentation of the PeerLibrary at Meteor Devshop 3
icon: video
publishDate: 2013-05-08 11:13:10-07:00
draft: false
tags:
- media
- peerlibrary
- openaccess
- meteorjs
- project
disqus_id: "49945733019"
slug: my-presentation-of-the-peerlibrary-at-meteor
aliases:
- /post/49945733019/my-presentation-of-the-peerlibrary-at-meteor
- /post/49945733019/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/7TYodA2RAm4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

My presentation of the [PeerLibrary](http://peerlibrary.org/) at [ Meteor Devshop
3](http://www.meteor.com/blog/2013/05/06/meteor-devshop-3-3-collaborative-ides-2-hackathon-winners-a-preview-of-meteor-ui).
PeerLibrary aims to enrich the experience of open access scholarly literature by combining real-time discussions with
scientific publications. [GitHub](https://github.com/peerlibrary/peerlibrary).

<!--more-->
