---
title: Plugins for Trac
icon: link
publishDate: 2013-03-23 02:58:10-07:00
draft: false
tags:
- project
- trac
- python
disqus_id: "46061476361"
slug: plugins-for-trac
aliases:
- /post/46061476361/plugins-for-trac
- /post/46061476361/
---

I [made, forked and improved, or contributed](http://trac-hacks.org/wiki/mitar) to many
[Trac](http://trac.edgewall.org/) plugins with emphasis on improving workflow of collaborative processes in projects
using Trac to coordinate their efforts.

<!--more-->
