---
title: Cargo Cult Science
icon: link
publishDate: 2014-05-17 18:05:10-07:00
draft: false
tags:
- richardfeynman
- caltech
- commencement
disqus_id: "86059980691"
slug: cargo-cult-science
aliases:
- /post/86059980691/cargo-cult-science
- /post/86059980691/
---

Feynman's 1974 [Caltech commencement address](https://lockhaven.edu/~DSIMANEK/cargocul.htm).

<!--more-->
