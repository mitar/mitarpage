---
title: Store IDs in MongoDB as binary or as string?
publishDate: 2020-11-07 07:51:12.910650+00:00
draft: false
tags:
- mongodb
- compression
- benchmark
slug: store-ids-in-mongodb-as-binary-or-as-string
---

I was curious if [MongoDB compression](https://docs.mongodb.com/manual/core/wiredtiger/#compression)
can efficiently store IDs if they are represented as string instead in
a more compact binary form. So I made a [benchmark](https://gitlab.com/mitar/benchmark-mongo-id)
and measure compression performance of three available
compressors: zlib, snappy, and zstd.

<!--more-->

Results for MongoDB 4.4.1 of storing 128 bit random values (e.g., UUIDs) as binary (16 bytes) or as base-58 encoded (22 characters):

<table style="font-size: 75%;" class="add-extra-margin">
<thead>
<tr>
<th></th>
<th>Binary none</th>
<th>String none</th>
<th>Binary snappy</th>
<th>String snappy</th>
<th>Binary zlib</th>
<th>String zlib</th>
<th>Binary zstd</th>
<th>String zstd</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>size</strong></td>
<td>3100000</td>
<td>3697229</td>
<td>3100000</td>
<td>3697196</td>
<td>3100000</td>
<td>3697150</td>
<td>3100000</td>
<td>3697196</td>
</tr>
<tr>
<td><strong>count</strong></td>
<td>100000</td>
<td>100000</td>
<td>100000</td>
<td>100000</td>
<td>100000</td>
<td>100000</td>
<td>100000</td>
<td>100000</td>
</tr>
<tr>
<td><strong>avgObjSize</strong></td>
<td>31</td>
<td>36</td>
<td>31</td>
<td>36</td>
<td>31</td>
<td>36</td>
<td>31</td>
<td>36</td>
</tr>
<tr>
<td><strong>storageSize</strong></td>
<td>3645440</td>
<td>4243456</td>
<td>2404352</td>
<td>3022848</td>
<td>2142208</td>
<td>2203648</td>
<td>1892352</td>
<td>2080768</td>
</tr>
<tr>
<td><strong>totalIndexSize</strong></td>
<td>2523136</td>
<td>3325952</td>
<td>2519040</td>
<td>3330048</td>
<td>2519040</td>
<td>3334144</td>
<td>2531328</td>
<td>3330048</td>
</tr>
<tr>
<td><strong>totalSize</strong></td>
<td>6168576</td>
<td>7569408</td>
<td>4923392</td>
<td>6352896</td>
<td>4661248</td>
<td>5537792</td>
<td>4423680</td>
<td>5410816</td>
</tr>
</tbody>
</table>

[zstd](https://github.com/facebook/zstd) compression looks really good. Moreover, it is clear that **storing values as binary
is more efficient than as string, even with compression, because compression can compress also
binary representation despite values being random**. The most compressed string size (zstd, 5410816 B)
is still larger than the least compressed binary size (snappy, 4923392 B). Do note though that
zlib compressed string (5537792 B) and zstd compressed string (5410816 B) are smaller than
uncompressed binary (6168576 B), meaning that those compression algorithms can recover storage
lost in string representation. But given that they can compress binary values even more, it seems
there are still things to improve in those algorithms.

Note: Compression algorithms generally perform poorly on small data and here we had very small
object sizes. This means that insights here cannot be generalized to performance with larger
amounts of binary (or string) data stored in MongoDB. (MongoDB does combine objects into blocks
to compress to alleviate this issue.)
