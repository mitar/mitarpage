---
title: Massive online collaborative decision-making system
publishDate: 2014-01-20 00:01:10-07:00
draft: false
tags:
- democracy
- collaborativewritting
- collaboration
- voting
- decissionmaking
- onlinevoting
- project
disqus_id: "73930434538"
slug: massive-online-collaborative-decision-making
aliases:
- /post/73930434538/massive-online-collaborative-decision-making
- /post/73930434538/
---

I am publishing a longer document describing one my idea of a massive online collaborative decision-making system. I
worked on this idea last year, but for now I put it on hold focusing on other projects. Still, I think there is some
value in sharing it.

<!--more-->

## Introduction ##

Doing group decisions can on the Internet be a much more common occurrence than in traditional contexts of, for example,
government elections. Many Internet communities, web portals and services regularly use some sort of user feedback to
make decisions, select or moderate the content, etc. But those systems tend not to leverage its online context but are
just adapting traditional offline approaches.

In the offline context group decision-making is done rarely, commences with fixed and limited in number options to
choose among (legacy of paper-based voting) and have an in-advance defined timespan (legacy of manually counting and
organizational limitations). Additionally, inherent assumptions in current democracy processes are that everybody knows
how and has time to make informed decisions.

In the online context all this is not necessary true anymore. Because decision-making feedback can be in real-time, it
is much more reasonable to want to do changes, propose new options or variations, while the decision-making is in
progress. Thus, options to choose among can be numerous, new options constantly being added and removed. Communities can
be both tightly or loosely knit, users more or less engaged, informed, with more or less time to participate, especially
in prolonged processes. It is hard to decide on the best timespan for the decision-making process, especially when users
are spread over multiple timezones with various commitment levels to the community, and issues are of various importance
to the community.

Current online systems concentrate on security/privacy ([Helios](http://heliosvoting.org/),
[Civitas](https://www.cs.cornell.edu/projects/civitas/)), experiment with more complicated voting schemes
([CIVS](http://civs.cs.cornell.edu/)), use delegation between users ([LiquidFeedback](http://liquidfeedback.org/)), or
try to engage users in deliberative interaction ([Deliberative Polling](http://cdd.stanford.edu/polls/)). But none is a
generalization and combination of those efforts which would really leverage the Internet medium.

We want to design a reasonable model of an online voting system and implement a proof-of-concept application – a massive
online collaborative decision-making system where users can collaboratively author texts on controversial topics in a
democratic and deliberative manner.

## Motivation ##

We believe that group decision making should be done while in the stage of collaborative text authoring on which to make
decision to support or oppose it. That if community is democratically involved in authoring of a text then the decision
making for accepting the text is just a formality, community already agreeing on its content.

Wikipedia is currently the most widely used collaborative text authoring system. But while it is suitable for an
encyclopedia and use cases where people more or less just want to collect and combine information, it fails miserably
when used on a controversial topic. Its main tool to determine which version of the text is available to others is
persistence of users – the user with more persistence to edit the text will have his or her version prevail over
others'. The more persistent/stronger wins. This is mitigated by community-appointed authority figures who patrol
Wikipedia and make sure such disputes are peacefully discussed and resolved. In the encyclopedia context this is maybe
reasonable as arguably it can be determined which version is true and which is not. But in more controversial topics or
topics where the truth is not really known and is a matter of public opinion, Wikipedia style of editing fails.

Furthermore, Wikipedia style breaks when there are large number of concurrent users wanting to contribute. Moreover, it
does not support different ways of contribution: some might want to contribute the content, changes to the text, but
others would just like to say whether current or proposed text is something they agree on or not, to direct its
evolution.

## User-facing interface ##

Design principles:

* user friendly, easy to use, intuitive
* real-time interaction and feedback
* collaborative, users should be able to directly interact with other users, "see & feel the community"
* minimization of "operation mode" switching, e.g., discussions and voting should not be separated from the content; if necessary, switching should be done automatically and in a transitive and intuitive manner
* interaction and voting should be made pervasive, something users can easily do while browsing around the portal (similar to "liking" on Facebook)
* no need to understand the technical background of the underlying voting system to be able to use the interface (but of course they can, the system is open source, algorithms used available, documented and published, and also some statistics and visualizations are available through the interface itself)

Based on this we are proposing a new kind of interface for collaborative text authoring. It consists of the current
version of the text in question and proposed changes to be included into future versions. Users can vote on those
proposed changes, discuss them, and once a change gets enough support it is applied.

[![](/post/voting.png)](/post/voting.png)

On the left we can see the current version of the text. On the right we initially have a list of proposed changes
ordered by how community currently votes on them. Those to the top are those which will sooner be applied. Uses can see
avatars of who all is participating in the change authoring (each change itself can be authored by multiple users),
current score for the change, what is the current vote for the user computed from his or her delegates in the case of
delegation, and four buttons, upvote, abstain, delegate, downvote. Default can be abstain or delegate, based on
site/user preferences. Hovering over each change shows in the panel on the left what the change adds and removes, with
that text emphasized.

In current online group decision making we found out that too much time is spent on discussing without concrete results.
The idea of our interface is to encourage concrete improvements to the topic at hand. Instead of people just discussing
and discussing what should be done, what should be changed, they can just propose a change directly. So that once
through decision process (both discussion and voting) some support is reached, there is an immediate effect on the
common result. In this way a collaborative text is also automatically summary of all discussions and users who later on
start participating can immediately see current status and continue from there, while still be able to traverse the
history (under "History" option) and see why some text part is as it is.

[![](/post/collaboration.png)](/post/collaboration.png)

To facilitate all this, users can easily start editing the text directly in the left panel. They can decide to do this
in private, invite others to collaborate, or do it publicly for everyone to participate. Collaboration is in real-time,
similar to Google Docs. Once a user or users are satisfied with the change, they can submit it to the vote. Those
changes can be listed under "My changes".

Clicking on a change listed in the right panel, discussion about the change is displayed below the change in the right
panel. Users can vote on the comments in the discussion as well in the same manner as on changes themselves. Those votes
are used to select relevant and community-supported comments.

[![](/post/preference.png)](/post/preference.png)

User can open the list of all his or her votes ("My votes") and refine the order of them. The list is split into two
sections, upvotes above and downvotes below. With drag and drop user can select the preference among them. Initially,
preference is based on his or her delegates. Optionally, user can also see a list of changes from which he or she
abstained.

## Backend voting system ##

The user-interface shows the needed requirements for the underlying voting system, but at the same time it is just one
use case of our Internet-centered voting system. The voting system is general and can be used in many scenarios.

The voting system supports the following:

* vote for or against an option, abstain from the option, or delegate the vote
* ranking of options
* adding and removing options at any time
* support for transitive delegation for votes and ranking
* real-time intermediate results
* various strategies for determining when and how voting is concluded, including indefinitely open voting

The voting system allows not just voting for or against an option, but also to abstain from the vote or to delegate a
vote to your network of delegates. The main idea of the voting system is to allow users to explicitly declare why they
are not for or against the option. Is this because they object to the option as a whole, or is this because they know
they do not know how to decide, but they still want some say in the decision. In our voting system, users can in the
latter case delegate their vote to their delegation network.

Through this delegation the voting system can take into account not just the will of those who explicitly voted for or
against the option, but also those who delegated the vote. They can delegate the vote to multiple other users, i.e.,
their friends or other people they trust, possibly share their values, but have more knowledge about the option at hand.
Instead of simply discarding the users who do not vote directly, our voting system having such delegation network can,
we believe, better achieve satisfaction of users with the results, and at worst be at par with current voting schemes.
Voting can be now done more frequently and in a loosely knit or engaged communities where voter turn-up is low. In our
use case of collaborative text authoring, the voting system allows that not all users read all proposed changes (which
can be quite numerous) and that the system can be used even for determining which comments in discussions the community
finds relevant, without users even noticing that there is voting happening for each of them.

A more detailed description of delegation in the voting system can be found in the [following
post](/post/towards-a-better-group-decision-making) with a [followup with comparison
with other similar ideas and the analysis](/post/peer-to-peer-voting-scheme).

To simplify, users can specify delegates in advance, so that the voting system can compute results in real-time and
without a need for users intervention. Users can change their position on the option at any time, moving from explicit
vote to delegation and back, and in the case of the delegation, see in real-time how their delegated vote is being used.

Such an online and real-time voting system allows novel ways on how decisions get into effect. Instead of having a
predefined and fixed timespan of how long a voting lasts, voting can be done in a sliding-window manner. Voting is open
until for, e.g., a week a majority has been uninterruptedly for or against. That is, while support fluctuates between
for and against the voting is open, once it settles for one, for or against, and does not cross the 50% threshold for a
week, the decision is made. Other similar schemes are possible as well. For example, another approach would be to use
hysteresis and say that decision is made when it gets 60% of the support. But that it is in effect until support fails
under 40%. If it fails under, decision is retracted. In this sense voting is open forever and users can later on change
the outcome, retract the decision.

There are multiple ways to map our general voting system to common use cases. For example, a traditional voting "for" or
"against" can be done through providing only one option "we accept/support", without any ranking. Voting for public
offices can be done by having users to vote for one or more options (candidates), optionally ranking them by preference.
Or, they could just be offered to rank them.

## Miscellaneous ##

In discussions users can reference other:

* users with `@username`
* changes with `%id` and `%id.comment`
* Wikipedia articles with `#wikipedia_article_name`
* other existing documents `&id`

## Future work ##

* Security/logging/history (verifiable and unforgeable results)
* Voting model properties analysis
* Privacy of voters and votes
* Decentralization and/or federation

## Acknowledgements ##

I would like to thank [Jure Judež](http://www.celovito.si/) for great mockups of the user interface.
