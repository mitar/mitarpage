---
title: wlan slovenija
icon: link
publishDate: 2013-03-28 23:37:10-07:00
draft: false
tags:
- project
- wlanslovenija
- openwireless
- opennetwork
disqus_id: "46576110193"
slug: wlan-slovenija
aliases:
- /post/46576110193/wlan-slovenija
- /post/46576110193/
---

[_wlan slovenija_](https://wlan-si.net/) is an open wireless network initiative, building an open, common and free
network in Slovenia by encouraging people to share their existing Internet connectivity with others, providing
participants guidance, knowledge and technology. For this effect, we have developed and used many [open source
technologies](http://dev.wlan-si.net/) ([GitHub](https://github.com/wlanslovenija)).

(Formerly _wlan ljubljana_.)

<!--more-->
