---
title: Read-write science
publishDate: 2013-05-14 07:02:10-07:00
draft: false
tags:
- readwrite
- science
- culture
- openaccess
disqus_id: "50419805220"
slug: read-write-science
aliases:
- /post/50419805220/read-write-science
- /post/50419805220/
---

There is a notion of a [read-write culture](https://en.wikipedia.org/wiki/Remix_culture), a culture where we are not
just consumers of the culture, but also producers. [Prosumers](https://en.wikipedia.org/wiki/Prosumer). But we are
missing a read-write science culture!

<!--more-->

I see move from read-only to read-write culture as a democratization of the culture. Creation of the culture became much
more inclusive, participatory and collaborative. There are less hierarchical structures, less authorities. In the past,
all this was needed because cost of communication was high, cost of production was high. You needed people to aggregate,
digest, and select to keep costs down and communication efficient. But this changed with the Internet and now we see
changes in all spheres of human culture.

In all spheres? Really? Science, scientific research and scientific communication, is resisting any changes quite well.
Research is still primarily done in closed and well-defined groups. Communication still happens through publishing
papers in journals, a model useful for past centuries where it was a good way to communicate with many other researchers
at the same time. But now we know better.

In science, we are still missing collaboration and inclusive approach to the research becoming something of a standard.
We have beautiful examples of opening research. For example, [BOINC](https://boinc.berkeley.edu/) and
[Foldit](http://fold.it/portal/) to general public, and [PolyMath](http://polymathprojects.org/) to fellow
mathematicians. [Citizen science](https://en.wikipedia.org/wiki/Citizen_science) is slowly taking off, despite most of
the papers still [being closed behind paywalls](https://en.wikipedia.org/wiki/Open_access).

We are missing a generalization of these individual cases and while involving general public is beautiful, an easy and
standard way for researches themselves to collaborate is still missing. If we are capable of making an encyclopedia
together, why not science as well? Do we really believe that from more than million papers which are published each year
none of those authors could rather work together on one paper, globally, collaboratively?

What is a paper? It is a log by a researcher (or researchers) of what has been done, what were results and what are
(possible) conclusions. It is a testimony that this is true, signed by the authors (sometimes I have a feeling that in
pursuit of being listed as an author, researchers forget that by having their name there it means they are also standing
behind the paper). Why we then not move this log keeping to the Internet?

Instead of publishing the log once a year in the form of a paper, we could have it online publicly on a common web
platform and update it regularly. I get an idea to research, I open a new log, describe the idea. I start thinking how
to define a hypothesis. How to design an experiment. How do implement the idea. I write all this down as it goes. I try
it out, I get some data, I upload data. I then analyze this data, I get some conclusions. I publish those conclusions.
Maybe during all this I am distracted by some other idea. Maybe I never finish. Maybe somebody else joins in. Maybe
somebody else decides to do the same experiment on the other side of the world, adding to my data and improving the
conclusion? Maybe somebody can discover an error in reasoning even before I get to the stage of doing an experiment?
Maybe somebody else things some other experiment would be better, he or she forks the log, does it, gets the data. We
can then compare, merge, get a combined better understanding. The web platform could find automatically cases of
possible collaboration. Or contradiction: this two hypotheses are stating the opposite. Scientific community can be
involved from the very beginning. Helping with ideas, discussing and giving feedback. Other researches could vote which
ideas look more reasonable to research first. They could even define dependencies: I need this research to finish for me
to be able to finish mine. They could pitch in. Or they could just propose ideas for research they themselves do not
have time to work on, but maybe some young researcher searching for an idea could adopt it? The community itself being a
mentor to him or her, following the execution, teaching the proper research in a hands-on manner? Somebody could join in
and make a summary [suitable for public](http://www.youtube.com/watch?v=AzcMEwAxSP8) in [innovative
ways](https://www.youtube.com/user/theRSAorg).

Then we might even remove the distinction between general public and researchers, citizen science and science.
Everything would be science, everybody could contribute, no matter his or her position in the society. Each
contribution, an idea, comment, data sample, or a beautiful drawing of a concept or the result, being evaluated just by
its (scientific) merit.
