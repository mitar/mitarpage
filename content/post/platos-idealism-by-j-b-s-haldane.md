---
title: Plato's Idealism by J. B. S. Haldane
icon: link
publishDate: 2013-10-14 07:31:10-07:00
draft: false
tags:
- haldane
- democracy
- size
disqus_id: "64019344138"
slug: platos-idealism-by-j-b-s-haldane
aliases:
- /post/64019344138/platos-idealism-by-j-b-s-haldane-1928
- /post/64019344138/
---

> And just as there is a best size for every animal, so the same is true for every human institution. In the Greek type
> of democracy all the citizens could listen to a series of orators and vote directly on questions of legislation. Hence
> their philosophers held that a small city was the largest possible democratic state. The English invention of
> representative government made a democratic nation possible, and the possibility was first realized in the United
> States, and later elsewhere. With the development of broadcasting it has once more become possible for every citizen to
> listen to the political views of representative orators, and the future may perhaps see the return of the national state
> to the Greek form of democracy. Even the referendum has been made possible only by the institution of daily newspapers.

An [essay](https://www.marxists.org/archive/haldane/works/1920s/right-size.htm) by J. B. S. Haldane from 1928.

<!--more-->
