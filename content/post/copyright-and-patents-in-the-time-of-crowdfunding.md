---
title: Copyright and patents in the time of crowdfunding
publishDate: 2013-05-26 02:25:10-07:00
draft: false
tags:
- copyright
- crowdfunding
- intellectualproperty
- patents
disqus_id: "51373589452"
slug: copyright-and-patents-in-the-time-of-crowdfunding
aliases:
- /post/51373589452/copyright-and-patents-in-the-time-of-crowdfunding
- /post/51373589452/
---

Many things were already said about how Internet makes distribution faster and cheaper and how this is putting under
[the question](http://questioncopyright.org/promise) rationale for copyright and patents. But Internet through
facilitation of [crowdfunding](https://en.wikipedia.org/wiki/Crowd_funding) is changing also the money flow around the
production itself, changing the nature of the investment which arguably has to have a protection of exclusivity to
return on the investment. Crowdfunding is changing this because it allows creators to skip the investment-return cycle
and directly fund the creation from its users. As such, copyright and patents are obsolete.

<!--more-->

In practice, the reason why copyright and patents[^1] exist is that often production and distribution costs of some
creative work are so high that a large upfront investment is required. This investment is often too large for creators
themselves, so they partner with some sort of a publisher organization. This organization wants (higher) assurance that
its investment will be returned and an established way is through copyright and patents which creators pass over to the
organization for the organization to exclusively monetize the creation. The organization gets for this service an award,
share of the profits from the monetization. To be truthful, those organizations do often provide additional services,
like promotion and many other needed services which creators might not be too familiar with themselves.

Proponents of copyright and patents argue that those rights do exist primarily to incentivize creators to create, but in
practice we see that this is not really true. We can see people everywhere who create because they love to create,
because they want to tell or give something to other people. But yes, creators do have to life from something.

It is important to remember what are downsides of copyright and patents. All creations are based on past creations by
other people. More creations are available for everybody to build upon, more new creations there can be. This is why
copyright and patents had to historically find the right balance between being long enough for investments to return and
profits to grow, but as short as possible to allow other people to build upon as soon as possible. Sadly, power to
exclusively monetize has proven too attractive for organizations to give up, so in 20th century we have seen extensions
of this time period which now [extend even after the death of original
creators](https://en.wikipedia.org/wiki/Copyright_Term_Extension_Act). This should not be too surprising though, as we
have just to remember again that creators pass over the rights to organizations, which then collect the profits.

While Internet is making distribution of creations cheaper and it is turning business models around, costs of the
production can still be too high for creators themselves. But crowdfunding is turning even this around. For example, on
[Kickstarter](http://www.kickstarter.com/), currently the most popular crowdfunding website, creators propose their
projects and required budgets to realize them. Those budgets can include everything, from material costs to awards
creators want for themselves. People ("the crowd") can then decide whether they like the project and back (fund) the
project if it reaches the given budget before the deadline. For the (co)funding of the project, backers are often
promised something in exchange, some result of the project. Once the project reaches the budget and the funding
deadline, creators get the funding, can create the project and backers get the results.

Such model of funding of production effectively puts copyright and patents under the question. Creators in advance
decide what is the incentive they require to create the project, together with all other costs. Once the project's
budget has been meet they can realize the project and results can enter into the public domain, available for others to
build upon and propose other projects. In this way we can all achieve better and faster innovation while still allowing
expensive productions to occur.

[^1]: Trademarks, moral rights, and some other intellectual property rights have arguably still some reason to exist. Especially as a defensive measures.
