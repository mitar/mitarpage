---
title: One person, one vote or one dollar, one vote and blockchain
publishDate: 2016-07-20 01:52:10-07:00
draft: false
tags:
- democracy
- market
- voting
disqus_id: "147790197426"
slug: one-person-one-vote-or-one-dollar-one-vote
aliases:
- /post/147790197426/one-person-one-vote-or-one-dollar-one-vote-and
- /post/147790197426/
---

We live in times of a hidden war between "one person, one vote" and "one dollar, one vote" ideologies. The reason why it
is hidden is because we use the same terms for both: democracy, voting, consensus, etc. We govern our governments each
having one vote, but in our companies shareholders commonly hold votes proportional to their share. Some people are
claiming that the latter is a better approach and everything should be decided through markets and power. I believe that
using power (physical or monetary) to make decisions is barbaric and that our civilization progress was to introduce a
more true democracy, one person, one vote. But I do not believe even that is the end of our developments in this respect
and we should continue developing our collective governance. Moreover, I do not believe that these two positions are
necessary the only possibilities, and some combinations might also exist. In some way we might even already have that:
using "one person, one vote" to decide the rules under which we operate, but using "one dollar, one vote" to decide how
to split the profits.

Anyway, all this could be a topic of some other longer blog post. Here I wanted to explain this existing tension between
these two ideologies to present how they have existed in decentralized technologies as well and why Bitcoin's blockchain
is so innovative.

<!--more-->

Traditional perspective on decentralized systems is that you have agents which communicate with each other to form a
system. Many decentralized protocols use some form of voting to decide on common decisions, e.g., to which new state
they should all transition. What the majority of agents vote for is seen as the correct new state for the whole system
(so called consensus). This means that there can be up to half of malicious agents and the decentralized system will
still operate correctly.

But sadly it is not so easy. The issue is that if membership of agents in the system is open, then a malicious agent can
join the system multiple times with "puppet" agents, and because each agent gets one vote, can then enforce its view of
system's state. This is know as the [Sybil attack](https://en.wikipedia.org/wiki/Sybil_attack). In centralized systems
this is not a problem because you can have a centralized entity controlling membership.

The ingenious solution made by Satoshi Nakamoto for Bitcoin's blockchain is to replace this "one person, one vote"
voting with "one dollar, one vote" voting. In Bitcoin's case you vote with CPU cycles, not dollars. Even if you create
multiple "puppet" agents they all have to share the same amount of CPU cycles you have at your disposal so you do not
really gain any advantage over others.

This innovation revived the interest in decentralized technologies. A hope emerged that we could completely decentralize
all aspects of our society and remove intermediaries who require fees we do not like, or can abuse power they have. We
have seen many projects embark to achieve this.

But while the solution is ingenious and is an innovative shift in a perspective, it has not solved the problem at a
fundamental level. As such it has issues we have to be aware of.

"One dollar, one vote" voting is the most suitable for use cases where we are already used to such voting: markets and
companies with shareholders. But we have to be wary of attempts of everything being transitioned to this type of voting
because we might lose true democracy of "one person, one vote" without even noticing. This is often not conscious and is
just a consequence of uncritically applying blockchain technology to the problem. Especially if blockchain is already
used for other aspects of a project.

While I can understand that some people like transition to "one dollar, one vote" for all aspects of a decentralized
society, I would like to warn especially people who otherwise do not align with this ideology to be careful when
embracing blockchain as a technology of choice for their projects. For example, projects around the [platform
cooperativism](http://wiki.p2pfoundation.net/Platform_Cooperativism) idea.

Another issue is that "one dollar, one vote" environment is susceptible to concentrations of power, like any other
environment which is built around power. While initially it looks like such voting is democratic as well, through time
an oligarchy forms. This can be seen in Bitcoin as well, where all mining is now effectively done only by a very few
large mining pools.

We can see that while "one dollar, one vote" voting solution addressed one technical challenge, it brought new (but
known elsewhere) challenges to the technical realm.

A consequence is that we have not really solved the problem of a decentralized true democracy where each user person
would have only one vote and where we would have an open membership. We still do not know how to do achieve both at the
same time. This can be seen with all governance issues of Bitcoin and Ethereum communities. One could say that those
communities are not interested in true democracy, but the real issue is that they do not really have a choice at the
moment. We do not yet know a necessary solution (and are we even working on it?). So we have to decide what to do in
meantime: use centralized technologies which allow true democracy, or use decentralized technologies which do not?

Moreover, in meantime, when somebody talks about voting, consensus, and democratic decentralized technologies, listen
very carefully if they are talking about "one person, one vote" or "one dollar, one vote".
