---
title: Meteor Blaze Components
icon: link
publishDate: 2015-04-08 15:38:10-07:00
draft: false
tags:
- project
- meteor
disqus_id: "115887658076"
slug: meteor-blaze-components
aliases:
- /post/115887658076/meteor-blaze-components
- /post/115887658076/
---

[Blaze Components](https://github.com/peerlibrary/meteor-blaze-components)
for [Meteor](https://meteor.com/) are a system for easily developing complex
UI elements that need to be reused around your Meteor app.
[Live tutorial](http://components.meteor.com/).

<!--more-->
