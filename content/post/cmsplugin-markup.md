---
title: cmsplugin-markup
icon: link
publishDate: 2013-03-28 21:57:10-07:00
draft: false
tags:
- project
- djangocms
- django
- python
- markup
disqus_id: "46570604272"
slug: cmsplugin-markup
aliases:
- /post/46570604272/cmsplugin-markup
- /post/46570604272/
---

An extendable [markup content plugin](https://pypi.python.org/pypi/cmsplugin-markup) for [Django
CMS](https://www.django-cms.org/). It allows you to use various markup languages for content.

<!--more-->
