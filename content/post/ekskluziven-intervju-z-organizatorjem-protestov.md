---
title: Ekskluziven intervju z organizatorjem protestov
icon: link
publishDate: 2013-03-29 00:19:10-07:00
draft: false
tags:
- media
- slovenskenovice
- demonstrations
- slovenia
- protests
- project
disqus_id: "46577754241"
slug: ekskluziven-intervju-z-organizatorjem-protestov
aliases:
- /post/46577754241/ekskluziven-intervju-z-organizatorjem-protestov
- /post/46577754241/
---

[Interview with me](http://www.slovenskenovice.si/novice/slovenija/ekskluziven-intervju-z-organizatorjem-protestov)
about ongoing demonstrations against corrupt government officials in Slovenia in most-read and quite a bit yellowish
Slovenian daily newspaper.

<!--more-->
