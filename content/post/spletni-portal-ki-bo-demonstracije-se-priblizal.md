---
title: Spletni portal, ki bo demonstracije še približal ljudem
icon: link
publishDate: 2013-03-29 00:46:10-07:00
draft: false
tags:
- media
- exposer
- dnevnik
- demonstrations
- slovenia
- protests
- project
disqus_id: "46578668614"
slug: spletni-portal-ki-bo-demonstracije-se-priblizal
aliases:
- /post/46578668614/spletni-portal-ki-bo-demonstracije-še-približal
- /post/46578668614/
---

[Article about the Exposer
project](https://www.dnevnik.si/magazin/znanost-in-tehnologija/spletni-portal-ki-bo-demonstracije-se-priblizal-ljudem)
in [Dnevnik](https://www.dnevnik.si/), Slovenian daily newspaper. It is described as a web portal which brings
demonstrations closer to the people, as a way for those who are otherwise not using social networks to see what is
happening there.

<!--more-->
