---
title: Presentation and release of nodewatcher 3.0
icon: video
publishDate: 2015-11-19 10:30:10-07:00
draft: false
tags:
- media
- project
- nodewatcher
- wlanslovenija
- battlemesh
disqus_id: "133540671971"
slug: presentation-and-release-of-nodewatcher-30
aliases:
- /post/133540671971/presentation-and-release-of-nodewatcher-30-at
- /post/133540671971/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/VVSR2gJVuNk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Presentation and release of [nodewatcher](https://dev.wlan-si.net/wiki/Nodewatcher) 3.0 at [Battlemesh
v8](http://battlemesh.org/). Nodewatcher is an open source network planning, deployment, monitoring and maintenance
platform with emphasis on community. Version 3.0 is a complete rewrite bringing modularity and extensibility.

<!--more-->
