---
title: xml4js – XML to JavaScript parser using XML Schema to guide conversion
icon: link
publishDate: 2014-06-22 04:17:10-07:00
draft: false
tags:
- project
- nodejs
- xml
- json
- javascript
- xsd
disqus_id: "89544084756"
slug: xml4js-xml-to-javascript-parser-using-xml-schema
aliases:
- /post/89544084756/xml4js-xml-to-javascript-parser-using-xml-schema
- /post/89544084756/
---

[XML to JavaScript parser](https://www.npmjs.org/package/xml4js) which uses XML Schema to guide conversion. Main
motivation is that instead of guessing the structure of an XML document from the document itself, structure is created
automatically from the XML Schema which makes output consistent for all XML documents, not just one at hand. For
example, arrays are always arrays even when there is only one element in the particular XML document, and if there is
only one element allowed, then there will be no one-element array wrapped around. This makes programmatically traversing
the structure much easier, because structure is consistent and predictable.

<!--more-->
