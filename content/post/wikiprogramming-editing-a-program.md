---
title: Wikiprogramming – editing a program from the program itself
publishDate: 2014-03-25 16:58:10-07:00
draft: false
tags:
- wikiprogramming
- collaboration
- programming
- wiki
disqus_id: "80725560313"
slug: wikiprogramming-editing-a-program
aliases:
- /post/80725560313/wikiprogramming-editing-a-program-from-the
- /post/80725560313/
---

I am still waiting for somebody to create a programming framework where user could edit the program itself while it is
running. Each user interface element would contain an "edit" button and by clicking it you would get source code editor
for all code related to that element. Edit it, try it temporary, click save, and you have a new version of the program.

<!--more-->

This is even more interesting if we imagine a webapp, a cloud service, which would be built upon such a framework. Users
could immediately edit the webapp, without having to establish a development environment somewhere else. You would use
the webapp, see something to improve, click edit, try it, save it, and everyone else would get this new version of the
program. All changes would be stored so somebody else could reverse back, or build upon your change. This would make
programming more accessible and approachable to novices. Develop faster. Community of users would also not be so locked
into what developers imagine a webapp should be and do.

What about security you say? What if somebody adds some malicious code? But is this really so different than what
happens on Wikipedia? If you read something misleading there, or if you run something misleading here. It is vandalism
in both cases and similar measures could be employed. Or even some new ones, like not applying the new code to everybody
immediately, but A/B test it on a random subset of users. Or ask a random subset of users to review it. This would be a
nice addition to Wikipedia as well. So some ideas how to improve Wikipedia itself could be applied here as well.

Of course you could also run two instances of the webapp, one for collaborative development, one for users to use. With
some process of code review in between.
