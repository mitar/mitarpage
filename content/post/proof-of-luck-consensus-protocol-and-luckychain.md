---
title: Proof of luck consensus protocol and Luckychain blockchain
publishDate: 2017-03-21 05:52:10-07:00
draft: false
tags:
- blockchain
- project
- ipfs
disqus_id: "158664249771"
slug: proof-of-luck-consensus-protocol-and-luckychain
aliases:
- /post/158664249771/proof-of-luck-consensus-protocol-and-luckychain
- /post/158664249771/
---

[Proof of work consensus protocol](https://en.wikipedia.org/wiki/Proof_of_work)
used in modern cryptocurrencies like Bitcoin and Ethereum consumes
a lot of energy and requires participants to use their CPUs for mining instead of
other useful work. But exactly this cost is why it is works to prevent
[Sybil attacks](https://en.wikipedia.org/wiki/Sybil_attack).
One cannot participate in the selection of the next block without paying this cost,
which makes the issue of puppet participants trying to influence block selection
irrelevant, because they also have to do the work, and pay the cost.

In recent Intel CPUs a new set of instructions is available,
[SGX](https://software.intel.com/en-us/sgx), which allows one to run code
inside a special environment where even operating system cannot change its execution.
In [the paper](https://dl.acm.org/citation.cfm?id=3007790)
we published ([arXiv](https://arxiv.org/abs/1703.05435),
[Cryptology ePrint Archive](https://eprint.iacr.org/2017/249))
we explore consensus protocol designs using the Intel SGX technology, with the
goal of making blockchain participation energy efficient, with
low CPU usage, and to democratize mining so that participants can participate again with their
general purpose computers (with Intel CPUs) instead of only with specialized
[ASICs](https://en.wikipedia.org/wiki/Application-specific_integrated_circuit).

<!--more-->

Proof of luck is an example of such a Intel SGX based consensus protocol where all participants
select a random number (luck) and one with the highest number wins (luckiest). The luckiest block
is then used as the next block in the blockchain. Because random number selection happens inside
an SGX environment one cannot fake it. Each CPU can choose only one random number per block.

We [made a blockchain implementation](https://github.com/luckychain/lucky) based on the
proof of luck.
[Instead of reimplementing lower layers needed for a blockchain](/post/towards-layered-re-decentralized-web)
we built it on top of [IPFS](https://ipfs.io/). It is written
in JavaScript and uses a [NPM module](https://github.com/luckychain/node-secureworker)
to run JavaScript inside SGX environment. Because it is build
on top of IPFS, its transactions can reference any other object stored in IPFS, whole files,
large or small, and other linked data structures.

Two main properties we want from a blockchain is liveness and persistence. Liveness means that
transactions submitted into a blockchain cannot be prevented from being added to a blockchain.
One could ignore a particular transaction in their block, but because somebody else's block might
win instead, they cannot really prevent that winning block to include the transaction. And even
if their block wins this time, the next winning block might include the transaction instead.

Persistence is the one we commonly associate with a blockchain. Once a transaction is committed
into a blockchain block, an attacker should not be able make the transaction or the block be
removed later on from the blockchain. Effectively this means that the attacker should not be
able to convince all other participants to switch to a blockchain version with that block
removed (or changed to not include the transaction). In our blockchain participants switch
to a new blockchain when the new blockchain is luckier as a whole (sum of all blocks' numbers)
than the old blockchain. But an attacker would have to create an alternative chain with many
blocks luckier than those in the original chain, and this is harder and harder as the chain
is getting longer, because all other participants are choosing the luckiest block among all
of their block candidates, and the attacker would have to surpass them all.

All this is currently at a prototype stage and not everything is yet properly implemented,
or even designed, but
[try it out and contribute to its development](https://github.com/luckychain/lucky).
Moreover, [you can now use SGX in your node.js app](https://github.com/luckychain/node-secureworker),
too.
