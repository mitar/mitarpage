---
title: Transparent touch-screen on the laptop
publishDate: 2013-09-17 07:31:10-07:00
draft: false
tags:
- idea
- laptop
- touchscreen
- transparency
disqus_id: "61499504301"
slug: transparent-touch-screen-on-the-laptop
aliases:
- /post/61499504301/transparent-touch-screen-on-the-laptop
- /post/61499504301/
---

I am still waiting for the laptop which would have an on demand transparent (see through) screen. This would be useful
while working. But even more interesting would be that you could close the laptop down and then use the other side of
the screen as a touch-screen based device. No flipping of the screen around necessary.

<!--more-->
