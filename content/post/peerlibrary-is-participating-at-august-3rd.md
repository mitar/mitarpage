---
title: PeerLibrary is participating at August 3rd Hacktivation
publishDate: 2013-08-02 06:49:10-07:00
draft: false
tags:
- peerlibrary
- hacktivation
- reallocate
- project
disqus_id: "57155850890"
slug: peerlibrary-is-participating-at-august-3rd
aliases:
- /post/57155850890/peerlibrary-is-participating-at-august-3rd
- /post/57155850890/
---

[Hacktivations](http://reallocate.org/hacktivations/) bring together developers, designers and storytellers to come
together and hack for good on software projects that are creating social good through their work. PeerLibrary will be
one among 8 projects to hack on this time. Come!

via [peerlibrary](https://blog.peerlibrary.org/post/57155739561/peerlibrary-is-participating-at-august-3rd)

<!--more-->
