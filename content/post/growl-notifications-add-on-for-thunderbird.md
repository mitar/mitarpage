---
title: Growl notifications add-on for Thunderbird
icon: link
publishDate: 2013-03-23 02:54:10-07:00
draft: false
tags:
- project
- thunderbird
- growl
disqus_id: "46061388537"
slug: growl-notifications-add-on-for-thunderbird
aliases:
- /post/46061388537/growl-notifications-add-on-for-thunderbird
- /post/46061388537/
---

[Growl New Message Notification
add-on](https://addons.mozilla.org/en-US/thunderbird/addon/growl-new-message-notification/) for
[Thunderbird](https://www.mozilla.org/thunderbird/) is an alternative way to have [Growl](http://growl.info/)
notifications when new e-mails arrive.

<!--more-->
