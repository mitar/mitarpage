---
title: wlan slovenija (part 2)
icon: audio
publishDate: 2013-03-23 02:32:10-07:00
draft: false
tags:
- media
- wlanslovenija
- project
- wireless
- meshnetworks
- communitynetworks
disqus_id: "46060777396"
slug: wlan-slovenija-radio-part-2
aliases:
- /post/46060777396/wlan-slovenija-open-wireless-network-radio
- /post/46060777396/
---

<audio class="add-margin" controls src="http://kruljo.radiostudent.si/mp3/KPU/111214-KPU-mitar2-P.mp3" type="audio/mpeg" style="outline: none; width: 570px; margin-bottom: -10px;"></audio>

*wlan slovenija* open wireless network radio interview at local [Radio Student](http://www.radiostudent.si/) (in
Slovene). Part 2.

<!--more-->
