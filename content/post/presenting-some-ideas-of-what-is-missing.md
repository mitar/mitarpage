---
title: Presenting some ideas of what is missing in open access and open science community
icon: video
publishDate: 2014-11-22 03:09:10-07:00
draft: false
tags:
- opencon2014
- opencon
- media
- ideas
- openaccess
- openscience
disqus_id: "103272799066"
slug: presenting-some-ideas-of-what-is-missing
aliases:
- /post/103272799066/presenting-some-ideas-of-what-is-missing-in-open
- /post/103272799066/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/w6H8opOLbwU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Presenting some ideas of what is missing in open access and open science community at [OpenCon
2014](http://opencon2014.org/) in Washington, DC.
[Slides](https://peerlibrary.github.io/outreach/slides/2014-11-16-opencon-ideas/).

<!--more-->
