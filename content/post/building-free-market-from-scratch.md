---
title: Building free market from scratch
publishDate: 2016-04-08 23:48:10-07:00
draft: false
tags:
- freemarket
disqus_id: "142501567841"
slug: building-free-market-from-scratch
aliases:
- /post/142501567841/building-free-market-from-scratch
- /post/142501567841/
---

Interesting that those who like free market prefer changing existing economies which are build on public spending and
public infrastructure, over building their own infrastructure from scratch somewhere in the middle of an ocean, and then
having free market there. I am still waiting for that. Also, please do not dump your trash into that ocean. Ocean is
commons. Make it work inside a closed system of your free market, without counting on future generations to clean after
you.

<!--more-->
