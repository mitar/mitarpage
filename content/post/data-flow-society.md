---
title: Data-flow society
publishDate: 2014-03-28 07:30:10-07:00
draft: false
tags:
- dataflow
- society
- interactions
- values
- stories
disqus_id: "80975791203"
slug: data-flow-society
aliases:
- /post/80975791203/data-flow-society
- /post/80975791203/
---

[I have proposed a new global economic system](/post/a-new-global-economic-system)
which, instead of trying to reduce transactions between people to one numerical value (money), stores all information
about the transaction itself so that then later on everybody can assign a personalized value to the transaction.

An interesting observation is that we can see people as nodes in a network and transactions between them as connections
(edges) in the network. The question is how we define these edges. Do we assign an edge a precomputed simple value, a
number, which is made at the moment the edge itself is made. (This is what we currently do.) Or do assign context of the
edge to the edge, so that anybody can evaluate the edge by themselves. (This is what I am proposing.)

<!--more-->

I think this could be generalized to many different networks found in a society. We can see relations as interactions
between peers in a network. Do we then assign to edges values as "objective" facts, independent from who is
observing/evaluating those relations. Or do we embed into edges contexts and stories so that everyone listening to them
can evaluate them for themselves. Do we remember interactions between people as a simple value, what we gained or lost
from the interaction, or do we remember interactions through stories, experiences.

By observing this we can see that a general tendency in our society today is to reduce interactions between people into
simple values, evaluate them immediately, in short term, and then remember them forever as such. When we argue who is
right we list facts and try to determine a conclusion. Once we find a conclusion, we assume it will hold forever. We
rarely go back and observe that environment or context changed. We can see most of our laws are defined in this manner.

But it is not necessary to be so. Traditionally we can see that stories and story-telling, anecdotes and personal
experiences, had been an important way for people to pass on memories, tell each other interactions. Each such story
tried to encode the context of the event, but then listener was able to interpret it by themself, adding their own
values and experiences. Today we are reducing interactions to simple "likes", a very simple value. But we are losing
that expression another person leaves on you when you tell them your story.

Maybe it is not enough to reimagine our economic system, maybe we have to reimagine also how we remember other our
interactions. Let's remember stories. Let's write down laws together with stories which prompted a rule. So that we can
later on reevaluate if stories are still applicable to a changed world. Maybe the rule will not work anymore directly,
but having stories would allow us to still apply it to a new situation.

Thanks to Derek Razo for triggering this line of thoughts.
