---
title: Vse demonstracije na enem mestu
icon: link
publishDate: 2013-03-29 00:32:10-07:00
draft: false
tags:
- media
- exposer
- mladina
- demonstrations
- slovenia
- protests
- project
disqus_id: "46578182419"
slug: vse-demonstracije-na-enem-mestu
aliases:
- /post/46578182419/vse-demonstracije-na-enem-mestu
- /post/46578182419/
---

[Presentation of the Exposer project](http://www.mladina.si/120418/vse-demonstracije-na-enem-mestu/) in
[Mladina](http://www.mladina.si/), a weekly Slovenian newspaper, describing it as a place to learn everything about
demonstrations in Slovenia.

<!--more-->
