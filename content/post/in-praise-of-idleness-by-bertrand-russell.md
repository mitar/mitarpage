---
title: In Praise of Idleness by Bertrand Russell
icon: link
publishDate: 2013-05-17 07:01:10-07:00
draft: false
tags:
- bertrandrussell
- economy
- work
disqus_id: "50651877824"
slug: in-praise-of-idleness-by-bertrand-russell
aliases:
- /post/50651877824/in-praise-of-idleness-by-bertrand-russell
- /post/50651877824/
---

> Suppose that, at a given moment, a certain number of people are engaged in the manufacture of pins. They make as many
> pins as the world needs, working (say) eight hours a day. Someone makes an invention by which the same number of men can
> make twice as many pins: pins are already so cheap that hardly any more will be bought at a lower price. In a sensible
> world, everybody concerned in the manufacturing of pins would take to working four hours instead of eight, and
> everything else would go on as before. But in the actual world this would be thought demoralizing. The men still work
> eight hours, there are too many pins, some employers go bankrupt, and half the men previously concerned in making pins
> are thrown out of work. There is, in the end, just as much leisure as on the other plan, but half the men are totally
> idle while half are still overworked. In this way, it is insured that the unavoidable leisure shall cause misery all
> round instead of being a universal source of happiness. Can anything more insane be imagined?

An [essay](http://www.zpub.com/notes/idle.html) by [Bertrand Russell](https://en.wikipedia.org/wiki/Bertrand_Russell)
from 1932.

<!--more-->
