---
title: What's Wrong with Technological Fixes?
icon: link
publishDate: 2013-07-10 07:31:10-07:00
draft: false
tags:
- evgenymorozov
- terrywinograd
- solutionism
- technology
- critique
disqus_id: "55087013758"
slug: whats-wrong-technological-fixes
aliases:
- /post/55087013758/whats-wrong-technological-fixes
- /post/55087013758/
---

[It is always very important to ask yourself about the meaning of some
solution](https://www.bostonreview.net/books-ideas/whats-wrong-technological-fixes). The reason for it. Engineers too
often like to solve some problem because they can. But is the problem really needed to be solved? Should there really be
an app for it?

> Morozov characterizes this impulse to fix everything as "solutionism," and offers two broad challenges to the
> solutionist sensibility. First, solutionists often turn public problems into more bite-sized private ones. Instead of
> addressing obesity by regulating the content of food, for example, they offer apps that will ‘nudge' people into better
> personal choices. Second, solutionists overlook the positive value in the ‘vices' they seek to ‘cure.'

I like the thought that sometimes inefficiencies in the system have a purpose. Probably we should really be careful
which inefficiencies we tackle and which we allow. Today we have means to remove many. But this does not mean that we
should remove them all.

<!--more-->
