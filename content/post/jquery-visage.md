---
title: jQuery Visage
icon: link
publishDate: 2013-03-23 02:56:10-07:00
draft: false
tags:
- project
- jquery
- lightbox
- gallery
- plugin
- javascript
disqus_id: "46061424638"
slug: jquery-visage
aliases:
- /post/46061424638/jquery-visage
- /post/46061424638/
---

[jQuery Visage](http://plugins.jquery.com/visage/) provides a stable (no partially displayed elements), robust (it
closes when you click close), styleable (CSS), customizable (you can reprogram many aspects of its behavior) and clean
(being an [jQuery](http://jquery.com/) plugin) way to display a series of images in a lightbox-like way.

<!--more-->
