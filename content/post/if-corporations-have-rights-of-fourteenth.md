---
title: If corporations have rights of Fourteenth Amendment, then they have rights
  of Thirteenth Amendment as well
publishDate: 2013-06-04 19:57:10-07:00
draft: false
tags:
- corporations
- corporatepersonhood
disqus_id: "52191510029"
slug: if-corporations-have-rights-of-fourteenth
aliases:
- /post/52191510029/if-corporations-have-rights-of-fourteenth
- /post/52191510029/
---

If [corporations are people](https://en.wikipedia.org/wiki/Corporate_personhood) and have their rights, then they should
not be enslaved, owned and traded as well, no?

<!--more-->
