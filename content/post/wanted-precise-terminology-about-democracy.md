---
title: 'Wanted: precise terminology about democracy'
publishDate: 2016-07-22 15:48:10-07:00
draft: false
tags:
- democracy
- terminology
disqus_id: "147817338056"
slug: wanted-precise-terminology-about-democracy
aliases:
- /post/147817338056/wanted-precise-terminology-about-democracy
- /post/147817338056/
---

In the [previous blog post](/post/one-person-one-vote-or-one-dollar-one-vote)
I presented one example of a confusion when talking about democracy: we use democracy for
both "one person, one vote" and "one dollar, one vote" approaches to voting.
But the issue is much broader. Saying that something is democratic does not
really tell much, because it can mean anything from a majority voting,
consensus (unanimity), voting based on shares, a system with representatives
and one where we vote directly. Democracy is used to wage wars, topple dictators,
but also topple democratically elected people. We use democracy to say
"[you cannot argue with it](/post/questioning-democracy)".
And we use it to position ourselves as morally superior. As such,
the term democracy became almost useless.

We need to start finding more precise terminology for all aspects of democracy.
What does it mean that a cooperative is democratically run? That workers can elect
board members? That they do not have votes based on shares? Or that they can directly
influence business decisions through a democratic process? Which process exactly?
Does it matter? Are all the same? I do not think so.

Let's start building terminology. Collectively.

<!--more-->
