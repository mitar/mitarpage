---
title: Crowdsourcing and ethics
publishDate: 2013-10-21 22:01:10-07:00
draft: false
tags:
- crowdsourcing
- ethics
- unions
disqus_id: "64751721270"
slug: crowdsourcing-and-ethics
aliases:
- /post/64751721270/crowdsourcing-and-ethics
- /post/64751721270/
---

Crowdsourcing definitely has some issues [we are not talking about
enough](http://www.slideshare.net/mattlease/crowdsourcing-ethics-a-few). They are workers as any other workers. They are
precarious as many others today. Who is thinking about their rights, worker rights? Who is making sure that they are
payed and are not working in sweatshops or even slavery conditions? We really do need a 21st century concept of worker
unions, for globalized and precarious world. All crowdsourcing workers,
[unite](http://turkopticon.differenceengines.com/)!

<!--more-->
