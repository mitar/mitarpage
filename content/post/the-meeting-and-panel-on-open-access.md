---
title: The meeting and panel on open access
icon: video
publishDate: 2013-08-20 07:19:10-07:00
draft: false
tags:
- media
- openaccess
disqus_id: "58788983870"
slug: the-meeting-and-panel-on-open-access
aliases:
- /post/58788983870/the-meeting-and-panel-on-open-access-organized-by
- /post/58788983870/
---

<iframe width="570" height="320" class="add-margin" src="https://archive.org/embed/OpenAccessPublicMeetingAndPanel" frameborder="0" allowfullscreen></iframe>

The meeting and panel on [open access](https://en.wikipedia.org/wiki/Open_access) organized by [Open Access Initiative
at Berkeley](http://oa.berkeley.edu/) on May 8 2013 at UC Berkeley, South Hall (School of Information), answering many
questions about open access.

The panel includes [Mike Eisen](http://www.michaeleisen.org/blog/) (co-founder of [PLoS](http://plos.org/)), Molly Von
Houweling ([Boalt Law School](http://www.law.berkeley.edu/), former director of [Creative
Commons](http://creativecommons.org/)), and Laine Farley (Executive Director of [California Digital
Library](http://www.cdlib.org/)). Moderated by me.

<!--more-->
