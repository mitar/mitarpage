---
title: Lego Mindstorms NXT interface for Haskell
icon: link
publishDate: 2013-03-23 02:51:10-07:00
draft: false
tags:
- project
- haskell
- lego
- mindstorms
- nxt
disqus_id: "46061299340"
slug: lego-mindstorms-nxt-interface-for-haskell
aliases:
- /post/46061299340/lego-mindstorms-nxt-interface-for-haskell
- /post/46061299340/
---

A [Haskell interface to Lego Mindstorms NXT](http://hackage.haskell.org/package/NXT) over Bluetoooth. It supports direct
commands, messages and many sensors (also unofficial). It has also support for a simple message-based control of a NXT
brick via remotely executed program (basic NXC code included).

<!--more-->
