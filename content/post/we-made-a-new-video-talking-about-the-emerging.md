---
title: We made a new video talking about the emerging community of PeerLibrary
icon: video
publishDate: 2014-04-27 15:25:10-07:00
draft: false
tags:
- peerlibrary
- media
- project
- openaccess
- history
- presentation
disqus_id: "84064835247"
slug: we-made-a-new-video-talking-about-the-emerging
aliases:
- /post/84064835247/we-made-a-new-video-talking-about-the-emerging
- /post/84064835247/
---

<iframe width="570" height="320" class="add-margin" src="https://player.vimeo.com/video/93085636" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

We made a new video talking about the emerging community of PeerLibrary, as well as the project's origins in the open access movement.

via [peerlibrary](https://blog.peerlibrary.org/post/84064256641/we-made-a-new-video-talking-about-the-emerging)

<!--more-->
