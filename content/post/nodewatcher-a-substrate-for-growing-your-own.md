---
title: 'nodewatcher: A Substrate for Growing Your own Community Network'
icon: link
publishDate: 2016-01-15 16:26:10-07:00
draft: false
tags:
- media
- project
- wlanslovenija
- nodewatcher
- communitynetworks
- wireless
- meshnetworks
disqus_id: "137377350481"
slug: nodewatcher-a-substrate-for-growing-your-own
aliases:
- /post/137377350481/nodewatcher-a-substrate-for-growing-your-own
- /post/137377350481/
---

A [paper](https://arxiv.org/abs/1601.02372) presenting nodewatcher.

<!--more-->
