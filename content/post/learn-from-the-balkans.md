---
title: Learn from the Balkans
publishDate: 2013-04-18 14:56:10-07:00
draft: false
tags:
- israelis
- palestinians
- balkans
- yugoslavia
- berkeley
- divestment
- asuc
- peace
- conflict
disqus_id: "48305772681"
slug: learn-from-the-balkans
aliases:
- /post/48305772681/learn-from-the-balkans
- /post/48305772681/
---

Yesterday I was following the ASUC Senate (student body) [discussion whether UC Berkeley should divest $14 million from
companies affiliated with Israel's
military](http://www.dailycal.org/2013/04/18/asuc-senate-passes-divestment-bill-11-9/) in opposition to human rights
abuses happening in the Israeli-Palestinian conflict. It was an interesting experience to listen and I believe UC
Berkeley has really some [great future politicians](http://nolanpack.com/). But it still made me sad because we all lost
once again. We would lost no matter how the decision would be made. I will explain you why.

<!--more-->

I do not know much about situation there, I have never been there, I am not really following or reading much about it,
it is not really my tea of cup. But I know one other story. The story of [the
Balkans](https://en.wikipedia.org/wiki/Balkans), or more precisely, of
[Yugoslavia](https://en.wikipedia.org/wiki/Yugoslavia). I know the story of brothers and sisters living for half a
century in a common country, working together, growing together, caring together. Not everything was perfect, far from
it, and we will probably never be able to describe relationships between all the people there. A kind of love-hate
relationships where you wish for your neighbor to break his leg, but if somebody dare to touch him, he will have to
speak with you. Because it is **your** neighbor. Relationships where you beat each other to a pulp over which football
team is better during the day but then drink together in the evening until you cannot stand on your feet anymore and the
person you fought before drags you to your home. Or invites you over to sleep on his sofa.

But all this changed in 1991 with split of the federation. Federal states now became countries. Or wanted to become
countries. It is interesting how fast brothers and sisters became arch-enemies willing to kill, rape, torture, humiliate
each other over the question of borders. This hill, this river, this village, it is ours! We have the right to it! You?
You should just disappear from the world. We do not care if this is your house, your farming land, if many generations
of your family lived here. Go! Disappear! Be [cleansed](https://en.wikipedia.org/wiki/Bosnian_Genocide) or
[erased](https://en.wikipedia.org/wiki/The_Erased)!

What changed? How so that people who just a moment ago lived next to each other now have problems with land? Did land
just disappear? Did many new people move to live there and they were just struggling to find a place under the sun for
themselves? No. Physically, everything stayed the same. Same people, same land, same hills, same rivers, same villages.

In 1980 [Tito](https://en.wikipedia.org/wiki/Josip_Broz_Tito) died. A benevolent dictator managed to establish
brotherhood and sisterhood both internally and [externally](https://en.wikipedia.org/wiki/Non-Aligned_Movement). But
after his death, power struggles started. Yugoslavia became a hunting ground open for everybody. Wannabe politicians,
religious leaders, private profiteers, and external interests. Everybody wanted a piece of cake and the best way to
achieve this is [divide and conquer](https://en.wikipedia.org/wiki/Divide_and_rule). Exactly this happened and small low
ranking politicians on a federal level now became top ranking politicians of new countries. Dream come true. Not just
that they moved to the top of the food chain, there was less competing politicians now, especially when you take the
opportunity and [remove some](https://en.wikipedia.org/wiki/Ivan_Kramberger). Ah, and of course, [arms dealing to all
sides](https://sl.wikipedia.org/wiki/Mariborska_oro%C5%BEarska_afera).

How is this possible? How could those politicians achieve this? People are for sure not so stupid! They lived together,
they knew each other, they speak more or less the same language, they were able to understand each other. But it seems
it is possible. It is quite easy in fact. You appeal to ethnicity, to nationalism, to religion. Or better, to
differences in those. (Catholics, Orthodox Christians, and Muslims, all lived in Yugoslavia.) You tell stories to people
and assure them that they are the only one who have the right to that hill over there. Because this is where the past
hero from long forgotten times fought his war. Or that river over there, because "our" people were first to cross it
(forgetting, conveniently, that of course there were some other people living there before "your" people). People start
believing this. Despite the fact that they lived next to the person of other religion for many years, they now discover
that it is simply impossible to live like that, that differences are too big, that the other one is barbaric, non-human.
Despite the hill being a nice hiking spot for many people of various backgrounds, meeting happily on the top, now it
becomes to small for all and only one has to rule it.

As a consequence, we had wars. Bloody wars. Sad wars. Wars full of tears and pain. Suffering and hate. Yugoslavia is no
more and all those private internal and external interests got what they wanted. They won. People, all people, lost.
Lost brotherhood and sisterhood, lost lives, lost property. For what? For that hill over there which they were already
free to climb?

This is why I became sad, seeing all this people discussing for hours and hours into the night with so much energy and
heart and hope. Because all this energy was directed into widening the gap between people. It will not move a bit
governments of both Israeli and Palestinian people, it will not move a bit their religious leaders. We will have a
feeling that we did the right thing, that we are fighting the human rights violations and now we will be able to sleep
better. Yes? How so? We did not do anything expect increased the pain of people. Yes, divestment is a powerful message,
powerful statement. But to whom? To the governments and religious leaders? Or to the people? Governments and religious
leaders will now just have one story more to tell to their people. See, how we are attacked from all sides, we have to
attack back ever more! See, how we are supported internationally, we have to attack even more, because now we will for
sure win!

Win? Who will win? We will all just lose. People will lose. Leaders will win. There is no conflict and no war without
leaders keeping them going. People do not want conflicts. People do not want to be killed. But they will stay in
conflicts and they will keep killing and being killed because nobody is helping them to get out that and it seems they
are unable to get out of it themselves? Or are they? Maybe they are unable to solve the issue themselves because
external interests are too big? (And yes, divestment could be seen as a step to decrease those external interests.)

Win? Some students now have a feeling that they won. Some students have the feeling that they lost. That they were
invaded, violated. This is sad. Because this is not a question of victory but of not yet getting the right (part of the)
solution to the issue we want to address. I dream of a consensus, of both sides agreeing wholeheartedly on a solution.
Then we would be able to say that we made a change. That we spun the world into the other direction. This would not just
be a solution to the issue but would also be an example of cooperation between all. It would be a double win!

We could. All this brains, all this energy, we could instead for 10 hours discuss a strategy how to enable Israeli and
Palestinian people to empower themselves so that they could create a change for themselves. We are lucky here, we do not
have bombs falling around and we can think in peace. If we cannot get Israeli and Palestinian people here, removed from
the harsh reality of the conflict, to work together towards a common peace, how can we believe we can change anything
there, there where it really matters? But now just imagine that all the Israeli and Palestinian people here would come
to agree on a common plan how to live, love and build together a place under the sun? How to recognize, analyze and
counteract all internal and external influences keeping this conflict going? How to not seek separation but cooperation.
They inhibit the same land, they are the same people. They are not? They are not the same people? They are. There are in
fact no Palestinians and no Israelis. They are all just humans.

We need an out-of-the-box solutions to the conflict. Where if not here we could create such solutions? But first we have
to recognize what or who we are fighting. Leaders who are keeping this conflict going. Yes, we can fight profiteers as
well, but this is just a side story. If we really want to make a change we should start thinking differently. First, let
us get all Israeli and Palestinian people on the campus to work together. Why they could not sign a peace act by
themselves? Create it in the way they believe it should be done by their governments. Put in everything they believe it
has to contain. Then seek to get more and more Israeli and Palestinian people from all around the world do join it, sign
it. Israeli and Palestinian people should take into their own hands the peace their governments seem not be able or
willing to create. All this can be started here and now. Would not be that much better message and statement to make?

In the Balkans people are again learning how to live together. Wondering, what frenzy got over them in '90s – how was
that possible? It is possible. But it is possible to get out of it as well.
