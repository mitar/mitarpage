---
title: Rising sea level will save us!
publishDate: 2013-10-18 07:30:10-07:00
draft: false
tags:
- future
- power
- peace
- sealevelrise
- tidalpower
- idea
disqus_id: "64388188455"
slug: rising-sea-level-will-save-us
aliases:
- /post/64388188455/rising-sea-level-will-save-us
- /post/64388188455/
---

I do not understand why people have issues with sea level rising. I think we should just take the opportunity. Cheaply
build tidal power plants on what is currently easy to build on land and then just wait for sea to come and power them
up. Cheap energy. For whole world. No more wars.

<!--more-->
