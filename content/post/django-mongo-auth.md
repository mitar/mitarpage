---
title: django-mongo-auth
icon: link
publishDate: 2013-03-28 23:17:10-07:00
draft: false
tags:
- project
- django
- mongodb
- python
- authentication
- mongoengine
disqus_id: "46575202112"
slug: django-mongo-auth
aliases:
- /post/46575202112/django-mongo-auth
- /post/46575202112/
---

[Django](https://www.djangoproject.com/) [authentication](https://pypi.python.org/pypi/django-mongo-auth) based on an
extensible [MongoEngine](http://mongoengine.org/) user class.

<!--more-->

Along with expected support for common external authentication providers and a traditional on-site registration workflow
with e-mail address confirmation, anonymous users are given a temporary account instance which can then be converted to
an authenticated one.

I believe this is the best way for community oriented websites where you want to allow even anonymous users to edit the
content or in some other way interact with the website. Instead of displaying their IPs (like Wikipedia does) or having
all anonymous users have the same name displayed, a new account with random username is generated for each anonymous
user and all content he or she creates is linked to it. Anonymous user can in this way be completely equal to registered
users and programming logic can be thus simplified, without a special case for anonymous users. The only difference is
that when user session terminates, this anonymous account gets orphaned. If the user wants to keep control over it, he
or she can convert it to a registered account. In fact, the only difference between anonymous and registered accounts is
that registered accounts have some authentication mechanism linked to them.
