---
title: How to get people to work more than 8 hours a day for you?
publishDate: 2015-06-18 05:28:10-07:00
draft: false
tags:
- investors
- startupculture
- venturecapitalist
disqus_id: "121832939606"
slug: how-to-get-people-to-work-more-than-8-hours-a-day
aliases:
- /post/121832939606/how-to-get-people-to-work-more-than-8-hours-a-day
- /post/121832939606/
---

If I approached you, the most talented person around, you with great knowledge and superior mind, you who can make any
concept into a reality, and I told you that I want you to work for me and develop my idea. I want you to work a lot more
than 8 hours a day, let's say 12, 15? I do not want that you do anything else. You should have have no life, you should
more or less live in the office. You should think about the project every moment of your life. Breath the project. Would
you accept it? I would pay you as little as possible, just so that you can get through in this expensive city. I will
maybe give you 10% of the profits of the company, if we grow really blazingly fast, but if we do not grow fast enough, I
will cancel the project and fire you. I would even ask you to invest your own money, and require you to ask your
parents, friends, to do the same. Would you accept? You who could work part-time and still earn enough to live a
comfortable live because you are well educated and you do not have to search for jobs, because you are getting job
offers daily. You can be picky. Would you still choose to work for me instead?

You would. You do. And I will tell you why.

<!--more-->

We know that ideas are cheap. For each given idea there are different groups of people working on that particular idea.
Your idea is not special. And you are also not special. You are maybe good, but there also others who are good. You
maybe have a special set of skills, but others have another special set of skills.

Today in this globalized and interconnected world this means that if I have an idea I would want to make a reality,
instead of me creating a company, trying to hire people, and face the reality that they will not work for me under
conditions I described above, a much easier way is to announce to people that they should come to me with their ideas. I
will listen to them, waiting for a group to come and tell me that they are working on my idea. Of course, they will
believe that it is their idea, but they do not understand that they are not special. I will help them create a company,
give them a bit of money, request just 30% of possible profit, and let them work whole days on "their" idea. I will let
them fight for me. If they will grow fast, I will provide more money so that they can continue working for me,
requesting next portion of the profit in exchange. And because their growth will not be blazingly fast (I admit, my idea
was not the best) and the project will take even more time (normally, they overestimated how good they are and simply
could not know all the issues they will be facing; any project has issues, you might not know which those will be, but you
can be assured that there will be issues), so I will allow them to work even more and take the rest of the profit, maybe
leaving few percents to so called "founders". To those people who were so happy when I decided to allow them to work for
me. Oh, what silly people. They were so happy. Sharing the success with their friends. Success that they can now work
for me for more than 8 hours a day. I can at any point decide that things are not going well, that maybe my idea was
even worse than I though, and I can pull out, effectively firing them. Maybe they get somebody else to hire them again,
for another share of their profits, if they have anything left. Maybe they still succeed, in this case profits are still
mine. They do not understand that me deciding not to fund them in the next round means effectively that I fired them.
And me funding them means that I am just continuing paying their salaries. Why would I get a bigger share just for
continuing allowing them to work for me? But they do not understand. They are happy. And I am happy. Win-win.

Imagine instead that we would use the globalized and interconnected world to get all these people to work together
instead on that one idea they share? Instead of making them compete and under the pressure, they could just collaborate.
They could take time to do things properly, thoughtfully and with a long-term vision. Hey, maybe you should consider
also the environmental impact of your idea. We would not require that the only thing about the project is the growth of
the userbase. Some projects are too important for that. But we as a society do not know how to approach their
realization. Or even how to recognize them. We use the same hammer for all nails.
