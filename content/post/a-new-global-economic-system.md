---
title: A new global economic system
publishDate: 2014-02-02 17:21:10-07:00
draft: false
tags:
- economicsystem
- economy
- couchsurfing
- giving
- idea
disqus_id: "75430079567"
slug: a-new-global-economic-system
aliases:
- /post/75430079567/a-new-global-economic-system
- /post/75430079567/
---

I am envisioning a new global economic system[^1] where you do not provide service or goods to another person because
they pay you, but because you evaluate how much, which and how they themselves have provided services and goods to
others in the past. You announce your availability of a given service or goods to others, interested apply, and based on
your values, beliefs, and information about their past contributions, you decide to whom you then provide given service
or goods. Such economy promotes balance, is not impersonal and can properly award cultural, artistic, and sustainable
services and goods.

If you know [Couchsurfing](http://couchsurfing.org/), then this will be very familiar to you. In essence it is a
generalization of Couchsurfing model, while thinking about necessary components to make it scalable and distributed.

It was written few years ago, before Bitcoin, which gives even more ideas how such economic system could be technically
implemented.

<!--more-->

Such economic system can be achieved only by using cutting edge information technologies. Current economic systems based
on money come from times of no global and omnipresent interconnectivity towards which we are steadily progressing. At
that times, money as a medium of conveying information about the value of goods and services exchanged and creation of
markets paved paths to great technological and cultural advances. Money streamlined goods and services exchanges and
allowed temporal and geographical division between providing goods and services on the one side and using them on the
other.

But we can do much better! Economic systems based on money have some severe limitations. They assume that all goods and
services can be numerically and one-dimensionally valuated. This is inherently not true for services and goods of
subjective value (such as found in culture or arts), alternative or uncommon nature (how much is teaching a new cooking
recipe worth?), or long-lasting or late results (how much is using sustainable technology now worth for people which
will live hundreds of years in the future?). Valuation in a western world is more and more indifferent to people
involved and their personal opinions, but is left to impersonal market to determine prices, influenced by demand and
supply and not by personal values and beliefs people involved might cherish. Or if influenced by values, it is just
through an aggregate of those values and without considering each one of the people involved.

Furthermore, when was the last time you bargained for a price for your apples at a store of a global international
food-market chain? And when was the last time you bargained to pay more than was the price of a book on sale at a book
store because you believed that it is worth more? Economic systems we have currently only award the buyer for lowering
the price and not for rising it - how can we then say that current market can valuate services and goods properly?

Because in current systems we valuate goods and services with simple numerical value, we are assuming that algebraical
properties of numbers hold also for these valuations. But is this necessary so? Maybe it holds for apples, valuation of
hundred apples is the same as hundred times valuation of one apple, but for many things it is not so. Cutting one tree
in the forest have completely other valuation than cutting the whole forest and definitely is not just sum of valuations
of individual trees.

Such limitations are reasonable for technologies, tools, and methods used at the time. But this days, with all
information technologies at our disposal, we can do much better. We can now connect all sellers and buyers together into
instantaneous communication. We can collect, store and process vast amounts of data. We can display information tailored
to each individual, personalized. We do not need to abstract information about each transaction into one-dimensional
value. We can do much more involved computations than just adding or subtracting them together. We have seen all this
technologies being used in other fields of human activities, like social interactions, why we would not use them also
for powering our economic system itself?

For the transition to the new economic system to be successful, it should be a gradual and voluntary process. This can
be done by building a parallel economic system on top of the decentralized nature of the Internet by inviting existing
Internet members which are offering services and/or goods to join and participate in it. More of them will join, more
diverse and thus applicable to different life situations the parallel system will be, slowly replacing current systems.

At the beginning, the new economic system itself will in practice not be much more than just a standardized way of
exchanging information about transactions, which will existing Internet services be able to integrate. Together with few
helper services. Later on, more higher-level services, for example centralized or specialized markets, will emerge.

Only in such decentralized and decoupled way it is possible to cover all different cultural and other differences
between all participants. In this aspect it is similar to current economic systems based on money. It is not really
important which currency exactly you are using and how exactly are transactions being done, just that some basic
properties of the system are uphold (for example, that you cannot spend the same money twice). Of course, for the
economic system build upon current information technologies, such properties are of a different nature.

To easier illustrate the concept we can compare it with the OpenID standard. But while OpenID deals with distributed
authentication, our system will deal with transactions. Each time a transaction (of service or goods) in virtual or
physical world will take place both parties will record this information through one of participating providers
(Internet services which are in the new system), each giving their feedback on the transaction, both in few quantitative
values and a free form comment. In this way each involved party will leave to each other a reference, linked to their
identities.

To help people finding goods and services to request, existing and new virtual and physical markets will be used. Often
markets themselves will be also providers recording transactions themselves.

Once a person will find something of interested offered, he or she will contact the person offering it and request for
it. In theory, the person offering it will get multiple such requests and decide to whom to give what he or she is
offering based on past references (recorded transactions) those people will have. He or she can decide that based on
personal values and beliefs, content, quantity and/or quality of those references. Of course very soon the number of
those references each person has will explode, but smart tools, evaluators, can be employed to display only to each
person relevant sample of all transactions, or to summarize them in some manner. In a similar way how Google is
providing relevant search results or aggregated news on Google News. Or how Facebook limits displayed posts from your
friends.

Summary of components:

* standard for exchange of transactions
* providers, participating Internet services which are integrating the standard to record transactions (current similar service: PayPal)[^2]
* markets, spaces (virtual or physical) where people can find what others are offering and can request it (current similar service: eBay)
* evaluators, tools which help evaluate those offering services or goods, and to those offering services or goods, help evaluate and choose among interested for it; here we will probably see a vast diversity of approaches created by global community of users (currently there is nothing like that)

Providers, markets and evaluators will often be combined into one, probably around a community; often specialized in
some type of service or goods, making bigger emphasis on those aspects of both what is offered on the market and how
transactions are evaluated and presented.

It is important to understand that everybody can become/deploy a provider, market, or evaluator service. In a similar
way to how currently everybody can use PayPal to record their (money based) transactions on their website. In the
proposed system you will in theory be able to be your own provider, but in practice you will in a similar manner to
PayPal outsource some existing provider. Or provide your own market for many users, or just a specialized webshop for
yourself. If you will be interesting in how to evaluate transactions, maybe you will develop a mobile application
visualizing transactions in a way to help others to decide to whom to give service of goods they are offering.

Furthermore, the new economic system is not limited only to services and goods provided over the Internet, it can be
equally used for traditional and physical services and goods. Only a record of the transaction is stored in the
Internet, and for that many providers will be available.

[^1]: In fact I really dislike use of word "economic" and "economy" here because the proposed system is in fact a money-less and exchange-less system. You do not use any type of money (even any of many alternative currencies) and you do not engage in a "I give you this in exchange for that" exchange, but just mutually record that you provided, gave, or did something to somebody and your personal evaluation of this deed, and this is it.
[^2]: Today Bitcoin would be another similar but decentralized system.
