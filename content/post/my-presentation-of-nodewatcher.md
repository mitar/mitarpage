---
title: My presentation of nodewatcher
icon: video
publishDate: 2013-06-21 20:24:10-07:00
draft: false
tags:
- media
- wlanslovenija
- nodewatcher
- project
disqus_id: "53566843565"
slug: my-presentation-of-nodewatcher
aliases:
- /post/53566843565/my-presentation-of-nodewatcher-an-open-source
- /post/53566843565/
---

<iframe width="570" height="427" class="add-margin" src="https://archive.org/embed/nodewatcher2010" frameborder="0" allowfullscreen></iframe>

My presentation of [nodewatcher](http://dev.wlan-si.net/wiki/Nodewatcher), an open source planning, deployment,
monitoring and maintenance platform for community wireless networks such as [wlan slovenija](http://wlan-si.net/). At
[International Summit for Community Wireless Networks](http://wirelesssummit.org/) 2010 in Vienna.

<!--more-->
