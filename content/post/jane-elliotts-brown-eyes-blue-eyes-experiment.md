---
title: Jane Elliott's "brown eyes, blue eyes" experiment
icon: video
publishDate: 2013-07-11 07:31:10-07:00
draft: false
tags:
- janeelliott
- racism
- eyes
disqus_id: "55173869112"
slug: jane-elliotts-brown-eyes-blue-eyes-experiment
aliases:
- /post/55173869112/jane-elliotts-brown-eyes-blue-eyes-experiment
- /post/55173869112/
---

<iframe width="570" height="427" class="add-margin" src="https://www.youtube-nocookie.com/embed/onKVeZaDzWg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Jane Elliott](https://en.wikipedia.org/wiki/Jane_Elliott)'s "brown eyes, blue eyes" experiment in 1970 showing racism
through a game of racism based on eyes color.

<!--more-->
