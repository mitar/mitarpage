---
title: wlan slovenija open wireless network
icon: video
publishDate: 2013-03-23 02:37:10-07:00
draft: false
tags:
- media
- wlanslovenija
- project
- wireless
- meshnetworks
- communitynetworks
disqus_id: "46060905790"
slug: wlan-slovenija-open-wireless-network
aliases:
- /post/46060905790/wlan-slovenija-open-wireless-network-at
- /post/46060905790/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/eKFgWZO6Inw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*wlan slovenija* open wireless network at [International Summit for Community Wireless Networks
2010](http://wirelesssummit.org/).

<!--more-->
