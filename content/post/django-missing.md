---
title: django-missing
icon: link
publishDate: 2013-03-28 22:09:10-07:00
draft: false
tags:
- project
- django
- python
disqus_id: "46571414968"
slug: django-missing
aliases:
- /post/46571414968/django-missing
- /post/46571414968/
---

[This Django application](https://github.com/mitar/django-missing) bundles some common and useful features which have
not (yet) found a way into [Django](https://www.djangoproject.com/) itself.

<!--more-->
