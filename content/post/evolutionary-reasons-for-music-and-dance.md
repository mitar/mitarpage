---
title: Evolutionary reasons for music and dance
publishDate: 2014-02-22 17:12:10-07:00
draft: false
tags:
- music
- dance
- evolution
disqus_id: "77534240137"
slug: evolutionary-reasons-for-music-and-dance
aliases:
- /post/77534240137/evolutionary-reasons-for-music-and-dance
- /post/77534240137/
---

Isn't it interesting how music and dance are so widespread among human cultures? How music makes us move, how we feel
that we should respond to music? But why we feel like that? Do other species have music and dancing as well? Not one
which is hard-coded in their genes, but one where they can improvise, be creative, go crazy? Do monkeys sing? Is maybe
music the reason why we shaped our language and intellect the way we have? To be able to sign together, to be able to
create new music not yet made in the past and impress our peers? To move our body in new ways not really necessary for
our survival, in a way maybe none of our ancestors ever moved?

<!--more-->
