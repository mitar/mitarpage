---
title: Hacking capitalism with capped returns
icon: link
publishDate: 2016-06-23 10:30:10-07:00
draft: false
tags:
- investments
- cappedreturns
- capitalism
disqus_id: "146363905420"
slug: hacking-capitalism-with-capped-returns
aliases:
- /post/146363905420/hacking-capitalism-with-capped-returns
- /post/146363905420/
---

A [post](http://joshuavial.com/capped-returns/) by [Joshua Vial](http://joshuavial.com/).

<!--more-->
