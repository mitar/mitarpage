---
title: Are corporations a new form of slavery?
publishDate: 2014-03-29 07:30:10-07:00
draft: false
tags:
- slavery
- corporations
- workers
- abolition
disqus_id: "81075000541"
slug: are-corporations-a-new-form-of-slavery
aliases:
- /post/81075000541/are-corporations-a-new-form-of-slavery
- /post/81075000541/
---

It is interesting to think about the whole concept of shareholding and corporations. It seems like a [modern form of
slavery](/post/if-corporations-have-rights-of-fourteenth). Because you cannot own a person anymore we developed a concept of
corporate personhood, which we can own, sell, resell, split, merge, while it is doing work for you, its owner.

<!--more-->

Instead of directly owning people we created an abstraction layer in between. This abstraction layer, a corporate
person, does work for you, you own it, you can benefit from it without having to really work, just by mere fact of
owning it, you are getting benefits, interests. You exploit the corporation. Sometimes bad owners push it too far, want
too much profit, so the corporation cannot sustain itself anymore, it bankrupts, it dies.

You can see an example of this in [factories taken over by workers in
Argentina](https://en.wikipedia.org/wiki/Workers%27_self-management#The_f.C3.A1bricas_recuperadas_movement). Owners were
not getting enough profit from factories so they decided to close them. But for workers factories were producing enough
income to cover their wages. Once factories become free of owners they could live, before they would die.

Corporations are the victim here. And through corporations, all the people working in them. Corporations are a tool to
own people.

Who is an owner? Who is a shareholder? In contrast with workers and worker-owners they do not really work in the
corporation, they do not contribute to corporation's business through personal work. They have control over the
corporation. Owners pay other owners when they buy a (share of a) corporation, they do not pay workers, they do not pay
the corporation.

Shareholding is an interesting concept as well. Instead of one owning a whole slave, you own just s small small portion
of it. Then we can maybe say that nobody is really a slave owner, because that small portion is insignificant, but all
together, collectively, we are still slave owners? Are we then collectively responsible? Or are we more diffusing our
collective responsibility in this way? We pretend that when we buy a share, we are not really buying part of a real
person who will now work for us? Because now almost anybody can be a slave owner, own at least one share, we cannot
admit to ourselves what we are? We talk about how our share appreciate through time, just by itself, without us having
to do any work. We just had to invest. Is there a big difference between past slave markets and current stock exchanges?

If we look how wealth moved after slavery was officially abolished, same privileged people who owned slaves directly
before, could own corporations instead, which then employed workers. Power relations did not change much. Even if
workers are now employed, their income is still small in comparison with owners. Their income more or less just covers
life expenses, similar to how slaves were given food to survive. It would be interesting to compare how much food and
housing slaves were given in comparison to their owners, and how much food and housing workers in corporations are given
in comparison to corporations' owners.

To be precise, through corporations we do not own other people directly, just their work. (Which is arguably the main
reason why we owned people in the first place.) We can then sell and resell the ownership of work on stock exchanges. We
introduced a layer of abstraction between work and its ownership. And if needed, we can add more layers of abstractions
by introducing more corporations owning other corporations. To be precise, workers in corporations can theoretically
freely decide to move to another corporation, so they are not stuck with their owners. But this freedom is quite
theoretical if you know how hard is to get a job this days. And if you can freely chose between your owners, but have to
stay in the system, have to stay under a corporation owned by somebody, is this really a freedom? Of course you could
always buy yourself real freedom, but where would you get money for that?

I do not believe corporations developed this way consciously. They developed as a way to limit liability, but once we
added ways to sell them and since our economy is based on debt and workers in most cases work to pay off a debt, we have
slavery again.

I do recognize that real slaves were and are in much worse situation than many current workers in corporations and I do
not want to minimize their suffering and issues, but on the other hand I want to show that we maybe officially replaced
slavery with something more human and civilized, but maybe conceptually not much different.

Owning a corporation allows owning a person. Or at least person's work.
