---
title: My presentation of PeerLibrary at OpenCon 2014
icon: video
publishDate: 2014-11-22 02:59:10-07:00
draft: false
tags:
- opencon2014
- opencon
- project
- media
- openaccess
- oer
- opendata
disqus_id: "103272440376"
slug: my-presentation-of-peerlibrary-at-opencon-2014
aliases:
- /post/103272440376/my-presentation-of-peerlibrary-at-opencon-2014
- /post/103272440376/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/xfIG2vorf7g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

My presentation of PeerLibrary at [OpenCon 2014](http://opencon2014.org/), the student and early career researcher
conference on Open Access, Open Education, and Open Data.
[Slides](https://peerlibrary.github.io/outreach/slides/2014-11-16-opencon/).

<!--more-->
