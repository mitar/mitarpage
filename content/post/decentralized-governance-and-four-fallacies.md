---
title: Decentralized governance and four fallacies
publishDate: 2016-09-18 01:40:10-07:00
draft: false
tags:
- blockchain
- bitcoin
- decentralization
- governance
- structurelessness
disqus_id: "150576712596"
slug: decentralized-governance-and-four-fallacies
aliases:
- /post/150576712596/decentralized-governance-and-four-fallacies
- /post/150576712596/
---

Together with popularization of blockchain we can notice revived calls for decentralization of national and
international governments, their reboots, or even their dissolution. But such calls lack fundamental understanding of
how our governments operate, their role in our global society, and what all in fact regulate and control our existence
beyond just governments.

<!--more-->

Jo Freeman in her essay [The Tyranny of Structurelessness](http://www.jofreeman.com/joreen/tyranny.htm) reminds us that
humans tried to remove centers of power and organize themselves in a horizontal way in the past as well, and presents to
us issues with power relations she observed in a 1960s women's liberation group.

What she observed was that once you removed explicit centers of power, implicit and hidden centers of power emerge. I
see this is as a normal social phenomenon. More people we have to coordinate with, more effort and time coordination
requires from us. Especially as the community scales up and more people join. We start interacting more with those we
interact easily, and cliques emerge. Through time some of those gain more power than others because they do not
distribute their power outside the clique in the same way as inside. So power concentrates.

The difference is that this power is implicit, hidden. While in traditional communities with centralized structures we
established institutions for checks and balances, like transparency and accountability, those do not exist for implicit
centers of powers. The whole concept of a "public service" is based around public being able to oversee the service. But
once we remove those public services and replace them with private services we loose mechanisms to oversee their
behavior. We are lucky if we can even detect that such [concentrations of power exist at
all](/post/sybil-attacks-and-shell-corporations).

Thus, I would name the first fallacy of decentralization: **a fallacy of structurelessness**. It is better to have clear
and explicit centers of powers with transparency and accountability than pretend that we have decentralization, just for
those centers of power to emerge hidden and unchecked.

Moreover, just having a decentralized underlying technology like blockchain does not mean that the community using it is
decentralized. Or that using a centralized technology means that the community is centralized. Wikipedia and many open
source projects can be seen as communities with centralized technologies which help the community operate together, but
the community itself is decentralized and global. On the other hand, Bitcoin mining power has concentrated in few mining
pools, despite technology being decentralized. This leads to the second fallacy: **a fallacy of equating
decentralization of power with decentralization of structure**.

Those building such new decentralized systems to replace existing governments and institutions often become victims of
**a fallacy of beautiful models and designs**. Our minds are limited. To be able to think about complex processes we
create simplified models using many assumptions and reductions. We love such models. Simplicity is beautiful.

Those tools are important to help us give insight into those complex processes, but we have to be wary of starting to
believe that this is all there is to those processes. That we can then just create code which encodes this simplified
models and require humans to adapt their behavior to the code. We can observe that in modern urbanism where we segment
cities into clear and predictable areas of different uses. This makes it easier to reason about cities, but it
establishes artificial limits on possible interactions between people living in those cities. We get commuter parts of a
city where people just sleep over the night. And parts of a city like financial districts where people work just over
the day, but over the night nobody is around. This makes those cities and communities fragile. So different from old
cities like Jerusalem.

Such models are maybe locally useful, but globally restricting. It is simply not possible to make one model, one design,
which will serve everybody. Nobody likes to make programs with a long list of exceptions, but I would claim that this is
what we should start doing. We should start making programs which can be messy. Be easily adapted, changed, modified.
[Web is like that](/post/towards-layered-re-decentralized-web). Wikis are like that.
[Programs should also be like that](/post/wikiprogramming-editing-a-program).

Arthur Brock talks about this by describing [how cryptocurrencies have crises of
governance](https://medium.com/metacurrency-project/cryptocurrencies-are-dead-d4223154d783) because they have removed
people from the design. It is easier to design things without messy people, but then do not be surprised when design
collapses in practice.

Lawrence Lessig [discusses the idea that there are four modalities which regulate
us](https://www.socialtext.net/codev2/what_things_regulate): [the law, social norms, the market, and
architecture/structure](https://en.wikipedia.org/wiki/Pathetic_dot_theory). Of course they also interact with each other
and are dynamic, changing. On the Internet, we can see architecture/structure in a form of the code which governs cyber
spaces we inhabit. Lessig talks how the government has multiple ways to regulate, not just by passing laws. And not just
the government, but everyone who controls cyber spaces. Moreover, he talks also about the danger of moving from direct
and explicit regulation by laws to implicit and indirect. For example, by changing the architecture/structure. Instead
of using laws to legalize segregation one can use highways without easy crossings and railroad tracks to divide
communities. Such urban changes still regulate, but we lose on transparency and oversight. It is a similar issue to the
previously described fallacy of structurelessness. Those issues exist online as well. We replaced regulation of the
cyberspace by law with the code operated by private companies. Who oversees that code? Does public have any say in it?
Do you know what Facebook removes from your feed and which videos are deleted from YouTube?

But for this discussion much more important is realization how many things regulate us and how are they all intertwined,
besides just law. We developed through centuries so many social institutions for all bugs and issues we discovered in
our social and economic processes. We might think those institutions are rigid and slow, unneeded, and obsolete. But
this is often a consequence of a fallacy of beautiful models and designs. We just do not realize all the reasons for
some things to exist. Yes, we could often improve them and modernize them. But it is **a fallacy to** believe we can
**reboot a living system from scratch**.

Through that we lose all the knowledge embedded inside those institutions. Most of them we are not even realizing we
have. Many of them are there to protect us against misuses and mistakes which can happen. Inefficiencies are sometimes a
feature to allow for a human to detect a problem.

Every founder of a community can tell you how many small things are necessary to establish for a community to really
live, and especially to make it pass the first generation. Even in software we are often [unable to rewrite the code
from scratch successfully](http://www.joelonsoftware.com/articles/fog0000000069.html). And we would want to do this for
such a complicated ecosystem as our society and economy is?

Current system is a mess, but how can we know that without it we would be better of? We already had unrestricted
capitalism in 19th century and learned that it has shortcomings for those who do not own the capital and means of
production. How we know that by removing safety belts embedded inside current institutions the old oligarchies will not
reappear? That we will not have again the wild wild west. Just this time, instead of six barrel manual guns we will have
assault weapons and cyber attacks at a disposal of every individual.

The DAO hack was an example of such realization. We are unable to write perfect contracts. This is why we created the
judicial system. It is not perfect, but it is still better than what was at the disposal to the community to do in the
case of the DAO attack. At the end, effectively rebooting the system to try again.

The main danger is when these fallacies combine. They reinforce each other. They make us think that we can reboot the
society because we designed a beautiful, decentralized, simple, and generalized model, which we encoded into a protocol,
code. But by doing that we forgot about humans and what makes us humans. The chaos of all exceptions and special cases.
Concentrations of power because we have friends. Because we like some people more than other. Because we do trust some
people. **It is not simple. It never is simple. But that is OK.**
