---
title: Thank you USA for making universal health care all around the world possible
publishDate: 2013-10-05 22:41:10-07:00
draft: false
tags:
- healthcare
- usa
- universalhealthcare
disqus_id: "63245757990"
slug: thank-you-usa-for-making-universal-health-care-all
aliases:
- /post/63245757990/thank-you-usa-for-making-universal-health-care-all
- /post/63245757990/
---

Today, health care related products and services markets are global. So thank you USA for having so [ridiculously
high](http://www.youtube.com/watch?v=qSjGouBmo0M) costs of health care because this allows all other countries to
negotiate lower costs for their universal health care, while corporations still having enough income to research and
develop new cures and equipment. Without you, USA, we would all have to pay higher prices, all around the world. Thank
you for deciding to carry this burden yourself.

<!--more-->
