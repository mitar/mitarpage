---
title: 'Let''s simplify taxes: tax all transactions'
publishDate: 2015-07-20 10:30:10-07:00
draft: false
tags:
- taxes
- taxation
- taxhavens
disqus_id: "124587412249"
slug: lets-simplify-taxes-tax-all-transactions
aliases:
- /post/124587412249/lets-simplify-taxes-tax-all-transactions
- /post/124587412249/
---

Have you ever looked into laws governing taxes? The tax code? I cannot believe that anyone can really adhere to all the
rules found there. I can understand that as a living organism they evolved through time and new and new rules were added
to patch holes from before, to support new technologies, new economic practices, etc. But maybe it is a time for a
reboot? If laws are too complex to comprehend and follow, do they still hold their social contract?

<!--more-->

Moreover, the situation additionally reinforces existing power relations in a society. Those with more power and more
knowledge have easier time (or can obtain help) figuring out how to navigate all those rules to their own benefit. This
is why big corporations and wealthy individuals are paying so little taxes and know how to take the advantage of tax
havens.

We could simplify tax code. Simply, every monetary transaction would be taxed. No matter if it is buying or selling
food, apartments, or companies. Each year, for everyone, we would compute tax on the accumulated absolute value of all
their incoming and outgoing transactions. Each transaction contributes to the yearly accumulated sum of both parties in
the transaction.

In this way it would not matter in which way you achieved your wealth. If you want to use it, you have to pay tax on it.
If you do not have a lot, you pay less, if you have more, you pay more. If you wire money out of the country, you still
pay taxes, so there is less incentive for wiring it out (or you spend it in small transactions in the country, or you
make one large to get the money out of the country, it is the same). If you receive money, you pay taxes even if you
have not spend the money. And you pay again when you spend it.

Many taxes already work like that so many mechanisms to record such transactions are already in place. For example, when
you buy things in stores. This would be just generalization and simplification.

Then we just have to decide on the exact function used to compute the tax from the value of all your transactions. How
much we want to tax the wealth. Should it be a progressive tax? Do we want to have any minimal limit under which you do
not have to pay any taxes? These are then political questions.
