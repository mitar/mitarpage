---
title: Seats on airplanes
publishDate: 2013-09-24 04:26:10-07:00
draft: false
tags:
- idea
- airplanes
- seats
disqus_id: "62145195518"
slug: seats-on-airplanes
aliases:
- /post/62145195518/seats-on-airplanes
- /post/62145195518/
---

Why, when choosing an airplane seat, you cannot say that you do not care where you seat, just that you seat next to
somebody interesting (listing your interests)? Sitting next to somebody for 10 hours could be a great opportunity to
exchange ideas, broaden horizons, and argue about the future of the planet.

<!--more-->
