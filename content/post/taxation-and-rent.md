---
title: Taxation and rent
publishDate: 2016-04-08 23:47:10-07:00
draft: false
tags:
- taxation
- rent
disqus_id: "142501540981"
slug: taxation-and-rent
aliases:
- /post/142501540981/taxation-and-rent
- /post/142501540981/
---

Those who claim that taxation is theft, why is it any different from rent? Just because one has an implicit contract,
and another an explicit? If you do not like the contract, you can always leave for another country. Oh, you cannot?
Maybe you are just not trying enough, like those who cannot afford your rents. So see, taxation is simply a rent for you
to be in a country.

<!--more-->
