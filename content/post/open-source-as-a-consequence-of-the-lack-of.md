---
title: Open source as a consequence of the lack of programmers?
publishDate: 2013-09-16 09:43:10-07:00
draft: false
tags:
- opensource
- laziness
- unions
- collaboration
disqus_id: "61415315656"
slug: open-source-as-a-consequence-of-the-lack-of
aliases:
- /post/61415315656/open-source-as-a-consequence-of-the-lack-of
- /post/61415315656/
---

Is open source possible because of the general lack of programmers so we do not even need an union to be able to require
from our employers perks? Employers do not have much space to negotiate if they want to do their businesses. As workers
we are then so lazy that we rather cooperate with others from other companies then (re)do all their work ourselves. In
which other industry you can say to the boss that you do not feel to do the mundane task which was already done by
somebody else and you would rather just work together with that colleague from the other company and let the boss
imagine some other business model instead?

<!--more-->
