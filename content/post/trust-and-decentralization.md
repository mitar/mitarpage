---
title: Trust and decentralization
publishDate: 2014-11-24 12:12:10-07:00
draft: false
tags:
- nsa
- surveillance
- collaboration
- wikipedia
- meshnetworks
- law
disqus_id: "103482798369"
slug: trust-and-decentralization
aliases:
- /post/103482798369/trust-and-decentralization
- /post/103482798369/
---

With recent NSA surveillance revelations many in the tech community started asking again for more decentralization and
encryption of our Internet tools and services. We should build technologies which allow us to minimize trust and
dependencies on others. Tools which allow each of us to self-protect ourselves. A new wave of such tools is emerging and
while I agree that it is good that we have such tools, the question is if we want to live in such a society. In which
society you would like to live? In society which is build around not trusting anyone. Or in society where you can trust
your fellow beings and where we handle rare exceptions, misuses of trust?

<!--more-->

I am not proposing any changes in direction of technological development of such decentralized tools and services, or
our adoption of them. But I think we should rethink our perspective on the issue and solutions. We should change our
mindset when we are developing these tools.

Instead of thinking how we want to minimize trust of others, or even how to create zero-trust systems, we should know
that we can trust most other users on the Internet. That we can rely on them and that it is good that we have a
community where we cooperate based on the trust. The issue is that we do not know who we can trust and how to handle
cases of misuses of the trust. But this is then a good direction to think about. Let us develop ways to handle rare
exceptions, develop ways to detect exceptions in advance, maybe even prevent them. Instead of saying that because we
cannot trust few, let us not trust anybody, we should be saying let us trust everybody, but know how to handle well
those few who misuse the trust.

Decentralization can be viewed as means to combine/coordinate efforts of people towards a common goal.

For few years now I have been involved in [open wireless network in Slovenija, wlan slovenija](https://wlan-si.net/),
build around open source mesh technology. Mesh networks are now again becoming popular because they are decentralized.
But for me their decentralized nature is not important because it would give us protection against NSA (in fact security
properties of current mesh networks are pretty bad), but because it allows community to come together with little
coordination overhead and build a network together. A network where none might be before, providing Internet
connectivity to everyone.

Wikipedia is an encyclopedia anyone can edit. It is build around the [principle of assuming good
faith](https://en.wikipedia.org/wiki/Wikipedia:Assume_good_faith) of fellow editors. Of course this does not mean that
there are no misuses and vandalism. But it is build around trust, understanding that vandalism is a rare exception
compared to all the edits made on Wikipedia. And it works. Its editing is decentralized to anyone in the world with
little coordination between them.

Bitcoin is build in a decentralized manner in a way that each participant is trusted minimally. They work and vote
together and if more than 50% agree on something, this is taken as true. The idea is that it is highly improbable that
more than half of users will be corrupted or compromised. In this respect it is similar to Wikipedia, where we also
assume that most of the users will not be malicious. But the perspective is different. We do not assume good faith in
fellow users of Bitcoin.

So while basic ideas of distributed trust might be similar, the perspective is one which makes the difference.

There is research done based on the observation that you can often trust people in your social circles more. One can use
information about your friends from Facebook to build decentralized systems which are even more resistant to coordinated
attacks of malicious users. But this is not the type of trusting others I am talking about here. Trusting your friends
more is nothing special, of course you know them. But that you can trust a stranger, that is what makes society strong.
That you can trust even when you do not have any other information about a new person. The rest of society promises to
handle a misuse, if anything happens. Why are not we building technical systems which would work the same?

Sadly, in our current society it is not true that we always handle misuse of the trust properly. But we should then
strive for it. Instead of giving up on the society and collaboration and trying to build technological fixes, we should
work on fixing what we already have. Laws with holes and situations where those with power escape consequences of
misusing the power.
