---
title: Pay-it-forward cryptocurrency
publishDate: 2016-10-09 03:12:10-07:00
draft: false
tags:
- bitcoin
- blockchain
- cryptocurrency
- payitforward
- flow
disqus_id: "151555359931"
slug: pay-it-forward-cryptocurrency
aliases:
- /post/151555359931/pay-it-forward-cryptocurrency
- /post/151555359931/
---

Bitcoin, Ethereum, and blockchain in particular are often claimed as revolutionary as the Internet itself. They will
decentralize the Internet again, change how we make apps, empower end-users, and remove intermediaries. But are they
really so revolutionary? Even ignoring the technical limitations of scaling and power consumption, we can hardly imagine
such wide influence on our society as we observed for Internet. Internet connected people globally, provided means of
immediate communication and access to knowledge and information. It changed many aspects of our lives and how we as a
species operate. But blockchain, does it really have this potential?

<!--more-->

Take Bitcoin for example. Its technical innovations are undeniable, but as a currency itself, it lacks innovation. It is
still [a one-dimensional numeric-like value assigned to each
transaction](/post/limitations-of-bitcoin-revolution). If we are comparing it with
the Internet, then it is the Internet in the form where people would take physical books, scan them as images, and make
them available online. No interactivity, no multimedia, no searching or social capabilities, no additional value besides
digitizing the content in the most simple way. Bitcoin does the same. It just digitizes existing concept of money. It
does this in an interesting and innovative way, but it is still just a digital image of money.

How much can then Bitcoin as a technology really influence the society, if it just copying and digitizing existing
practices?

To answer this question I asked myself another question: how would a cryptocurrency which would not be just digitizing
existing practices look like? How would we design a new cryptocurrency which would by its design influence and change
the behavior of its users?

To be able to answer these questions I needed an additional insight about current cryptocurrencies: it is not true that
they provide just a one-dimensional numeric-like value. As a side-effect of their public nature **they** in fact
**provide a vector for each transaction**, direction and a magnitude of the value transfer.

Using this idea **we can design an anti-cryptocurrency**. If our current economy operates on debt, we can design a
cryptocurrency for an economy which operates on giving. If currently you lend money to somebody, you want to record this
fact to assure that they return the money to you eventually. In our cryptocurrency, you would record this fact to assure
that they give the money further and do not keep it for themselves. We know the direction of money, so we can make sure
that it never flows back. That one cannot return the money it received. If currently money transfer is instantaneous,
independent from your behavior and existing wealth, in our cryptocurrency it would be a process depending on your
behavior and existing wealth.

In short, **our cryptocurrency would be a pay-it-forward cryptocurrency**.

Its properties would be:

* Everyone can have only a non-negative amount of money.
* When somebody wants to send you money, they direct a flow of that amount of money towards you. Less money you have,
  quicker can that money flow towards you.
* Moreover, looking at the network of your past transactions, less money you sent in the general direction of the sender,
  quicker can that sender send you money.
* Once the sender directs a flow, they cannot cancel it anymore.
* The money once directed (even if not yet transferred completely) is reserved for that flow.

The less money you have, quicker and more money others can send you. You have incentive to send the money forward as
soon as possible, because only this is how you can get even more money. If you do not have any concrete reason to send
money, you can always donate it. If you do not know of anyone to donate to, you can always send it to a special address
for universal basic income. In this way you get rid of your money, and at regular intervals then all money from that
address is redistributed back to all users of the cryptocurrency. (How one would [assure unique users is another
problem](/post/one-person-one-vote-or-one-dollar-one-vote).)

One would still be able to pay in a store with this cryptocurrency. Only that store would have to have by itself little
money in its account to quickly receive your payment. A store can donate as well if needed.

There are some interesting details. Like if I direct a flow towards somebody with a lot of money, they are receiving my
money slowly, but then also I cannot receive money from others, because I still have money. So I should be selecting
recipients who do not have much money.

There are also some issues to be ironed out. Such cryptocurrency might incentivize wealthy people to hire other people
to be their wallets. They could distribute their wealth among those people-wallets. So some form of regulation might be
needed to forbid this. Or laws which would allow people-wallets to claim the money as theirs, if wanted.

Such cryptocurrency seems to be aligned with the idea of improving our economy by [maximizing the velocity of
money](https://www.youtube.com/watch?v=DQKQKCe1xl0).
