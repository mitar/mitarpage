---
title: django-pushserver
icon: link
publishDate: 2013-03-28 23:32:10-07:00
draft: false
tags:
- project
- django
- nginx
- httppush
- http
- python
- comet
disqus_id: "46575911996"
slug: django-pushserver
aliases:
- /post/46575911996/django-pushserver
- /post/46575911996/
---

[This Django application](https://pypi.python.org/pypi/django-pushserver) provides a push server for
[Django](https://www.djangoproject.com/) based on Leo Ponomarev's [Basic HTTP Push Relay
Protocol](http://pushmodule.slact.net/protocol.html). Useful especially while locally developing Django applications
using [Nginx HTTP push module](http://pushmodule.slact.net/).

<!--more-->
