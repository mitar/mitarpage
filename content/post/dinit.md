---
title: dinit
icon: link
publishDate: 2023-07-09 18:49:53.426426+00:00
draft: false
tags:
  - project
  - init
  - docker
  - go
slug: dinit
---

I made an opinionated init specially for Docker containers: [dinit](https://gitlab.com/tozd/dinit). It supports running and managing multiple
programs, their stdout and stderr, and handling signals, all in the way more suitable for Docker containers. Using it means no more resource
exhaustion from zombie processes, data loss by uncleanly terminated containers, stray daemonized processes that keep running, or hidden
issues because failed processes inside a container gets restarted instead of taking the whole container with them.

<!--more-->
