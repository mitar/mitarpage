---
title: PeerLibrary is calling for collaboration on open dataset of all academic publications
icon: video
publishDate: 2014-07-27 15:34:10-07:00
draft: false
tags:
- media
- project
- peerlibrary
- csvconf
- bibliography
- bibliographicdata
disqus_id: "93051170466"
slug: peerlibrary-is-calling-for-collaboration-on-open
aliases:
- /post/93051170466/peerlibrary-is-calling-for-collaboration-on-open
- /post/93051170466/
---

<iframe width="570" height="320" class="add-margin" src="https://www.youtube-nocookie.com/embed/cBXxRAxCr88" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

via [peerlibrary](https://blog.peerlibrary.org/post/93050996673/peerlibrary-is-calling-for-collaboration-on-open)

<!--more-->
