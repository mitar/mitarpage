---
title: Ideal of a economy in the society
publishDate: 2014-05-29 11:01:10-07:00
draft: false
tags:
- ideal
- economy
- society
disqus_id: "87217386871"
slug: ideal-of-a-economy-in-the-society
aliases:
- /post/87217386871/ideal-of-a-economy-in-the-society
- /post/87217386871/
---

I proposed the [new global economic system](/post/a-new-global-economic-system) which
allows [gradual transition from the current
system](/post/gradual-transition-to-the-new-global-economic). But to what would
such a system lead? What is the ideal organization of economy in the society?

<!--more-->

I do not know, but I like to imagine the following metaphor. Imagine a village with a big warehouse in the middle.
Whatever you need you go that warehouse, find it, and take it. Whatever you produce you take to the warehouse and leave
it there. You do not get any money for leaving things there, you do not spend any money to take things.

In front of the warehouse there is a bulletin board where people can request things so that you know what people would
like for somebody to produce. People work on anything they want, or nothing. In the era of abundance it is not important
if we all work or how much we work. It is not seen anymore that it is unfair if somebody works and somebody else does
not, because anyone works only if they want to anyway. And what exactly is work in such a society anyway? You live,
socialize, play, create, and if you decide to share what you created, it is easy. And this holds for all.
