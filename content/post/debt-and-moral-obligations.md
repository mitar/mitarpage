---
title: Debt and moral obligations
publishDate: 2015-07-05 05:58:10-07:00
draft: false
tags:
- greece
- debt
- interestfree
- banking
disqus_id: "123277729911"
slug: debt-and-moral-obligations
aliases:
- /post/123277729911/debt-and-moral-obligations
- /post/123277729911/
---

Those lazy Greeks. They should pay their debts! If you take a loan you are obliged to pay it back. But are you? Let's
assume that it is true, if you lend something then you should return it someday. But there is a big difference between
returning a loan and paying also the interest on that loan. Are you obliged to pay also the interest? And how much of
the interest should you pay?

<!--more-->

One could argue that it does not matter. If you accepted the terms of the loan, you should respect them. But these days
loans are made with interest which grows exponentially through time. The idea, I hear, is that you are not paying back
for the service of being given a loan, but you are paying for the potential profit that money could bring if it was used
differently, instead of it being lent to you. Longer you are returning the loan, longer could that money be used
differently, so the interest grows, exponentially with time. But here an important assumption is hidden. That money (or
capital) can produce an exponential growth of profits. This is of course a wet dream of investors, but in practice it is
simply impossible. Maybe you can see a temporary exponential growth, but in the longer term you will experience or crash
or devaluation. It is simply impossible to have an exponential growth with limited resources, space, and time available
on this planet. Should we then judge debtors by the standard which is in fact impossible to reproduce otherwise?

Where does this lead us concerning the debt? If you offer a contract with conditions which are in general impossible to
satisfy (except for few lucky ones which do manage to experience a temporary exponential growth and were able to pay the
loan back in time, before it got out of hands), and you trick somebody to accept them, who is to blame? The person (or
country) who needed a loan or you, who wanted not just a reasonable award for the service you offered, but were greedy
and wanted an exponentially growing award? And now you are complaining that a reality struck you? You were simply
greedy.

But what would be a reasonable award for giving loans? Luckily, there are already alternatives. For example, [there are
already interest-free banks](http://www.feasta.org/documents/review2/carrie2.htm) in existence. Or should we calculate
how much real profits would money produce if invested otherwise? Then in times of recession those profits might even be
negative, meaning that debtors would have to return even less than they took out.
