---
title: Filter to extract all clean water from soda and other drinks
publishDate: 2016-06-21 10:30:10-07:00
draft: false
tags:
- idea
- kickstarter
- water
- filter
- crowdfunding
disqus_id: "146264352919"
slug: filter-to-extract-all-clean-water-from-soda
aliases:
- /post/146264352919/filter-to-extract-all-clean-water-from-soda-and
- /post/146264352919/
---

Can somebody create a filter which extracts only clean water from soda and other drinks? No sugar, no colors, no
additives. Just pure water. It seems bottled water is more expensive than soda drinks, so, let's just use a filter.

<!--more-->
