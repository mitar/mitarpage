---
title: Is There Anything Good About Men?
icon: link
publishDate: 2013-07-27 07:31:10-07:00
draft: false
tags:
- roybaumeister
- men
- women
- society
- culture
disqus_id: "56605911699"
slug: is-there-anything-good-about-men
aliases:
- /post/56605911699/is-there-anything-good-about-men
- /post/56605911699/
---

I am not sure if I agree with everything in [this essay](http://www.denisdutton.com/baumeister.htm) by [Roy F.
Baumeister](https://en.wikipedia.org/wiki/Roy_Baumeister), but it contains some very interesting ideas on sources of
differences between men and women in the society.

<!--more-->
