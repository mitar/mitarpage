---
title: Questioning democracy
publishDate: 2013-04-16 16:13:10-07:00
draft: false
tags:
- democracy
- dictatorship
- questioning
disqus_id: "48153989412"
slug: questioning-democracy
aliases:
- /post/48153989412/questioning-democracy
- /post/48153989412/
---

Why it is not allowed to question democracy? Why this became a taboo? An idea you do not question? A human artifact you
do not want to improve? We wage wars in the name of the democracy. It is a stop word for any discussion: "This is not
democratic." Yes? Maybe? It is good to recognize this, but not to stop discussion because of that.

How many of you can count how many times democracy went wrong? How many of you can count how many times a dictatorship
turned good? Do you even know about benevolent dictators from our past? Do you believe that there have been none? Or you
just have not learned about them? I agree that we have come far in our search for a good system to govern ourselves, but
we must not stop our search here. We should be able to look in all directions for clues to guide us to the system which
would really have people at its core. Not just in the name.

<!--more-->

(I will not even go into an issue that there many different forms of democracy and many different forms of dictatorship
and everything in between.)
