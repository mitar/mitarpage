---
title: PiplMesh
icon: link
publishDate: 2013-03-28 23:44:10-07:00
draft: false
tags:
- project
- wlanslovenija
- piplmesh
- python
- mongodb
- django
disqus_id: "46576415315"
slug: piplmesh
aliases:
- /post/46576415315/piplmesh
- /post/46576415315/
---

[PiplMesh](http://dev.wlan-si.net/wiki/PiplMesh) is a social info portal for wireless networks combining social
interactions between users, wireless, mobile and locality-sensitive nature of their connectivity (the reach of what they
share is limited by the reach of the wireless signal), and relevant GIS-based information.

<!--more-->
