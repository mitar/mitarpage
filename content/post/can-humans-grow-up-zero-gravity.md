---
title: Can humans grow up in zero gravity?
publishDate: 2020-10-25 06:54:33.105158+00:00
draft: false
tags:
- growing
- learning
- zerogravity
- humans
slug: can-humans-grow-up-zero-gravity
---

Gravity seems to be our first and most important teacher.
Patient, consistent and always present.
Children can learn cause and effect through it.
What happens if I lift an object and let go?
Over and over again. On repeat. Always the same thing.

When we think about life in space. Can we raise children there?
Can children grow up in zero gravity?
Can they develop their mental abilities without having this teacher around them?
Or will they develop in other ways? A different kind of logic?

<!--more-->
