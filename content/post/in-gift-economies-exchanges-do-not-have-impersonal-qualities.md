---
title: In gift economies exchanges do not have the impersonal qualities of the capitalist
  marketplace
icon: link
publishDate: 2014-03-25 07:30:10-07:00
draft: false
tags:
- mauss
- gifteconomy
- gifts
- giving
disqus_id: "80678039030"
slug: in-gift-economies-exchanges-do-not-have-impersonal-qualities
aliases:
- /post/80678039030/in-gift-economies-mauss-argued-exchanges-do-not
- /post/80678039030/
---

> In gift economies, Mauss argued, exchanges do not have the impersonal qualities of the
> capitalist marketplace: In fact, even when objects of great value change hands, what
> really matters is the relations between the people; exchange is about creating
> friendships, or working out rivalries, or obligations, and only incidentally about
> moving around valuable goods.
>
> As a result everything becomes personally charged, even property: In gift economies,
> the most famous objects of wealth - heirloom necklaces, weapons, feather cloaks – always
> seem to develop personalities of their own.

[David Graeber on MAUSS](http://www.freewords.org/graeber.html).

<!--more-->
