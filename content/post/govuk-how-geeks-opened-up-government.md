---
title: 'Gov.uk: how geeks opened up government'
icon: link
publishDate: 2013-07-09 07:31:10-07:00
draft: false
tags:
- gov.uk
- government
- opendata
- opengovernment
disqus_id: "55001061544"
slug: govuk-how-geeks-opened-up-government
aliases:
- /post/55001061544/govuk-how-geeks-opened-up-government
- /post/55001061544/
---

A [nice example](https://www.theguardian.com/technology/video/2013/jun/13/geeks-opened-up-government-video)
how to approach open data and open government and how to build tools for public.

<!--more-->
