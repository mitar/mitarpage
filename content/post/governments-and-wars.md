---
title: Governments and wars
publishDate: 2013-04-19 17:59:10-07:00
draft: false
tags:
- government
- war
disqus_id: "48398311951"
slug: governments-and-wars
aliases:
- /post/48398311951/governments-and-wars
- /post/48398311951/
---

I believe that any government which is involved in the war (or any armed conflict, or in fact any conflict where human
rights are violated, or, maybe, even any act where human rights are violated) has failed its people and all other people
from all around the world. The government is doing something wrong and should be helped to find a way to stop the war
peacefully or be replaced with one which can do that. How can people make this change happen is often not obvious, but
definitely it must not be done violently, because this would defeat the main idea behind.

<!--more-->

The change should come from people governed and not from the outside. The people themselves have the means (and arguably
the right) over their own government. People themselves should oppose their government's decision not because they have
been attacked but because they are attacking. They should come into the defense of the people their government has
decided to attack. They should cooperate with people from the other side, the attacked. The other side should be doing
the same with their government. Because sooner or later there is no difference between attackers and defenders, they are
both attackers and they both should want their governments to stop attacking the other.

You can say: "They attacking us, we have to defend!" Yes, maybe. I am saying that the people of the country who is
attacking you should pressure their government. You can help those people by informing them, explaining to them,
discussing with them, working with them, giving them reasons why they should help you, you can provide them with ideas
how. World is now global, you can meet outside involved countries and discuss in-person and find a common solution,
people to people. World is now virtual, you can use Internet to communicate. You should at the same time pressure your
own government, because it might be that from the viewpoint of others, you are the attacking country, even if you
believe that you are just defending. You should do this with even larger vigor as you would want the people of the other
country to do for their government.

Many times governments involved in wars are democratically elected and even have an ongoing support by people. If you
want to create a change, you have to influence this support. You have to expose the reasons for the attack, inform and
educate the people, show the mistruths, false representations. I cannot believe that people from any side would want
war, but I can understand that they individually do not have a feeling that they can change anything. I can understand
that they believe that their government is doing the best for them in a given situation. That there is no other way. But
this is not true. They are not alone, they can work with other people. Moreover, the image of the situation can be
skewed, it might just look like it is the only way to deal with the situation. It is really simple, if we have not found
a solution how to not go into a war, we have not tried enough.

This is why we should become better investigators, better at exposing, better at telling what's wrong and why. Better at
being creative and imagine creative solutions. Better at discussing and better at listening. Especially to the people we
do not agree with or we just believe we do not agree with. We should not wait for somebody to lead us, for the
government to find a way to the peace, because it might be we will never see it.

I am not saying that people should overthrow the government involved in a war. The best is if they manage to change
things from inside the system, possibly in an innovative way which plays the system against itself. Create a new party?
Launch a new radio station? Organize demonstrations? Prepare and disseminate publications? Deploy Internet discussion
forums involving both sides. Organize mutual cultural events with artists from all sides? Maybe current government is
simply not capable of finding a better solution, presenting such a solution and helping the government could already be
enough.

People have advantages. They are many. They have bigger imagination. They are diverse. They can create ideas from all
possible walks of life. They can imagine completely new ways to change things and life peacefully with a other
countries, especially with other people. But they have to first want to make a change.

It is important to remember that governments might declare wars and invoke conflicts, but people wage them. If
governments are unable to stop wars and prevent conflicts, people can do that. In the past, there were not many ways for
people from different countries to directly work together and decide on a peace. Their only possibility of communication
was through their respective governments. But today this is not true anymore. People on both sides can decide for a
mutual peace no matter what their governments decide. They can communicate and coordinate. They can make a change. They
can learn about each other, understand each other, help each other.
