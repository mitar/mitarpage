---
title: PeerLibrary won the Highest Impact award at Meteor Hackathon 2013
icon: link
publishDate: 2013-07-10 13:57:10-07:00
draft: false
tags:
- peerlibrary
- meteor
- hackathon
- project
disqus_id: "55112824602"
slug: peerlibrary-won-the-highest-impact-award-at-meteor
aliases:
- /post/55112824602/peerlibrary-won-the-highest-impact-award-at-meteor
- /post/55112824602/
---

[PeerLibrary](https://github.com/peerlibrary/peerlibrary), a project I am currently working on, won the award for
Highest Impact at [Meteor Hackathon
2013](http://www.meteor.com/blog/2013/07/09/congratulations-to-the-meteor-summer-hackathon-2013-teams). This is how they
describe the project:

> Peer Library is like Rap Genius for academia that adds crowdsourced notetaking to academic papers. Users import
> academic papers in PDF format, view papers in-browser from the library, and make inline comments on selected text in
> each paper. Users can share those comments and view other people's notes in real time. They also implemented searching
> for papers by title or journal. Team: Mitar, Gheric Speiginer, [Tony Chen](http://tonychen.me/).

<!--more-->
