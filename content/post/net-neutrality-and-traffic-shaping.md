---
title: Net neutrality and traffic shaping
publishDate: 2014-03-11 03:00:10-07:00
draft: false
tags:
- netneutrality
- qos
- trafficshaping
disqus_id: "79252331627"
slug: net-neutrality-and-traffic-shaping
aliases:
- /post/79252331627/net-neutrality-and-traffic-shaping
- /post/79252331627/
---

[Net neutrality](https://en.wikipedia.org/wiki/Net_neutrality) is an important feature of the Internet, but it is at
risk. Net neutrality means that Internet service provides must not discriminate and must transport all traffic with same
priority and quality. ISPs would like to be able to discriminate because this opens new profit sources for ISPs. For
example, they can start asking various Internet services to pay them, or their traffic will have lower priority than
some other competing web service.

Profit is not the only reason why would somebody like to discriminate traffic on the Internet. One more reasonable
reason is that sometimes to assure good quality of service, it is better for ISP to prioritize, for example, latency
sensitive [VoIP](https://en.wikipedia.org/wiki/VoIP) protocol over bulk download of the movie. This means that you can
still use high quality voice communication over the Internet, while at the same time consume your whole link for movie
download.

But one can look at the issue from the other perspective as well. The issue of net neutrality is the issue of a power
relation. Because ISP has power to decide on the [traffic shaping
policy](https://en.wikipedia.org/wiki/Traffic_shaping), this power can be misused or at least used against end-user's
wishes. The solution is thus simple. End-user ought to be the one deciding on priorities of their traffic. ISPs ought to
provide a protocol through which end-user can request how ISP ought to shape incoming traffic (outgoing traffic end-user
can shape by themself). ISP can provide a default (high priority for VoIP), but end-user should be able to change those
rules and add their own rules. If traffic shaping is done with the consent of the user then this is not discrimination
anymore. But a feature, and it still serves the purpose of higher quality of service for the end-user and better user
experience with ISP's service.

<!--more-->
