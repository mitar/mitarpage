---
title: Gradual transition to the new global economic system
publishDate: 2014-05-25 02:41:10-07:00
draft: false
tags:
- economicsystem
- economy
- transition
disqus_id: "86778285751"
slug: gradual-transition-to-the-new-global-economic
aliases:
- /post/86778285751/gradual-transition-to-the-new-global-economic
- /post/86778285751/
---

I wrote about the [new global economic system](/post/a-new-global-economic-system) in
the past. One property I like is that it provides a simple and gradual transition to it.

<!--more-->

Imagine a hairdresser. The hairdresser could start participating in the new economic system by starting offering few
haircuts through it. Other people participating in the new economic system would apply for those haircuts and the
hairdresser would based on their past transactions inside the new economic system determine for which of all applied
would do haircuts. For each of those haircuts, both the hairdresser and a person receiving a haircut would leave each
other a reference, recording and evaluating a transaction. Such a record can be highly subjective, personal, elaborate,
and even objective, all at the same time through various ways transactions could be described. For example, simply as
free form description, optionally expressing general sentiment of a transaction by choosing from a predefined set of
values. Important is to notice that both a person performing a service (hairdresser) and a person receiving it get a
reference and feedback, not just the hairdresser. In this way it is not just important how you performed your service,
but also how you received it. The hairdresser could be very entertaining while performing a haircut and people could
like that and describe that recommendations. But at the same time also a particular person receiving a haircut could
have many interesting stories to share, making hairdresser's service more pleasant. With these new references and a
recorded transaction both of them can in the future then engage in the new economic system with others.

In addition, the hairdresser has benefits even in the current economic system. References received can be used to
encourage customers to buy hairdresser's haircuts. References and transactions can serve in a way as a alternative to
[Yelp](http://www.yelp.com).
