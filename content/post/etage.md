---
title: Etage
icon: link
publishDate: 2013-03-23 02:45:10-07:00
draft: false
tags:
- project
- haskell
- dataflow
disqus_id: "46061126323"
slug: etage
aliases:
- /post/46061126323/etage
- /post/46061126323/
---

[Etage](http://hackage.haskell.org/package/Etage) is a general data-flow framework for
[Haskell](http://www.haskell.org/) featuring nondeterminism, laziness and neurological pseudo-terminology. It can be
used for example for data-flow computations or event propagation networks.

There is also an additional package with [data-flow based graph
algorithms](http://hackage.haskell.org/package/Etage-Graph).

<!--more-->
