---
title: Free as in … ? My LibrePlanet 2016 talk
icon: link
publishDate: 2016-06-28 10:30:10-07:00
draft: false
tags:
- luisvilla
- capabilitytheory
- freedoms
disqus_id: "146611600451"
slug: free-as-in-my-libreplanet-2016-talk
aliases:
- /post/146611600451/free-as-in-my-libreplanet-2016-talk-luis
- /post/146611600451/
---

[Interesting talk](https://lu.is/blog/2016/03/23/free-as-in-my-libreplanet-2016-talk/) by [Luis Villa](https://lu.is/) pointing out that freedoms are
not enough, but we have to aim for empowering people to have [capabilities](https://en.wikipedia.org/wiki/Capability_approach) to exercise those freedoms.

An example of this can be found in free software where just a freedom to run a software is not always enough because it
does not address that many people cannot do much with such an abstract freedom. We should build software which gives
people capability to run them.

<!--more-->
