---
title: Towards a better group decision-making
publishDate: 2014-01-20 01:14:10-07:00
draft: false
tags:
- democracy
- delegation
- decisionmaking
- voting
- onlinevoting
- project
disqus_id: "73933896602"
slug: towards-a-better-group-decision-making
aliases:
- /post/73933896602/towards-a-better-group-decision-making
- /post/73933896602/
---

One more old document explaining technical background of the voting system [presented in the previous
post](/post/massive-online-collaborative-decision-making). Historically it was
made few years before the ideas of the previous post, so it does not describe all the properties needed for it. But it
does explain how to achieve that just a smaller number of people have to vote and you can still have some understanding
what is the opinion of the whole population of a group. This is a necessary property when you want to vote often and on
many different topics. We cannot assume that all members of the group will have time and/or knowledge to decide on all
those questions, but we believe that they at least know who in their social network is capable of making such a decision
and at the same time share their values. So they can delegate voting on this particular questions to them.

<!--more-->

## Finding out what the whole group wants ##

Currently the most common way of a group decision-making is voting with simple vote counting, determining the result by
the majority. Such a scheme is easy to understand and implement, but the question is if it does the most important thing
in the best possible way. Namely, the main purpose of a group decision-making scheme is to give the result which would
satisfy the whole group the most.

Theory sounds good, everybody in the group expresses her opinion and by majority you can determine the result which
would satisfy the group the most. But in practice it is quite different, group members can have time, knowledge,
information, motivational and/or other constraints which prevent them from expressing their opinion in an optimal way.
This is even more important if we would like to use such scheme on a regular basis on a variety of subjects, for
example, in a horizontally organized open source project, an association or even at a state level.

If voting is used regularly, not everybody can participate every time so it is important to be able to infer their
votes, for them not to be at a disadvantage. If used on a broad spectrum of decision topics, not everybody has necessary
knowledge and information to draw an opinion, but at the same time consequences of decision-making still affect them. So
it is important that the scheme would allow such group members not to vote while still taking them into the account when
determining the results, not simply discarding them out of the equation in the way simple vote counting is doing. And if
used in a group where voting participation is generally low, good voting scheme should still determine the results which
would satisfy the whole group the most.

One approach to the problem of determining the results is to see it as a prediction problem: we have a subset of votes
and we would like to infer how would the whole population vote if it would be able to.

In this text I present a proposal for a group decision-making scheme aimed at solving the above-mentioned issues. It
uses additional information of a social/trust network in the group to infer votes for members who have not cast them
themselves.

This scheme is targeting electronic voting, which is the form of voting that can be done on a regular basis, where
additional information can be collected, and complex algorithms can be employed to determine the results.

I will first present the proposed scheme, suitable for general decision-making in groups, and then show some ideas how
to use it in different contexts.

## The scheme ##

The scheme is a two-stage process. In the first stage every member of a group chooses zero or more other members who she
trusts and would delegate her voting decision to in the case she does not cast a vote herself. In the case she chooses
nobody, her vote is discarded if she does not cast a vote. In the case of more than one delegates, her one vote is
inferred from delegates in chosen ratios, summing together to her one vote.

The first stage can be done only once but members could also change their delegates at will later on, probably with some
limitations on how often in a given time span this can be done. The important thing is that this stage can be done less
frequently than the second stage.

In fact, this can be generalized somehow in a way that a member chooses one or more other members as delegates for her
one vote with chosen ratios with an addition of selecting whether her own vote casting has a precedence (in the case
that she casts a vote) or not. Default if somebody does not do the first stage could be that a member has only a
precedence delegation on herself. Although maybe the first stage should simply be compulsory for everybody.

Examples on how could a member \\(A\\) delegate her voting decision in the first stage. We have three members, \\(A\\),
\\(B\\) and \\(C\\). \\(A\\) can decide to delegate like this:

$$(A, *), (B, 0.4), (C, 0.6)$$

\\(A\\) has a precedence vote for herself but in the case she does not cast a vote, her vote is inferred \\(0.4\\) from
\\(B\\) and \\(0.6\\) from \\(C\\). If she casts a vote, only her vote counts.

$$(A, *)$$

This is a default which means her vote counts only if she casts a vote.

$$(A, 0.9), (B, 0.04), (C, 0.06)$$

This is without a precedence vote which means that if she casts a vote then \\(0.9\\) of her vote is counted, but still
\\(0.04\\) and \\(0.06\\) is inferred from \\(B\\) and \\(C\\), respectively. If she does not cast a vote, then her vote
is inferred from \\(B\\) and \\(C\\) in \\(0.4\\) and \\(0.6\\) shares.

There could be also another possibility how to interpret these delegations: in the third example, in the case that
\\(A\\) would not cast a vote, only \\(0.04\\) and \\(0.06\\) of a vote would be inferred -- no normalization to the
whole vote would be done. But I see this as problematic as it would dilute the main difference between current voting
schemes and the proposed scheme: it would make a vote (or part of it) to be lost if a member does not vote. This losing
of votes is also the problem with default delegation mentioned above (when a member does not vote, her vote is lost),
but such default is proposed to ease transition. Later on I propose some other default delegations.

Even more possibilities would make the scheme even more complicated. On the one hand this would allow people to have
more freedom to choose the combination they like. But on the other hand this would also make scheme so complicated that
it would be hard to understand all possibilities and (especially important) to reason about them and how different
possibilities influence the whole scheme. So maybe it is better to chose only these few (which are already numerous)
possibilities, but to choose them well and to reason about their implications well. I see chosen possibilities as a good
compromise for initial research on the subject, but of course also other possibilities could be researched.

So now we have all this delegations. This defines a (social) network between members which is a kind of "web of trust"
or "trust network". We can see it as a directed graph between (hopefully) everybody. We call this a "delegation
network".

When a decision is needed, votes are cast. It is not required that everybody casts a vote. For those who do not cast a
vote, their vote is inferred. But even more than that. This is done transitively. So in the last example above, if also
\\(B\\) (next to \\(A\\)) does not cast a vote, then \\(B\\)'s \\(0.4\\) share is inferred from \\(B\\)'s own
delegations. In the case nobody of \\(B\\)'s delegates (transitively) casts a vote, then \\(A\\)'s vote is wholly
inferred just from \\(C\\)'s vote.

This last possibility is an unfortunate one as it means we have lost votes: of \\(B\\) and all \\(B\\)'s delegates who
have not voted, transitively. This is probably a very unlikely event (based on the six degrees of separation idea) and
possible only if we allow members to not choose any delegates (we could change default delegation or make it compulsory
to delegate in the first phase).

This is it. Inferred votes are calculated transitively and votes, real and inferred, are counted.

## Discussion and variations ##

In this way everybody participates in the decision making even if they do not vote in a particular instance of vote
casting.

The key insight is that many people currently cast votes based on opinions and decisions of their friends. So we can
generalize and formalize this informal practice, make it a part of the scheme, making it not just a legitimate practice,
but the integral part of the scheme.

There are different variations of the idea, based on the scale of its use. On a smaller scale, for example, on a scale
of associations or other NGOs who might use such voting scheme to do decision making in their organization, the proposed
scheme could be used directly. So also members who might not be able to attend a given organization's meeting could
still indirectly influence the results through members who are attending the meeting.

At a state level it would probably be better to define a number of delegation networks for every area of state's
conduct. Of course the question is then which areas should be defined and to which of these areas should a given issue
be assigned and who would assign this. This could open a window to manipulation: you categorize an issue to an area
where delegation network is better suited to your own agenda.

To solve this problem, we can again use the proposed scheme. First we have a voting round on to which areas the issue
belongs and then in the next voting round we calculate the result based on the first round ratios between determined
areas. We do not use an absolute winner of the first round but just weight accordingly to the second round's results of
different areas and their delegation networks.

There are also two kinds of this voting scheme whether we deal with a limited (known) population or unlimited
population. An example of the first would be a state which has a known index of all eligible voters. An example of the
second is an open community on the Internet where everybody can join (and thus gets a vote). Because of this there is no
limit on number of votes so everybody can steer the results into her own direction if she gets enough of her friends to
join.

This is why there exist two perspectives on a decision. A group perspective is used when population is limited and the
decision represents the will of the whole population. An individual perspective is used when population is unlimited and
the decision represents a decision important mostly just to one individual and is probably different for somebody else.

Maybe an example is due to present these two ideas better. For the first one making decisions on state affairs is a good
example. And for the second one a collaborative moderation tool could be an example: users vote if a website comment is
abusive or not. You do not really care what is the real "truth", you just care that what you get moderated is something
you would say that it is abusive and that what you do not get moderated is something you would not say it is abusive.
You delegate this decision to friends you believe have a similar taste of what is abusive and what is not and have read
a given comment before you. It does not matter if some abuser gets millions of her friends to vote, as you and your
friends and their friends... do not infer their votes from those millions of abuser's friends. And even if somebody
would do so, it would mostly be just a rare exception.

The other such system with individual perspective, already deployed, is a PGP/GPG web of trust approach to users'
certificates validation.

As I wrote before there are also different possibilities what to choose for a delegation default. What I have proposed
above is based on the currently pervasive voting scheme, where if you do not cast a vote, your vote does not count in
any way. But other possibilities are also possible. For example, that the default is that your parents have equal share
of your vote in the case you do not vote, or person's husband, or children, or some other social person/group you
identify with.

Different number of votes (numerical maximum, not just 1.0) could be given to members. Based, for example, on values of
a given group this voting scheme is applied in. My proposal is based on current values found in modern societies:
everybody gets one equally valued vote. But next to this, the voting scheme allows also that members decide (for example
in a company) that number of votes depends on the stock share, or in some (dystopian) society on IQ or some other
metric.

Furthermore, in the case multiple options are given for a decision, we could, instead of one preferred choice, rank
choices in preference and then use this as an input to the delegation network.

One problem of proposed voting scheme is that it is not easily understandable. Traditional voting is easy to reason
about and people understand why somebody lost and why somebody won. Here the loser would be able to stir this complexity
for her own agenda and disagree with the results (or argue about corruption). This could be mitigated partially by using
open source technologies and public information on the systems employed to run the voting scheme.

The scheme is inherently non-anonymous. For every vote it is required that it is known from whom it is so that inferred
votes can be calculated. The votes do not need to be public, but they still have to be stored non-anonymously. But to
really mitigate the issue of understanding why somebody lost and somebody won also the votes should be made public so
that people would be able to recalculate results by themselves. It would be an interesting research subject to find some
solutions to this problem of privacy. For example, some way of distributed computing where only people one-hop away from
the voter would know how she cast a vote, but in further hops only a combined vote would be seen. The other way is to
develop some new cryptographic primitives which would allow privacy on the one hand and verifiability on the other. We
do not really need to have public votes, just computability/verifiability of the results.

A members' social networks are also revealed. Especially the very important type of a social network -- the network of
influence between people. This is an additional hit on their privacy by the proposed scheme.

The voting scheme is still a majority rule and allows tyranny of the majority without proper (legal) preventions.

Vote trading is even easier done as person needs just to delegate as instructed (and payed). But also such "influences"
are then visible and recorded in the delegation network itself, so probably could be discovered if done systematically.

There are many things to research. Does limitations prevail over advantages? Would the proposed scheme be better than
current schemes? What are possible other misuses of the scheme or issues with it? How to improve privacy? Is it stable
(how much, in general, does the result change when one person changes her vote)? Can we employ even better algorithms to
analyze such delegation networks, or even use some other concept altogether to solve the presented decision problem. All
this are the questions which I would like to research, possibly with both modeling them mathematically and real-life
experiments.

A followup with comparison with other similar ideas and the analysis is available in [this
post](/post/peer-to-peer-voting-scheme).
