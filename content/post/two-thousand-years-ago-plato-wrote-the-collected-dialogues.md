---
title: Two thousand years ago, Plato wrote the collected dialogues
icon: quote
publishDate: 2013-09-11 15:31:10-07:00
draft: false
tags:
- books
- socrates
- thought
- reading
- questioning
- debating
disqus_id: "60968608630"
slug: two-thousand-years-ago-plato-wrote-the-collected-dialogues
aliases:
- /post/60968608630/two-thousand-years-ago-plato-wrote-the-collected
- /post/60968608630/
---

> Two thousand years ago, Plato wrote the collected dialogues in which he presented the views of Socrates on the
> important issues of those times. Socrates, Plato tells us, argued that books would destroy thought. How could this be?
> After all, books, reading, and writing are considered to be the very essence of the educated, intellectual citizen. How
> could one of the foremost thinkers of civilization deny their importance?

<!--more-->

> Socrates is famous for his dialogues between teacher and student in which each questions and examines the thoughts of
> the other. Questioning and examination are the tools of reflection: Hear an idea, ponder it, question it, modify it,
> explore its limitations. When the idea is presented by a person, the audience can interrupt, ask questions, probe to get
> at the underlying assumptions. But the author doesn't come along with a book, so how could the book be questioned if it
> couldn't answer back? This is what bothered Socrates.

> Socrates was concerned with reflective thought: the ability to think deeply about things, to question and examine every
> statement. He thought that reading was experiential, that it would not lead to reflection.

>> SOCRATES: Then anyone who leaves behind him a written manual, and likewise anyone who takes it over from him, on the
>> supposition that such writing will provide something reliable and permanent, must be exceedingly simple-minded; he must
>> really be ignorant of Ammon's utterance, if he imagines that written words can do anything more than remind one who
>> knows that which the writing is concerned with.

>> PHAEDRUS: Very true.

>> SOCRATES: You know, Phaedrus, that's the strange thing about writing, which makes it truly analogous to painting. The
>> painter's products stand before us as if they were alive, but if you question them, they maintain a most majestic
>> silence. It is the same with written words; they seem to talk to you as if they were intelligent, but if you ask them
>> anything about what they say, from a desire to be instructed, they go on telling you just the same thing forever. And
>> once a thing is put in writing, the composition, whatever it might be, drifts all over the place, getting into the hands
>> not only of those who understand it, but equally of those who have no business with it; it doesn't know how to address
>> the right people, and not to address the wrong. And when it is ill-treated and unfairly abused it always needs its
>> parent to come to its help, being unable to defend or help itself.

>> PHAEDRUS: Once again you are perfectly right.

> Socrates was an intellectual, and to him thinking was reflection or nothing. He didn't go for this experiential stuff.
> The worst kind of writing for people like Socrates would be novels, storytelling. A story engages the mind in an
> experiential mode, capturing the reader in the flow of events. All such experiential modes – music, drama, and novels –
> were considered to be the entertainment of the masses, not worthy of serious respect. Socrates worried that reading
> would be too passive, an acceptance of the thoughts of the writer without the chance to question them seriously.

> In the Middle Ages, just the opposite was true. Reading was generally done aloud, often to an audience. It was an active
> process, so active that Susan Noakes, in her analysis of medieval reading, points out "that it had been recommended by
> physicians, since classical times, as a mild form of exercise, like walking."

> Moreover, Noakes observes that the characteristics of a good novel today were unheard of in earlier times: "Today, many
> readers take as the hallmark of the good novel the way it propels them to read it continuously, without putting it down,
> from beginning to end. Readers of many late medieval books would have been forced, on the other hand, to read dis-
> continuously, stopping to puzzle over the relationship between complement and text." (The term complement refers to the
> dialogue provided through the illustrations and marginal comments – illuminations and glosses – sometimes put in by the
> author, sometimes by the copyist, sometimes by other readers.)

> During the Middle Ages, readers were taught the rules of rhetoric and were implored to employ them with each sentence:
> mnemonics, to memorize and learn the material; allegory, to find the multiple levels of meaning hidden beneath the
> literal text; typology, to think in historical parallels. No text was thought to be complete without mental elaboration
> in the mind of the individual reader or debates within the social group that might be listening to the read-aloud text.

> Readers in the latter part of the Middle Ages did with books exactly what Socrates had claimed was impossible: They
> questioned and debated each idea. True, the author wasn't around, but in many ways that made the job more challenging,
> more interesting. Read a sentence, question it. Read a page, criticize it. No authors to object. No authors to refute
> your arguments with the force of their rhetoric. Readers were free to develop their own objections and opinions without
> interference from meddling authors. Today we may have regressed to match the fears of Socrates: We read too quickly,
> without questioning or debating the thoughts of the author. But the fault does not lie with the book, the fault lies
> with the reader.

> Cognitive artifacts are tools, cognitive tools. But how they interact with the mind and what results they deliver depend
> upon how they are used. A book is a cognitive tool only for those who know how to read, but even then, what kind of tool
> it is depends upon how the reader employs it. A book cannot serve reflective thought unless the reader knows how to
> reason, to reflect upon the material.

From Things that Make Us Smart: Defending Human Attributes in the Age of the Machine by Donald A. Norman.

via [peerlibrary](https://blog.peerlibrary.org/post/60965604596/two-thousand-years-ago-plato-wrote-the-collected)
