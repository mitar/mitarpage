---
title: The Danger in Demonizing Male Sexuality
icon: link
publishDate: 2013-07-07 06:30:10-07:00
draft: false
tags:
- alyssaroyse
- sexism
- taboo
- sexuality
- male
- female
disqus_id: "54828476180"
slug: the-danger-in-demonizing-male-sexuality
aliases:
- /post/54828476180/the-danger-in-demonizing-male-sexuality
- /post/54828476180/
---

> [Alyssa Royse explains](https://goodmenproject.com/featured-content/the-danger-in-demonizing-male-sexuality/)
> how our current predator/prey model of sexual relationships is harmful to both men and women.

<!--more-->
