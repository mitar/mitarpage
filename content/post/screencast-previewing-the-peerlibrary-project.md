---
title: Screencast previewing the PeerLibrary project
icon: video
publishDate: 2013-10-08 04:25:10-07:00
draft: false
tags:
- media
- peerlibrary
- preview
- openaccess
- project
disqus_id: "63459032201"
slug: screencast-previewing-the-peerlibrary-project
aliases:
- /post/63459032201/screencast-previewing-the-peerlibrary-project
- /post/63459032201/
---

<video width="570" height="320" class="add-margin" controls src="/post/peerlibrary-preview.mp4" type="video/mp4" style="outline: none;"></video>

via [peerlibrary](https://blog.peerlibrary.org/post/63458789185/screencast-previewing-the-peerlibrary-project)

<!--more-->
