---
title: Reactive queries in PostgreSQL
publishDate: 2019-01-07 01:50:10-07:00
draft: false
tags:
- project
- postgresql
- reactivity
- nodejs
disqus_id: "181801121651"
slug: reactive-queries-in-postgresql
aliases:
- /post/181801121651/reactive-queries-in-postgresql
- /post/181801121651/
---

I am a big fan of the application architecture promoted by [Meteor](https://www.meteor.com/). I like declarative programming.
You describe what you want and not how and the system does the rest. Reactive programming is very similar.
You define how outputs should be computed from inputs, but when is this computed and how it is composed with
other computations is left to the system. So you can define what is read from the database and send to the client.
And how it is read on the client and transformed and send to the UI library. And then UI library can render this data.
And every time something changes, the rest gets automatically recomputed, refreshed, re-rendered.

Meteor is tightly linked with MongoDB. They developed a complex piece of technology to provide reactive queries.
Reactive queries are queries which after providing initial results they also continue providing any changes to those
results as input data used in queries change. While I like MongoDB, I still prefer consistency tools provided by
traditional SQL databases: transactions, foreign keys, joins and triggers. They are close to declarative programming as well.
You define relations between data once and then the system makes sure data is consistent.
I had to implement many of those features on top of MongoDB, like my package [PeerDB](https://github.com/peerlibrary/meteor-peerdb).

This is why I made [`reactive-postgres`](https://github.com/tozd/node-reactive-postgres) node.js package.
It provides exactly such reactive queries, but for
PostgreSQL open source database. Its API is simple, on purpose, and because it should be. You provide a query,
you get initial data, and then you get all changes. Try it out.

<!--more-->
