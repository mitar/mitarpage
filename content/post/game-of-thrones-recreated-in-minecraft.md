---
title: Game of Thrones recreated in Minecraft
icon: link
publishDate: 2013-04-01 00:36:10-07:00
draft: false
tags:
- collaboration
- gameofthrones
- minecraft
disqus_id: "46833071748"
slug: game-of-thrones-recreated-in-minecraft
aliases:
- /post/46833071748/game-of-thrones-recreated-in-minecraft
- /post/46833071748/
---

If [this](https://www.wired.com/2013/03/westeroscraft-game-thrones-minecraft/) is not a prime example of how Internet
can help people collaborate at scales previously not imaginable. So, if we can create virtual words together, can we
also govern our own physical world together?

<!--more-->
