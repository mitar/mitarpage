---
title: Solar powered WiFi node
icon: video
publishDate: 2013-03-23 02:39:10-07:00
draft: false
tags:
- media
- wlanslovenija
- project
- solar
- wireless
- meshnetworks
- communitynetworks
disqus_id: "46060959849"
slug: solar-powered-wifi-node
aliases:
- /post/46060959849/solar-powered-wifi-node-in-slovene
- /post/46060959849/
---

<iframe width="570" height="427" class="add-margin" src="https://www.youtube-nocookie.com/embed/dE3-QOokCIY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Solar powered WiFi node (in Slovene).

<!--more-->
