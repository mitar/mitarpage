---
title: regex2json
icon: link
publishDate: 2023-06-14 09:35:07.242624+00:00
draft: false
tags:
  - project
  - json
  - regexp
  - go
slug: regex2json
---

I made a simple tool [regex2json](https://gitlab.com/tozd/regex2json) in Go to convert traditional text-based
(and line-based) logs to JSON for programs which do not support JSON logs themselves. But the tool is more
general and can enable any workflow where you prefer operating on JSON instead of text. It works especially
great when combined with [jq](https://jqlang.github.io/jq/). I was inspired by this
[blog post](https://blog.kellybrazil.com/2019/11/26/bringing-the-unix-philosophy-to-the-21st-century/).

<!--more-->
