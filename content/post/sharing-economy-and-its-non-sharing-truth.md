---
title: Sharing economy and its non-sharing truth
publishDate: 2015-08-01 14:25:10-07:00
draft: false
tags:
- sharing
- capitalism
- unions
- sharingeconomy
disqus_id: "125621389961"
slug: sharing-economy-and-its-non-sharing-truth
aliases:
- /post/125621389961/sharing-economy-and-its-non-sharing-truth
- /post/125621389961/
---

Term "sharing economy" was coopted. It has little to do with real sharing. It has as much with sharing as my landlord
"shares" the apartment to me. This is not sharing. Sharing economy became another term for renting, lending,
outsourcing, only that the units of "sharing" are now smaller because information technology backing sharing economy
allow less overhead in coordination and transactions. Oh, and of course, almost no regulation and oversight. So less
paperwork. No contracts are signed, no responsibility or liability, no labor rights, no insurance. In real sharing you
also have none of those, so is maybe this the only thing which sharing economy shares with real sharing?

It is interesting to notice that a web service is not seen as sharing economy if it just facilitates real sharing
between people, without having an intermediary in between which is collecting a commission every time somebody shares
something. You need that intermediary charging a commission for a service to become sharing economy.

<!--more-->

[Wikipedia](https://www.wikipedia.org/) is not called sharing economy despite being a web service where people share
knowledge with each other.

On the other hand, [Amazon Mechanical Turk](https://www.mturk.com/mturk/welcome) was probably too early to be put into
the sharing economy basket, but it should be there. You use your own computer to do micro tasks for others. No
contracts, no labor rights, and nobody cares about tools you use.

It is interesting that services can be technically very similar, but results can be very different.
[Uber](https://www.uber.com/) connects drivers with riders. [Prevoz](http://prevoz.org/), a simple Slovenian ridesharing
website, is doing exactly the same. The main difference is that in the latter case website just connects people
together, but does not inject itself into the whole riding and payment processes later on, or collects any commission.

A similar example we can see is if we compare [AirBnB](https://www.airbnb.com/) and
[CouchSurfing](https://www.couchsurfing.com/). AirBnB connects and takes commission, CouchSurfing just connects, and
even forbids any monetary award for hosting a couch surfer.

Maybe sharing economy is real sharing, just with those intermediaries trying to find a new business model to keep the
sharing going? While Wikipedia lives from donations, CouchSurfing has not figured a good way to keep itself running. So
maybe putting yourself between all transactions is just a new approach to this problem of sustainability for the
service?

Maybe. But it also changes the dynamics and interactions with the service if every interaction has to be payed for, so
that commission can be extracted. Contrast this with volunteer donations of Wikipedia. Imagine that you would have to
pay a fee, even if it is a very small fee, every time you would want to read a Wikipedia page. Would you still use it
the same? Would still all people be able to use it the same? Decoupling consuming from a payment and even not requiring
a payment is an important property of real sharing.

Maybe the main difference is not in the ways you make a service sustainable, with commission or not, but what you do
with the income. Wikipedia is open and transparent about their income and expenses and they are putting all the income
back into the service itself, its operation, and its and community's growth. But what happens when the commission is
higher than just to keep the service running? When it has also satisfy investors' appetite for profit?

Those little but important details are what differentiates real sharing supported by information technology from a
simple capitalism driven to the extreme using the same information technology. In sharing economy workers are
individualized. They work on micro tasks. They have no continuity of employment. They all compete against each other so
their wages are minimized. They even have to use their own means of production (cars, apartments, etc.).

On the other hand, the companies, the intermediaries, keep all traditional powers (and issues arising from those
powers). They control who can work or not (you can only accept terms of use and cannot negotiate them). They control how
much workers are compensated (Uber decides the level of commission and prices of rides themselves). They decide about
the product or service and how it will evolve (drivers at Uber have no direct influence on the development of apps and
services provided). They control which values will be embedded in the company, product, service. Workers are there just
to do the work and this is it. Sharing economy is presented as full of amazing innovations, but maybe it is simply just
a regression back to the 19th century capitalism, just hidden under the veil of technology.

To address this discrepancy in power the solution back in the 19th century was for workers to unionize. They discovered
that if each worker operates on their own, they collectively get into a worse of situation. Without organizing they
cannot coordinate their actions, so they are at a disadvantage. They can make decisions only about themselves (work for
a given compensation or not), while employer can make decisions about all of them. Thus, employer can play workers
against each other, while workers cannot do anything against that. Their own personal decision might be the best for
them personally, but for them overall it might be a bad decision, especially in the long term.

This is one of the main issues of capitalism and free market anyway: the assumption that if all actors behave according
to what is the best for them, this will be the best for all. But this does not take into the account differences in
information one has, and power to react based on that information. Even if one worker has the best possible information,
they cannot just decide to make all workers respond based on this information. On the other hand, a company can. A
company can react as a whole, coordinate its activities, and have a persistent influence on how things are evolving.

The unionization solution to such capitalistic exploitation was so effective in the past that it had to be demonized.
Unionization was not perfect, it brought some new issues (power struggles inside unions, corruption, and other issues
common in hierarchical organization structures), but it was effective against the main issue it was trying to address:
lack of coordination and information flow between workers while companies have a natural benefit of both.

So if sharing economy is the old traditional capitalism using modern technologies to coordinate their workers, maybe
workers should start unionizing using modern technologies as well, removing the other issues unions brought with them.
Maybe it is time for a simple online platform where workers could share [how much you earn with each
other](http://www.wired.com/2015/07/happens-talk-salaries-google/), your benefits, you [could coordinate
actions](https://pando.com/2014/01/23/the-techtopus-how-silicon-valleys-most-celebrated-ceos-conspired-to-drive-down-100000-tech-engineers-wages/),
help each other, engage in solidarity with each other, [educate and learn from each other](http://p2pu.org/), you could
[decide to not work more than 8
hours](/post/why-vacations-and-8-hour-workday-matter).

We can learn from sharing economy. We can learn that using information technology to improve how people coordinate small
units of their time, work, property, effort, money, is really powerful, but do we really need intermediaries taking
commission to have this information technology to help us?
