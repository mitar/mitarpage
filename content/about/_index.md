---
title: "About"   
---

My work centers around building tools and systems for better collaboration.

Some of my interests are collective intelligence, trust networks, group decision support systems, collaboration tools,
peer-to-peer and distributed systems, democratizing AI/ML, computer security, e-democracy, deliberative democracy,
wireless mesh/community networks, organic/self-healing technologies.

Besides this blog, for more information see my [GitHub.com profile](https://github.com/mitar) and
[GitLab.com profile](https://gitlab.com/mitar) as well.

If you want to support my work, consider [sponsoring me](https://github.com/sponsors/mitar).
