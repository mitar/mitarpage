import logging
import os.path

import yaml

logger = logging.getLogger(__name__)


def parse_post(filename):
    with open(filename, 'r', encoding='utf8') as file:
        lines = file.readlines()

    front_matter = []
    body = []

    in_front_matter = False
    for i, line in enumerate(lines):
        if line == '---\n':
            in_front_matter = not in_front_matter
        elif in_front_matter:
            front_matter.append(line)
        else:
            body.append(line)

    front_matter = ''.join(front_matter)
    body = ''.join(body)

    front_matter = yaml.safe_load(front_matter)

    if len(front_matter['title']) > 100:
        logger.warning("""Title "%(title)s" from filename "%(filename)s" is too long.""", {
            'title': front_matter['title'],
            'filename': filename,
        })

    expected_slug = os.path.splitext(os.path.basename(filename))[0]

    if expected_slug != front_matter['slug']:
        logger.warning("""Slug "%(slug)s" from filename "%(filename)s" does not match expected slug "%(expected_slug)s".""", {
            'slug': front_matter['slug'],
            'filename': filename,
            'expected_slug': expected_slug,
        })

    return front_matter, body


def write_post(front_matter, body, filename):
    with open(filename, 'w', encoding='utf8') as file:
        file.write('---\n')
        yaml.safe_dump(front_matter, file, sort_keys=False, allow_unicode=True)
        file.write('---\n')
        file.write(body)
