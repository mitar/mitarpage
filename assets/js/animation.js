// Based on: http://jsdo.it/tholman/eVUR

var speed = 10;
var size = 8;
var totalBoids = 150;
var colorDark = [25, 25, 25];
var colorBright = [86, 147, 168];
var colorDecay = 0.04;

function distance(a, b) {
    dx = a.position.x - b.position.x;
    dy = a.position.y - b.position.y;
    return Math.sqrt((dx * dx) + (dy * dy));
}

function Boid(g) {
    var self = this;

    self.updateColor = function (flockSize) {
        var value = Math.min(1, self.colorDynamics.value + self.colorDynamics.velocity);
        self.colorDynamics.velocity -= colorDecay;
        self.colorDynamics.velocity = Math.max(0, self.colorDynamics.velocity);

        var red = Math.floor(colorDark[0] + (colorBright[0] - colorDark[0]) * value);
        var green = Math.floor(colorDark[1] + (colorBright[1] - colorDark[1]) * value);
        var blue = Math.floor(colorDark[2] + (colorBright[2] - colorDark[2]) * value);
        self.color = 'rgba(' + red + ',' + green + ',' + blue + ',1.0)';

        if (flockSize > Math.random() * (3 * totalBoids)) {
            self.colorDynamics.velocity = Math.random();
        }
    }

    self.addForce = function (force) {
        self.velocity.x += force.x;
        self.velocity.y += force.y;

        var magnitude = Math.sqrt(self.velocity.x * self.velocity.x + self.velocity.y * self.velocity.y);

        self.velocity.x /= magnitude;
        self.velocity.y /= magnitude;
    }

    self.applyForces = function () {
        var percievedCenter = {
            'x': 0,
            'y': 0
        };
        var percievedVelocity = {
            'x': 0,
            'y': 0
        };
        var flockCenter = {
            'x': 0,
            'y': 0
        };

        var flockSize = 0;

        g.each(function (boid, index) {
            if (self != boid) {
                var dist = distance(self, boid);

                if (dist > 0 && dist < 50) {
                    flockSize++;

                    percievedCenter.x += boid.position.x;
                    percievedCenter.y += boid.position.y;

                    percievedVelocity.x += boid.velocity.x;
                    percievedVelocity.y += boid.velocity.y;

                    if (dist < 12) {
                        flockCenter.x -= (boid.position.x - self.position.x);
                        flockCenter.y -= (boid.position.y - self.position.y);
                    }
                }
            }
        });

        self.updateColor(flockSize);

        if (flockSize > 0) {
            percievedCenter.x /= flockSize;
            percievedCenter.y /= flockSize;

            percievedCenter.x = (percievedCenter.x - self.position.x) / 400;
            percievedCenter.y = (percievedCenter.y - self.position.y) / 400;

            percievedVelocity.x /= flockSize;
            percievedVelocity.y /= flockSize;

            flockCenter.x /= flockSize;
            flockCenter.y /= flockSize;
        }

        self.addForce(percievedCenter);
        self.addForce(percievedVelocity);
        self.addForce(flockCenter);
    };

    self.checkWallCollisions = function () {
        if (self.position.x > g.canvas.width) {
            self.position.x = 0;
        }
        else if (self.position.x < 0) {
            self.position.x = g.canvas.width;
        }

        if (self.position.y > g.canvas.height) {
            self.position.y = 0;
        }
        else if (self.position.y < 0) {
            self.position.y = g.canvas.height;
        }
    };

    self.update = function () {
        self.tail.x = self.velocity.x * speed;
        self.tail.y = self.velocity.y * speed;

        self.position.x += self.tail.x;
        self.position.y += self.tail.y;

        self.applyForces();
        self.checkWallCollisions();
    };

    self.draw = function (ctx, canvas) {
        ctx.save();

        ctx.beginPath();
        ctx.strokeStyle = self.color;
        ctx.lineWidth = size;

        ctx.moveTo(self.position.x - self.tail.x, self.position.y - self.tail.y);
        ctx.lineTo(self.position.x, self.position.y);

        ctx.stroke();

        ctx.restore();
    };

    self.tail = {
        'x': null,
        'y': null
    };

    self.position = {
        'x': Math.random() * g.canvas.width,
        'y': Math.random() * g.canvas.height
    };

    self.velocity = {
        'x': Math.random() * 2 - 1,
        'y': Math.random() * 2 - 1
    };

    self.color = null;
    self.colorDynamics = {
        'value': 0,
        'velocity': 0
    };
    self.updateColor(0);
}

$(document).ready(function () {
    var g = $g().place('#canvas');

    function resizeCanvas() {
        g.size($('#canvas').width(), $('#canvas').height());
        g.draw();
    }

    $(window).resize(resizeCanvas);
    resizeCanvas();

    for (var i = 0; i < totalBoids; i++) {
        g.add(new Boid(g));
    }
    g.play(40);
});
