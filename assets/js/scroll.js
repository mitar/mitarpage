$(document).ready(function() {
    function waitFor(tester, fun) {
        if (tester()) {
            fun();
        } else {
            setTimeout(function () {
                waitFor(tester, fun);
            }, 50);
        }
    }

    $('#tumblr-posts > .posts').infinitescroll({
        navSelector: '#page-footer .pages',
        nextSelector: '#page-footer .pages .next a',
        itemSelector: '#tumblr-posts > .posts > .post',
        donetext: "<em>No more posts.</em>",
        loading: {
            finishedMsg: "<em>No more posts.</em>"
        },
        pathParse: function (path, nextPage) {
            if (path.match(/^(.*\/page\/)(\d+)$/)) {
                var match = path.match(/^(.*\/page\/)(\d+)$/);
                this.state.currPage = parseInt(match[2]) - 1;
                return [match[1], ''];
            } else {
                this.state.isInvalidPage = true;
                return null;
            }
        }
    }, function (newElements) {
        $.each(newElements, function (i, el) {
            waitFor(function () {
                return typeof FB !== 'undefined';
            }, function () {
                FB.XFBML.parse(el);
            });
        });
        waitFor(function () {
            return typeof twttr !== 'undefined'
        }, function () {
            twttr.widgets.load();
        });
        waitFor(function () {
            return typeof DISQUSWIDGETS !== 'undefined'
        }, function () {
            DISQUSWIDGETS.getCount();
        });
    });
});
