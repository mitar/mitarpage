$(document).ready(function () {
    $('#pgp').hover(function () {
        $('a', this).text($(this).data('fingerprint'));
    }, function () {
        $('a', this).text($(this).data('id'));
    });
});

function onloadRecaptcha() {
    grecaptcha.render('recaptcha', {
        'sitekey': '6LchX0kaAAAAALyfa4v7vsL9WNq5wnTe--ALUboR',
        'callback': function (token) {
            grecaptcha.reset();
            $('#recaptcha').replaceWith('<div id="recaptcha"></div>');
            $('#news').attr('action', 'https://common.tnode.com/sympa').submit();
        },
    });
}

$(document).ready(function () {
    $('#news').submit(function (e) {
         if ($('#news').attr('action') !== 'https://common.tnode.com/sympa') {
             e.preventDefault();

             var scriptElm = document.createElement('script');
             scriptElm.src = 'https://www.google.com/recaptcha/api.js?onload=onloadRecaptcha&render=explicit';
             document.body.appendChild(scriptElm);
         }
    })
});
